/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


public class generador {
    
    public static void main(String[] args) {
        String arr[] = {"nombre",
            "apellido",
            "cedula",
            "direccion",
            "carrera",
            "modalidad",
            "nivelestudios",
            "convencional",
            "movil",
            "correo",
            "clave"};
        String s = "";
        for (String var : arr) {
            s += "<div class=\"form-group\">\n";
            s += "    <form:label path=\"" + var + "\" cssClass=\"col-lg-2 control-label\">" + var + "</form:label>\n";
            s += "    <div class=\"col-lg-10\">\n";
            s += "         <form:input path=\"" + var + "\" cssClass=\"form-control\"/>\n";
            s += "    </div>\n";
            s += "</div>\n";
        }
//        System.out.println(s);
        atributos(arr);
    }
    
    public static void atributos(String arr[]) {
        String salida = "";
        for (String var : arr) {
            salida += "model.addAttribute(\"" + var + "\", e.get" + var.substring(0,1).toUpperCase() +var.substring(1)+ "());\n";
        }
        System.out.println(salida);
    }
}
