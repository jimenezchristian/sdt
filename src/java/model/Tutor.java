package model;
// Generated 18/01/2017 21:27:42 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Tutor generated by hbm2java
 */
public class Tutor  implements java.io.Serializable {


     private Integer id;
     private Rol rol;
     private String nombrecompleto;
     private int cedula;
     private String clave;
     private int carrera;
     private Set proyectoinveses = new HashSet(0);
     private Set procesoproyectoses = new HashSet(0);
     private Set estudiantes = new HashSet(0);
     private Set procesosexamens = new HashSet(0);
     private Set examencomplexivos = new HashSet(0);

    public Tutor() {
    }

	
    public Tutor(Rol rol, int cedula, String clave) {
        this.rol = rol;
        this.cedula = cedula;
        this.clave = clave;
    }
    public Tutor(Rol rol, String nombrecompleto, int cedula,int carrera, String clave, Set proyectoinveses, Set procesoproyectoses, Set estudiantes, Set procesosexamens, Set examencomplexivos) {
       this.rol = rol;
       this.nombrecompleto = nombrecompleto;
       this.cedula = cedula;
       this.carrera = carrera;
       this.clave = clave;
       this.proyectoinveses = proyectoinveses;
       this.procesoproyectoses = procesoproyectoses;
       this.estudiantes = estudiantes;
       this.procesosexamens = procesosexamens;
       this.examencomplexivos = examencomplexivos;
    }

    public Tutor(String nombrecompleto) {
        this.nombrecompleto = nombrecompleto;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Rol getRol() {
        return this.rol;
    }
    
    public void setRol(Rol rol) {
        this.rol = rol;
    }
    public String getNombrecompleto() {
        return this.nombrecompleto;
    }
    
    public void setNombrecompleto(String nombrecompleto) {
        this.nombrecompleto = nombrecompleto;
    }
    public int getCedula() {
        return this.cedula;
    }
    
    public void setCedula(int cedula) {
        this.cedula = cedula;
    }
    public String getClave() {
        return this.clave;
    }
    
    public void setClave(String clave) {
        this.clave = clave;
    }
    public Set getProyectoinveses() {
        return this.proyectoinveses;
    }
    
    public void setProyectoinveses(Set proyectoinveses) {
        this.proyectoinveses = proyectoinveses;
    }
    public Set getProcesoproyectoses() {
        return this.procesoproyectoses;
    }
    
    public void setProcesoproyectoses(Set procesoproyectoses) {
        this.procesoproyectoses = procesoproyectoses;
    }
    public Set getEstudiantes() {
        return this.estudiantes;
    }
    
    public void setEstudiantes(Set estudiantes) {
        this.estudiantes = estudiantes;
    }
    public Set getProcesosexamens() {
        return this.procesosexamens;
    }
    
    public void setProcesosexamens(Set procesosexamens) {
        this.procesosexamens = procesosexamens;
    }
    public Set getExamencomplexivos() {
        return this.examencomplexivos;
    }
    
    public void setExamencomplexivos(Set examencomplexivos) {
        this.examencomplexivos = examencomplexivos;
    }

    public int getCarrera() {
        return carrera;
    }

    public void setCarrera(int carrera) {
        this.carrera = carrera;
    }




}


