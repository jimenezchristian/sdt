package model;
// Generated 18/01/2017 21:27:42 by Hibernate Tools 4.3.1



/**
 * Items generated by hbm2java
 */
public class Items  implements java.io.Serializable {


     private Integer id;
     private Catalogo catalogo;
     private String nombre;
     private Boolean activo;
     private String fechaingresocarrera;

    
    public Items() {
    }
    public Items(Integer id) {
        this.id=id;
    }
	
    public Items(Catalogo catalogo) {
        this.catalogo = catalogo;
    }
    public Items(Catalogo catalogo, String nombre, Boolean activo, String fechaingresocarrera) {
       this.catalogo = catalogo;
       this.nombre = nombre;
       this.activo = activo;
       this.fechaingresocarrera=fechaingresocarrera;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Catalogo getCatalogo() {
        return this.catalogo;
    }
    
    public void setCatalogo(Catalogo catalogo) {
        this.catalogo = catalogo;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public Boolean getActivo() {
        return this.activo;
    }
    
    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getFechaingresocarrera() {
        return fechaingresocarrera;
    }

    public void setFechaingresocarrera(String fechaingresocarrera) {
        this.fechaingresocarrera = fechaingresocarrera;
    }

}


