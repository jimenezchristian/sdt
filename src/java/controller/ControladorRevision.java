/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.ProcesoDao;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Estudiante;
import model.Items;
import model.Procesoproyectos;
import model.Procesos;
import model.Tutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller
@RequestMapping("agregarRevision.htm")

public class ControladorRevision {
    private JdbcTemplate jdbctemplate;
    
    public ControladorRevision (){
         Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
         
    }
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView ControladorRevision(HttpServletRequest http){
        int id= Integer.parseInt(http.getParameter("id"));
        int idSec=Integer.parseInt(http.getParameter("idSec"));
        int tipo=Integer.parseInt(http.getParameter("Tipo"));
        ModelAndView modelo = new ModelAndView();
        if(tipo==25){
            String sql= "SELECT p.nombreProceso as name_proceso, p.id as id from procesos p left Join procesoproyectos r on p.id =r.id_proceso and r.estudiante ="+id+" where r.id_proceso is null";

           List pro=this.jdbctemplate.queryForList(sql);
           
           String sqlultimoPro= "SELECT MAX(id)as id FROM procesos";
            List<Map<String, Object>> idProceso=this.jdbctemplate.queryForList(sqlultimoPro);
            int proceso= Integer.parseInt(idProceso.get(0).get("id").toString());
            String comprobar = "SELECT p.estudiante  FROM procesoproyectos p JOIN estudiante e WHERE p.id_proceso="+proceso+" and p.estudiante="+id+" and p.estudiante=e.id";
            List listComprobar=this.jdbctemplate.queryForList(comprobar);
            if(listComprobar.size()==0){
                //System.out.println("no se graduooooooooooooooo");
            }
            else{
                //actualizar con 1
                this.jdbctemplate.update(
                    "update estudiante "
                    +"set graduacion=?"                              
                    +" where id= ?"   , 
                    1,id
                );
        
            }
           
           modelo.addObject("proceso",pro);
           modelo.addObject("procesoproyectos", new Procesoproyectos());
           
        }
        else{
            if (tipo==26){
           String sql= "SELECT p.nombreProceso as name_proceso, p.id as id from procesos_complexivo p left Join procesosexamen r on p.id =r.id_procesoCompl and r.estudiante ="+id+" where r.id_procesoCompl is null";

           List pro=this.jdbctemplate.queryForList(sql);
           String sqlultimoPro= "SELECT MAX(id)as id FROM procesos_complexivo";
            List<Map<String, Object>> idProceso=this.jdbctemplate.queryForList(sqlultimoPro);
            int proceso= Integer.parseInt(idProceso.get(0).get("id").toString());
            String comprobar = "SELECT p.estudiante  FROM procesosexamen p JOIN estudiante e WHERE p.id_procesoCompl="+proceso+" and p.estudiante="+id+" and p.estudiante=e.id";
            List listComprobar=this.jdbctemplate.queryForList(comprobar);
            if(listComprobar.size()==0){
                //System.out.println("no se graduooooooooooooooo");
            }
            else{
                //actualizar con 1
                this.jdbctemplate.update(
                    "update estudiante "
                    +"set graduacion=?"                              
                    +" where id= ?"   , 
                    1,id
                );
            }
           
           
           modelo.addObject("proceso",pro);
           modelo.addObject("procesoproyectos", new Procesoproyectos());
           
            }
        }
        modelo.addObject("idSec",idSec);
        modelo.setViewName("agregarRevision");
        return modelo;
        
    }
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView agregarDatos(@ModelAttribute("procesoproyectos") Procesoproyectos p,HttpServletRequest http ) throws Exception{
        
        ModelAndView modelo= new ModelAndView();
         int id= Integer.parseInt(http.getParameter("id"));
         int idSec=Integer.parseInt(http.getParameter("idSec"));
         int tipo=Integer.parseInt(http.getParameter("Tipo"));
       InputStream inputStream = null;
        CommonsMultipartFile uploaded=p.getArchivo();
        ArrayList recuperarId= this.seleccionarDocente(id);
       inputStream=uploaded.getInputStream();
       //recuperar el proceso 
       
       if(tipo==25){
        String sqlfase= "SELECT fase  FROM procesos where id="+p.getProcesos().getId();
            List<Map<String, Object>> ListFase=this.jdbctemplate.queryForList(sqlfase);
            int fase= Integer.parseInt(ListFase.get(0).get("fase").toString());   
        this.jdbctemplate.update(
            "insert into procesoproyectos (tipofase,id_proceso, archivo,nombreArchivo,estado,estudiante,tutor) values (?,?,?,?,?,?,?)",
             fase,p.getProcesos().getId(),inputStream,p.getArchivo().getOriginalFilename(),p.getEstado(),id,recuperarId.get(0)
        );
            String sqlultimoPro= "SELECT MAX(id)as id FROM procesos";
            List<Map<String, Object>> idProceso=this.jdbctemplate.queryForList(sqlultimoPro);
            int proceso= Integer.parseInt(idProceso.get(0).get("id").toString());
            String comprobar = "SELECT p.estudiante  FROM procesoproyectos p JOIN estudiante e WHERE p.id_proceso="+proceso+" and p.estudiante="+id+" and p.estudiante=e.id";
            List listComprobar=this.jdbctemplate.queryForList(comprobar);
            if(listComprobar.size()==0){
                //System.out.println("no se graduooooooooooooooo");
            }
            else{
                //actualizar con 1
                this.jdbctemplate.update(
                    "update estudiante "
                    +"set graduacion=?"                              
                    +" where id= ?"   , 
                    1,id
                );
        
            }
       }
       else{
           if(tipo==26){
               String sqlfase= "SELECT fase  FROM procesos_complexivo where id="+p.getProcesos().getId();
            List<Map<String, Object>> ListFase=this.jdbctemplate.queryForList(sqlfase);
            int fase= Integer.parseInt(ListFase.get(0).get("fase").toString());   
               this.jdbctemplate.update(
            "insert into procesosexamen (tipofase,id_procesoCompl, archivo,nombreArchivo,estado,estudiante,tutor) values (?,?,?,?,?,?,?)",
             fase,p.getProcesos().getId(),inputStream,p.getArchivo().getOriginalFilename(),p.getEstado(),id,recuperarId.get(0)
                );
               
            String sqlultimoPro= "SELECT MAX(id)as id FROM procesos_complexivo";
            List<Map<String, Object>> idProceso=this.jdbctemplate.queryForList(sqlultimoPro);
            int proceso= Integer.parseInt(idProceso.get(0).get("id").toString());
            String comprobar = "SELECT p.estudiante  FROM procesosexamen p JOIN estudiante e WHERE p.id_procesoCompl="+proceso+" and p.estudiante="+id+" and p.estudiante=e.id";
            List listComprobar=this.jdbctemplate.queryForList(comprobar);
            if(listComprobar.size()==0){
                //System.out.println("no se graduooooooooooooooo");
            }
            else{
                //actualizar con 1
                this.jdbctemplate.update(
                    "update estudiante "
                    +"set graduacion=?"                              
                    +" where id= ?"   , 
                    1,id
                );
            }
           }
       }
        
        return new ModelAndView("redirect:/secretariahome.htm?id="+idSec);
    }

    private ArrayList seleccionarDocente(int id) {
     
        final ArrayList lista= new ArrayList();
        String consulta="select tutor from estudiante WHERE id="+id;
        
         return (ArrayList) jdbctemplate.query(consulta, new ResultSetExtractor<ArrayList>(){
            public ArrayList extractData(ResultSet rs) throws SQLException{
                if(rs.next()){
                    lista.add(rs.getInt("tutor"));
                }
                return lista;
            }   
        });
    }
    
}
