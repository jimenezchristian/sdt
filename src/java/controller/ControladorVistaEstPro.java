/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Estudiante;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller

public class ControladorVistaEstPro {
        private JdbcTemplate jdbctemplate;
    
    public ControladorVistaEstPro(){
         Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
         
    }
    @RequestMapping(value="vistaprocesosest.htm", method = RequestMethod.GET)
    public ModelAndView viewEstudianteNew(HttpServletRequest http) {
        ModelAndView modelo= new ModelAndView();
        int id=Integer.parseInt(http.getParameter("id"));
        try{
        String sql="select tipo from estudiante where id="+id;
        List<Map<String,Object>> tipo= this.jdbctemplate.queryForList(sql);
        int valor=(int)tipo.get(0).get("tipo");
        String titulo="";
        if (valor==25){
            String sqlprocesos= "select * from procesos";
            List procesos=this.jdbctemplate.queryForList(sqlprocesos);
            modelo.addObject("datosprocesos",procesos);//enviamos a la vistta
            modelo.addObject("tipo",valor);
            titulo="Procesos para Trabajos de titulación";
           modelo.addObject("titulo", titulo);
        }
        else{
            if (valor==26){
                String sqlprocesos= "select * from procesos_complexivo";
                List procesos=this.jdbctemplate.queryForList(sqlprocesos);
                modelo.addObject("datosprocesos",procesos);//enviamos a la vistta
                modelo.addObject("tipo",valor);
                titulo="Procesos para Examen Complexivo";
                modelo.addObject("titulo", titulo);
            }                                   
        }
            
         modelo.addObject("id", id);
         return modelo;
        }
        catch (Exception e){
            String titulo="Ud. no esta inscrito en Ninguna Unidad de Titulación Especial";
            modelo.addObject("titulo", titulo);
            modelo.addObject("id", id);
            return modelo;
        }
        

    }
    
    @RequestMapping(value = "vistarevisionest.htm",method = RequestMethod.GET )
    public ModelAndView viewEstudianteProceso(HttpServletRequest http) {
        ModelAndView modelo= new ModelAndView();
        
        int id=Integer.parseInt(http.getParameter("id"));
        try{
                String sql="select tipo from estudiante where id="+id;
               List<Map<String,Object>> tipo= this.jdbctemplate.queryForList(sql);
               int valor=(int)tipo.get(0).get("tipo");

               
               if(valor==25){
                   String sqlEst= "select e.id as id,e.nombre as nombre, e.apellido as apellido, i.nombre as carrera, e.correo as correo from estudiante e inner join items i on e.carrera=i.id and e.id="+id+" and e.tipo=25 inner join proyectoinves p on e.id=p.estudiante";
                   String sqlProcesos= "select * from procesos";
                   String sqlRevision ="select e.id as idEstudiante ,r.estado as estadoRev,r.id_Proceso as procesoRev, r.archivo as archivoRev from procesoproyectos  r JOIN estudiante e where e.id=r.estudiante ";
                   List datosestudiantes=this.jdbctemplate.queryForList(sqlEst);
                   List listaRev=this.jdbctemplate.queryForList(sqlRevision);
                   List procesos=this.jdbctemplate.queryForList(sqlProcesos);
                   modelo.addObject("datos",datosestudiantes);
                   modelo.addObject("datosRevision",listaRev);
                   modelo.addObject("datosprocesos",procesos);//enviamos a la vista
                   modelo.addObject("tipo",valor);
               }
               else{
                   if (valor==26){
                       String sqlEst= "select e.id as id,e.nombre as nombre, e.apellido as apellido, i.nombre as carrera, e.correo as correo from estudiante e inner join items i on e.carrera=i.id and e.id="+id+" and e.tipo=26 inner join examencomplexivo p on e.id=p.estudiante";
                       String sqlcompexivo= "select * from procesos_complexivo";
                       String sqlRevision ="select e.id as idEstudiante ,r.estado as estadoRev,r.id_ProcesoCompl as procesoRev, r.archivo as archivoRev from procesosexamen  r JOIN estudiante e where e.id=r.estudiante and e.id="+id;
                       List datosestudiantes=this.jdbctemplate.queryForList(sqlEst);
                       List procesos=this.jdbctemplate.queryForList(sqlcompexivo);
                       List listaRev=this.jdbctemplate.queryForList(sqlRevision);
                       modelo.addObject("datos",datosestudiantes);
                       modelo.addObject("datosprocesos",procesos);//enviamos a la vista
                       modelo.addObject("datosRevision",listaRev);
                       modelo.addObject("tipo",valor);
                   }
               }


             modelo.addObject("id", id);
                return modelo;
      
        }catch (Exception e){
            
            modelo.addObject("id", id);
            return modelo;
        }
        

    }
    
}
