/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Estudiante;
import model.Items;
import model.Procesos;
import model.Tutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller

public class ListaEstudiantesController {
    private JdbcTemplate jdbctemplate;
    
    public ListaEstudiantesController (){
         Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
         
    }
    @ModelAttribute("tipoList")
    public Map<Integer, String> getTipo() {
        Map<Integer, String> tipo = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getTipo()) {
            tipo.put(i.getId(), i.getNombre());
        }
        return tipo;
    }
            @ModelAttribute("carrerasList")
    public Map<Integer, String> getCarreras() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Tutor i : dao.TutorDao.getCarreras()) {
            periodos.put(i.getCarrera(), i.getNombrecompleto());
        }
        return periodos;
    }

    @RequestMapping(value = "listaEstudiantesTesis.htm" , method = RequestMethod.GET)
    public ModelAndView viewMenu(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo= new ModelAndView();
        
        modelo.addObject("id",id);
        
        return modelo;
    }
    
    @RequestMapping(value = "listaEstudiantesTesis.htm" , method = RequestMethod.POST)
    public ModelAndView viewEstudianteNew(HttpServletRequest http) {
        int id= Integer.parseInt(http.getParameter("id"));
        int tipo=Integer.parseInt(http.getParameter("tipo"));
        int carrera=Integer.parseInt(http.getParameter("carreras"));
        ModelAndView modelo= new ModelAndView();
        if (tipo==25){
            //String sqlEst= "select e.id as id,e.nombre as nombre, e.apellido as apellido, i.nombrecompleto as carrera, e.correo as correo from estudiante e inner join tutor i on (i.carrera=e.carrera and e.carrera="+carrera+") and e.tipo=25 inner join proyectoinves p on e.id=p.estudiante";
            String sqlEst= "select e.id as id,e.nombre as nombre, e.apellido as apellido, i.nombrecompleto as carrera, n.nombre as niveles,"
                    +" e.correo as correo, it.nombre as periodo"
                    +" from nivelestudios n, items it, estudiante e inner join tutor i on (i.carrera=e.carrera and e.carrera="+carrera+") and e.tipo=25"
                    +" inner join proyectoinves p on e.id=p.estudiante WHERE e.nivelestudios=n.id AND p.periodorealizacionpi= it.id";
            String sql= "select * from procesos";
            
            String sqlRevision ="select e.id as idEstudiante ,r.estado as estadoRev,r.id_Proceso as procesoRev, r.archivo as archivoRev from procesoproyectos  r JOIN estudiante e where e.id=r.estudiante";
                        
            List datosestudiantes=this.jdbctemplate.queryForList(sqlEst);
            List procesos=this.jdbctemplate.queryForList(sql);
            List listaRev=this.jdbctemplate.queryForList(sqlRevision);                                                
            
            modelo.addObject("datos",datosestudiantes);
            modelo.addObject("datosprocesos",procesos);
            modelo.addObject("datosRevision",listaRev);
            modelo.addObject("tipo",tipo);
        }
        else{
            if(tipo == 26 ){
                //String sqlEst= "select e.id as id,e.nombre as nombre, e.apellido as apellido, i.nombrecompleto as carrera, e.correo as correo from estudiante e inner join tutor i on (i.carrera=e.carrera and e.carrera="+carrera+") and e.tipo=26 inner join examencomplexivo p on e.id=p.estudiante";
                String sqlEst= "select e.id as id, e.nombre as nombre, e.apellido as apellido, i.nombrecompleto as carrera, n.nombre as niveles,"
                        +" e.correo as correo, ut.nombre as periodo"
                        +" from nivelestudios n, periodosunidadtitulacion ut, estudiante e inner join tutor i on (i.carrera=e.carrera and e.carrera="+carrera+") and e.tipo=26 inner join examencomplexivo p on e.id=p.estudiante"
                        +" WHERE e.nivelestudios=n.id AND p.periodoingresounidadtit=ut.id";
                String sqlcompexivo= "select * from procesos_complexivo";
                String sqlRevision ="select e.id as idEstudiante ,r.estado as estadoRev,r.id_ProcesoCompl as procesoRev, r.archivo as archivoRev from procesosexamen  r JOIN estudiante e where e.id=r.estudiante";
                List datosestudiantes=this.jdbctemplate.queryForList(sqlEst);
                List procesos=this.jdbctemplate.queryForList(sqlcompexivo);
                List listaRev=this.jdbctemplate.queryForList(sqlRevision);
                modelo.addObject("datos",datosestudiantes);
                modelo.addObject("datosprocesos",procesos);//enviamos a la vista
                modelo.addObject("datosRevision",listaRev);
                modelo.addObject("tipo",tipo);
            }
        }
    
         modelo.addObject("id",id);
     
      
        return modelo;
       

    }
    
    
}
