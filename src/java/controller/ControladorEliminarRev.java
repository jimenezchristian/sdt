/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller
@RequestMapping("eliminarRevision.htm")
public class ControladorEliminarRev {
    private JdbcTemplate jdbctemplate;
     public ControladorEliminarRev(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }
     @RequestMapping(method = RequestMethod.GET)
     public ModelAndView eliminar(HttpServletRequest http){
         int id=Integer.parseInt(http.getParameter("id"));
         int idSec=Integer.parseInt(http.getParameter("idSec"));
          int idProceso=Integer.parseInt(http.getParameter("idProceso"));
          int tipo=Integer.parseInt(http.getParameter("Tipo"));
          
         ModelAndView modelo = new ModelAndView();
         if (tipo==25){
            this.jdbctemplate.update(
            "delete from procesoproyectos where estudiante=? and id_proceso=?",
            id,idProceso
            );
            
            this.jdbctemplate.update(
            "update estudiante "
            +"set graduacion=?"                
            +" where id= ?"   , 
             0,id
            );                        
            
         }
         else{
             if(tipo==26){
                this.jdbctemplate.update(
                "delete from procesosexamen where estudiante=? and id_procesoCompl=?",
                id,idProceso
                );
                
                this.jdbctemplate.update(
                "update estudiante "
                +"set graduacion=?"                
                +" where id= ?"   , 
                0,id
                ); 
             }
         }
         modelo.addObject("id", id);
         
         return new ModelAndView("redirect:/listaEstudiantesTesis.htm?id="+idSec);
     }
    
}
