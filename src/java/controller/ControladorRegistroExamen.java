/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateUtil;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Estudiante;
import model.Examencomplexivo;
import model.Items;
import model.Tutor;
import org.hibernate.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller

public class ControladorRegistroExamen {
    @RequestMapping(value = "tutorformex.htm",method =RequestMethod.GET)
    public ModelAndView view(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
         int idTutor=Integer.parseInt(http.getParameter("idTutor"));
         ModelAndView modelo= new ModelAndView();
             modelo.addObject("examencomplexivo",new Examencomplexivo());
           modelo.addObject("id", idTutor);
            
         return modelo;
    }
    @RequestMapping(value = "tutorformex.htm",method = RequestMethod.POST)
    public ModelAndView agregarTema(@ModelAttribute("examencomplexivo") Examencomplexivo p, HttpServletRequest http){
        int id=Integer.parseInt(http.getParameter("id"));
        int idTutor=Integer.parseInt(http.getParameter("idTutor"));
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        //Add new Employee object
        Estudiante est= new Estudiante();
        session.load(est, id);
        est.setId(id);
        Examencomplexivo nuevoexamen = new Examencomplexivo();
        nuevoexamen.setFolioex(p.getFolioex());
        nuevoexamen.setMatriculaex(p.getMatriculaex());
        nuevoexamen.setPeriodoingresounidadtit(p.getPeriodoingresounidadtit());
        nuevoexamen.setPeriodocapacitacion(p.getPeriodocapacitacion());
        nuevoexamen.setFechaexamencomplexivo(p.getFechaexamencomplexivo());
        System.out.println("fecha de investidura: "+p.getFechainvestidura());
        if (p.getFechainvestidura().isEmpty()) {
            nuevoexamen.setFechainvestidura(null);
        } else {
                nuevoexamen.setFechainvestidura(p.getFechainvestidura());
        }
        nuevoexamen.setFechagracia(p.getFechagracia());
        
        nuevoexamen.setEstudiante(est);
         Tutor t= new Tutor();
         session.load(t, idTutor);
         t.setId(idTutor);
         
         nuevoexamen.setTutor(t);
         
        //Save the employee in database
        session.save(nuevoexamen);
 
        //Commit the transaction
        session.getTransaction().commit();
        return new ModelAndView("redirect:/tutorhome.htm?id="+idTutor);
        
    }
    
    @ModelAttribute("periodosList")

    public Map<Integer, String> getPeriodos() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        periodos.put(0,"Seleccione..");
        for (Items i : dao.CatalogoDao.getPeriodos()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }
    
}
