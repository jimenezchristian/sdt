/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.EstudianteDao;
import dao.HibernateUtil;
import dao.ProyectoInvestigacionDao;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import model.Estudiante;
import model.Examencomplexivo;
import model.Items;
import model.Menu;
import model.Menurol;
import model.Proyectoinves;
import model.Tutor;
import org.hibernate.Session;
import org.recursos.clasesauxi.Persona;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TutorController {

    @RequestMapping(value = "tutorhome.htm",method = RequestMethod.GET)
    public ModelAndView viewHome(HttpServletRequest http) {
        
        ModelAndView modelo = new ModelAndView();
         int id=Integer.parseInt(http.getParameter("id"));
         Tutor t=dao.TutorDao.getTutor(id);
         
         modelo.addObject("usuario", t.getNombrecompleto());
          modelo.addObject("idTutor", t.getId());
        return modelo;
    }

    @RequestMapping(value = "tesis.htm",method = RequestMethod.GET)
    public ModelAndView viewTesis() {
        return new ModelAndView("tesis","command",new Proyectoinves());
    }

    @RequestMapping(value = "estudiantenew.htm", method = RequestMethod.GET)
    public ModelAndView viewEstudianteNew() {
        ModelAndView modelo = new ModelAndView();
        modelo.setViewName("estudiantenew");
        modelo.addObject("estudiante",new Estudiante());
        return modelo;
    }
    
     @RequestMapping(value = "tutorformex.htm",method = RequestMethod.GET)
    public ModelAndView viewExamen() {
        return new ModelAndView("tutorformex","command",new Examencomplexivo());
    }


    @ModelAttribute("modalidadList")



    public Map<Integer, String> getModalidades() {
        Map<Integer, String> modalidades = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getModalidades()) {
            modalidades.put(i.getId(), i.getNombre());
        }
        return modalidades;
    }

    @ModelAttribute("carreraList")
    public Map<Integer, String> getCarreras() {
        Map<Integer, String> carreras = new LinkedHashMap<>();
        for (Tutor i : dao.TutorDao.getCarreras()) {
            carreras.put(i.getCarrera(), i.getNombrecompleto());
        }
        return carreras;
    }

    @ModelAttribute("nivelestudiosList")
    public Map<Integer, String> getNivelEstudios() {
        Map<Integer, String> nivelestudios = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getNivelEstudios()) {
            nivelestudios.put(i.getId(), i.getNombre());
        }
        return nivelestudios;
    }

    @ModelAttribute("periodosList")
    public Map<Integer, String> getPeriodos() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getPeriodos()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }
    
    
@ModelAttribute("tipoList")
    public Map<Integer, String> getTipo() {
        Map<Integer, String> tipo = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getModalidades()) {
            tipo.put(i.getId(), i.getNombre());
        }
        return tipo;
    }

    @ModelAttribute("menuList")
    public List<Menu> getMenu() {
        List<Menu> menus = new ArrayList<>();
        for (Menurol i : dao.MenuRolDao.getMunus(2)) {
            Menu menu = dao.MenuDao.getId(i.getMenu().getId());
            
            menus.add(menu);
        }
        return menus;
    }
    @RequestMapping(value = "dtutornew.htm", method = RequestMethod.POST)
    public ModelAndView addStudent(@ModelAttribute("tutornew")  @Valid Estudiante e)
            {
                Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        //Add new Employee object
        Estudiante nuevoest = new Estudiante();
        nuevoest.setNombre(e.getNombre());
        nuevoest.setApellido(e.getApellido());
         nuevoest.setCedula(e.getCedula());
         nuevoest.setDireccion(e.getDireccion());
         nuevoest.setCarrera(e.getCarrera());
         nuevoest.setModalidad(e.getModalidad());
         nuevoest.setNivelestudios(e.getNivelestudios());
         nuevoest.setConvencional(e.getConvencional());
         nuevoest.setMovil(e.getMovil());
         nuevoest.setCorreo(e.getCorreo());
         nuevoest.setClave(e.getClave());
         nuevoest.setTipo(e.getTipo());
        //Save the employee in database
        session.save(nuevoest);
 
        //Commit the transaction
        session.getTransaction().commit();
        return new ModelAndView("redirect:/tutorhome.htm");
    }
}
