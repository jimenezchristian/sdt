/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import model.Administrador;
import model.Estudiante;
import model.Secretaria;
import model.Tutor;
import org.recursos.clasesauxi.Persona;
//import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    @RequestMapping(value = "login.htm", method = RequestMethod.GET)
    public ModelAndView form() {
        return new ModelAndView("login", "command", new Persona());
    }
    
    @RequestMapping(value = "contactos.htm")
    public ModelAndView contactos() {
        return new ModelAndView("contactos");
    }

    @RequestMapping(value = "login.htm", method = RequestMethod.POST)
    public String login(Persona p, HttpServletRequest request) {
        String cedula = request.getParameter("cedula");
        String clave = request.getParameter("clave");
        Tutor tutor = dao.UsuarioDao.getTutor(cedula, clave);
        
        Secretaria secretaria = null;
        Estudiante estudiante = null;
        Administrador administrador = null;
        HttpSession session = request.getSession();
        String res = "login";
        Persona persona = new Persona();
        if (tutor != null) {
            System.out.println("llegooooooooooooooo");
            persona.setId(tutor.getId());
            persona.setRol(tutor.getRol().getId());
            session.setAttribute("persona", persona);
            res = "redirect:/tutorhome.htm?id="+tutor.getId();
        } else {
            estudiante = dao.UsuarioDao.getEstudiante(cedula, clave);
            if (estudiante != null) {
                persona.setId(estudiante.getId());
                persona.setRol(estudiante.getRol().getId());
                session.setAttribute("persona", persona);
                res = "redirect:/estudiantehome.htm?id="+estudiante.getId();
            } else {
                secretaria = dao.UsuarioDao.getSecretaria(cedula, clave);
                if (secretaria != null) {
                    persona.setId(secretaria.getId());
                    persona.setRol(secretaria.getRol().getId());
                    session.setAttribute("persona", persona);
                    res = "redirect:/secretariahome.htm?id="+secretaria.getId();
                }
                else {
                    administrador = dao.UsuarioDao.getAdministrador(cedula, clave);
                    if (administrador != null) {
                        persona.setId(administrador.getId());
                        persona.setRol(administrador.getRol().getId());
                        session.setAttribute("persona", persona);
                        res = "redirect:/administradorhome.htm?id="+administrador.getId();
                    }
                }
            }
        }
        return res;
    }
}
