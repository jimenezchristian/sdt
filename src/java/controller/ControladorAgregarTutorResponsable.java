/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateUtil;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Datostutor;
import model.Estudiante;
import org.hibernate.Session;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("dtutornew.htm")
public class ControladorAgregarTutorResponsable {
    private JdbcTemplate jdbctemplate;
    public ControladorAgregarTutorResponsable() {
        Conectar con = new Conectar();
        this.jdbctemplate = new JdbcTemplate(con.conectar());
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView registro(HttpServletRequest http) {
        System.out.println("--------------------------------------------------------");
        int id = Integer.parseInt(http.getParameter("id"));
        System.out.println("id: "+id);
        System.out.println("*****************************************************");
        ModelAndView modelo = new ModelAndView();
        modelo.setViewName("dtutornew");
        modelo.addObject("datostutor", new Datostutor());
        modelo.addObject("idTutor", id);
        return modelo;
    }
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView agregarDatos(@ModelAttribute("datostutor") Datostutor dt, HttpServletRequest http) {
        int id = Integer.parseInt(http.getParameter("id"));
        System.out.println("id fiel: "+id);
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Datostutor nuevoResp = new Datostutor();
//        nuevoResp.setId(1);
        nuevoResp.setNombre(dt.getNombre());
        nuevoResp.setApellido(dt.getApellido());
        nuevoResp.setCedula(dt.getCedula());
        nuevoResp.setPeriodoresponsable(dt.getPeriodoresponsable());
        nuevoResp.setIdtutorFk(id);
        try {
        //Save the employee in database
        session.save(nuevoResp);
        //Commit the transaction
        session.getTransaction().commit();
        System.out.println("sql: "+session.getTransaction().toString());
          
        } catch (Exception e) {
            System.out.println("error al guardar: "+e.getMessage());
        }
        return new ModelAndView("redirect:/tutorhome.htm?id=" + id);  
    }
}