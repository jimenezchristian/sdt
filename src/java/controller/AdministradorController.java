/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.Menu;
import model.Menurol;
import model.Administrador;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
/**
 *
 * @author user
 */
@Controller
public class AdministradorController {
    @RequestMapping(value = "administradorhome.htm")
    public ModelAndView viewHome(HttpServletRequest http) {
        int id=Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();
        Administrador a=dao.AdministradorDao.getAdministrador(id);
         
         modelo.addObject("usuario", a.getNombrecompleto());
        modelo.addObject("id", id);
        return modelo;
    }
    @ModelAttribute("menuList")
    public List<Menu> getMenu() {
        List<Menu> menus=new ArrayList<>();
        for (Menurol i : dao.MenuRolDao.getMunus(1)) {
            Menu menu=dao.MenuDao.getId(i.getMenu().getId());
            menus.add(menu);
        }
        return menus;
    }
}
