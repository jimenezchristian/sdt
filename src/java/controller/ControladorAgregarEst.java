/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateUtil;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import model.Conectar;
import model.Estudiante;
import model.Items;
import model.Rol;
import model.Tutor;
import org.hibernate.Session;
import org.recursos.clasesauxi.UtilsSecure;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller
@RequestMapping("estudiantenew.htm")
public class ControladorAgregarEst {
    private JdbcTemplate jdbctemplate;
    public ControladorAgregarEst(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }   
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView registro(HttpServletRequest http){
         int id=Integer.parseInt(http.getParameter("id"));
         
         ModelAndView modelo = new ModelAndView();
        modelo.setViewName("estudiantenew");
        modelo.addObject("estudiante",new Estudiante());
        modelo.addObject("id",id);
        
        return modelo;
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView agregarDatos(@ModelAttribute("estudiante") Estudiante e, HttpServletRequest http){
        int id=Integer.parseInt(http.getParameter("id"));
       Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        //Add new Employee object
        Rol rol= new Rol();
        session.load(rol, 4);
        rol.setId(4);
        Estudiante nuevoest = new Estudiante();
        nuevoest.setNombre(e.getNombre());
        nuevoest.setApellido(e.getApellido());
         nuevoest.setCedula(e.getCedula());
         nuevoest.setFolio(e.getFolio());
         nuevoest.setMatricula(e.getMatricula());
         nuevoest.setDireccion(e.getDireccion());
         nuevoest.setCarrera(e.getCarrera());
//         nuevoest.setFechaingresocarrera(e.getFechaingresocarrera());
         nuevoest.setPeriodomatricula(e.getPeriodomatricula());
         nuevoest.setModalidad(e.getModalidad());
         nuevoest.setNivelestudios(e.getNivelestudios());
         nuevoest.setConvencional(e.getConvencional());
         nuevoest.setMovil(e.getMovil());
         nuevoest.setCorreo(e.getCorreo());
         nuevoest.setGenero(e.getGenero());
         nuevoest.setEtnia(e.getEtnia());
         nuevoest.setDiscapacidad(e.getDiscapacidad());
         nuevoest.setClave(UtilsSecure.Encriptar(e.getClave()));
         nuevoest.setTipo(e.getTipo());
         nuevoest.setRol(rol);
         
         String sql="select id from tutor where carrera = "+e.getCarrera();
         List<Map<String, Object>> listidTutor=this.jdbctemplate.queryForList(sql);
            int idtutor= Integer.parseInt(listidTutor.get(0).get("id").toString());
         
         
         Tutor t= new Tutor();
         
         session.load(t, idtutor);
         
         t.setId(idtutor);
         nuevoest.setTutor(t);
         
        //Save the employee in database
        session.save(nuevoest);
 
        //Commit the transaction
        session.getTransaction().commit();
        return new ModelAndView("redirect:/secretariahome.htm?id="+id);
    }
    @ModelAttribute("modalidadList")

    public Map<Integer, String> getModalidades() {
        Map<Integer, String> modalidades = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getModalidades()) {
            modalidades.put(i.getId(), i.getNombre());
        }
        return modalidades;
    }

    @ModelAttribute("carreraList")
    public Map<Integer, String> getCarreras() {
        Map<Integer, String> carreras = new LinkedHashMap<>();
        for (Tutor i : dao.TutorDao.getCarreras()) {
            carreras.put(i.getCarrera(), i.getNombrecompleto());
        }
        return carreras;
    }

    @ModelAttribute("nivelestudiosList")
    public Map<Integer, String> getNivelEstudios() {
        Map<Integer, String> nivelestudios = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getNivelEstudios()) {
            nivelestudios.put(i.getId().intValue(), i.getNombre());
        }
        return nivelestudios;
    }

    @ModelAttribute("periodosList")
    public Map<Integer, String> getPeriodos() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getPeriodos()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }
    
    
@ModelAttribute("tipoList")
    public Map<Integer, String> getTipo() {
        Map<Integer, String> tipo = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getTipo()) {
            tipo.put(i.getId(), i.getNombre());
        }
        return tipo;
    }
    
    @ModelAttribute("generoList")
    public Map<String, String> getGenero() {
        Map<String, String> genero = new LinkedHashMap<>(); 
        genero.put("F", "Femenino");
        genero.put("M", "Masculino");        
        return genero;
    }
        @ModelAttribute("disList")
    public Map<String, String> getDiscapacidad() {
        Map<String, String> disc = new LinkedHashMap<>(); 
        disc.put("Si", "Si");
        disc.put("No", "No");        
        return disc;
    }
    @ModelAttribute("etniasList")
    public Map<String, String> getEtnias() {
        Map<String, String> disc = new LinkedHashMap<>(); 
        disc.put("Mestizo", "Mestizo");
        disc.put("Indígena", "Indígena");        
        disc.put("Afroecuatoriano", "Afroecuatoriano");        
        return disc;
    }

}
