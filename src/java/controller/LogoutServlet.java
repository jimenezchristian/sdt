/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        request.getRequestDispatcher("login.htm").include(request, response);
        HttpSession session = request.getSession();
        session.invalidate();
        
           out.println("<script type=\"text/javascript\">");
   out.println("alert('Cerrando sesión...');");
   out.println("location='login.htm';");
   out.println("</script>");
////            request.ge
//        int status = response.getStatus();
//        System.out.println("status: " + status);
////            out.print("You are successfully logged out!");
//        out.println("<script type=\"text/javascript\">");
//        out.println("confirm('Desea salir');");
//        out.println("</script>");
//
//        response.sendRedirect("login.htm");
//        out.close();

    }
}
