/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Secretaria;
import model.Items;
import model.Tutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
/**
 *
 * @author user
 */
public class ControladorGestionSecr {
    private JdbcTemplate jdbctemplate;
   
    public ControladorGestionSecr(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }
    @RequestMapping(value = "gestionarSecretaria.htm",method = RequestMethod.GET)
    public ModelAndView viewSecretariaNew(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
         
        ModelAndView modelo= new ModelAndView();
        
        String sql= "SELECT * from secretaria";
        
        List datossecre=this.jdbctemplate.queryForList(sql);
        
        modelo.addObject("datos",datossecre);
        modelo.addObject("id", id);
        
        return modelo;
    
    }
    @RequestMapping(value = "editarSecretaria.htm",method = RequestMethod.GET)
    
    public ModelAndView editar(HttpServletRequest http){
        ModelAndView modelo= new ModelAndView();
        int id= Integer.parseInt(http.getParameter("id"));
        int idSecr= Integer.parseInt(http.getParameter("idSecr"));
        
        Secretaria datos=this.seleccionarSecretaria(idSecr);
        
        
        modelo.addObject("secretariaEditado", new Secretaria( datos.getNombrecompleto(), datos.getCedula(), datos.getClave()));
        modelo.addObject("id",id);
        
        return modelo;
    }
    
    
    //Agregar datos
     @RequestMapping(value = "editarSecretaria.htm",method = RequestMethod.POST)
    public ModelAndView editarDatos(@ModelAttribute("secretariaEditado") Secretaria s,HttpServletRequest http ){
           int id= Integer.parseInt(http.getParameter("id"));
        int idSecr= Integer.parseInt(http.getParameter("idSecr"));
        
        ModelAndView modelo= new ModelAndView();
        

        this.jdbctemplate.update(
            "update secretaria "
            +"set nombrecompleto=?,"     
             +" cedula=?,"
            +" clave=? "    
            +" where id= ?"   , 
             s.getNombrecompleto(),s.getCedula(),s.getClave(),idSecr );
        
        
            return new ModelAndView("redirect:/administradorhome.htm?id="+id);
        
        
    }
    
    
    @RequestMapping(value = "eliminarSecretaria.htm",method = RequestMethod.GET)
     public ModelAndView eliminar(HttpServletRequest http){
         int id=Integer.parseInt(http.getParameter("id"));
          int idSecr= Integer.parseInt(http.getParameter("idSecr"));
         ModelAndView modelo = new ModelAndView();
         
         this.jdbctemplate.update(
         "delete from secretaria where id=?",
         idSecr
         );
         modelo.addObject("idSecr", idSecr);
        return new ModelAndView("redirect:/administradorhome.htm?id="+id);
     }

    private Secretaria seleccionarSecretaria(int idSecr) {
         final Secretaria listaUsuarios= new Secretaria();
        String consulta="select * from secretaria WHERE id="+idSecr;
        
            
       
         return (Secretaria) jdbctemplate.query(consulta, new ResultSetExtractor<Secretaria>(){
            public Secretaria extractData(ResultSet rs) throws SQLException{
                if(rs.next()){
                    listaUsuarios.setNombrecompleto(rs.getString("nombrecompleto"));
                    listaUsuarios.setCedula(rs.getInt("cedula"));
                    listaUsuarios.setClave(rs.getString("clave"));
                   
                   
                    
                }
                return listaUsuarios;
            }   
        });
    }
    
    
}
