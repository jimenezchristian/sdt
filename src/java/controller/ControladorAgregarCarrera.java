/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import dao.HibernateUtil;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import model.Catalogo;
import model.Conectar;
import model.Estudiante;
import model.Items;
import model.Rol;
import model.Secretaria;
import model.Tutor;
import org.hibernate.Session;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
/**
 *
 * @author user
 */
@Controller
@RequestMapping("datosCarrera.htm")
public class ControladorAgregarCarrera {
    private JdbcTemplate jdbctemplate;
    public ControladorAgregarCarrera() {
        Conectar con = new Conectar();
        this.jdbctemplate = new JdbcTemplate(con.conectar());
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView registro(HttpServletRequest http){
         int id=Integer.parseInt(http.getParameter("id"));
         String idNombre = http.getParameter("idNombre");
         ModelAndView modelo = new ModelAndView();
//         String sql= "SELECT * from Items";
//        
//        List datositem=this.jdbctemplate.queryForList(sql);
         //Items datos=this.seleccionarItem(idNombre);
        modelo.setViewName("datosCarrera");
        modelo.addObject("carrera",new Tutor());
        //modelo.addObject("carrera",new Items(datos.getId()));
//        modelo.addObject("datos",datositem);
        modelo.addObject("id",id);
        
        return modelo;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView agregarDatos(@ModelAttribute("carrera") Tutor t, HttpServletRequest http){
        ModelAndView modelo= new ModelAndView();    
        int id = Integer.parseInt(http.getParameter("id"));
        String idNombre = http.getParameter("idNombre");
        int idItem = Integer.parseInt(http.getParameter("idItem"));
        
//        int idItem ;
//        Items datos=this.seleccionarItem(idNombre);
//        idItem=datos.getId();
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Rol rol = new Rol();
        session.load(rol, 2);
        rol.setId(2);
        Tutor nuevotutor = new Tutor();
        nuevotutor.setNombrecompleto(idNombre);
        nuevotutor.setCedula(t.getCedula());
        nuevotutor.setCarrera(idItem);
        nuevotutor.setClave(t.getClave());
        nuevotutor.setRol(rol);
        //Save the employee in database
        session.save(nuevotutor);
        //Commit the transaction
        session.getTransaction().commit();
//        System.out.println("sql: "+session.getTransaction().toString());
//        
          
        return new ModelAndView("redirect:/administradorhome.htm?id=" + id );  
    }
//    private Items seleccionarItem(String idNombre) {
//         final Items listaUsuarios= new Items();
//        String consulta="select id from Items WHERE nombre = '"+idNombre+"'";
//        
//            
//       
//         return (Items) jdbctemplate.query(consulta, new ResultSetExtractor<Items>(){
//            public Items extractData(ResultSet rs) throws SQLException{
//                if(rs.next()){
//                    //listaUsuarios.setNombre(rs.getString("nombre"));
//                    listaUsuarios.setId(rs.getInt("idItem"));
//
//                }
//                return listaUsuarios;
//            }   
//        });
//    }
}
