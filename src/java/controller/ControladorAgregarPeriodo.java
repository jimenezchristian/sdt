/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.sql.Statement;
import dao.HibernateUtil;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import model.Catalogo;
import model.Conectar;
import model.Items;
import model.Rol;
import model.Periodomatricula;
import model.Periodosunidadtitulacion;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
/**
 *
 * @author user
 */
@Controller
@RequestMapping("periodonew.htm")
public class ControladorAgregarPeriodo {
    private JdbcTemplate jdbctemplate;
    public ControladorAgregarPeriodo(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }   
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView registro(HttpServletRequest http){
         int id=Integer.parseInt(http.getParameter("id"));
         
         ModelAndView modelo = new ModelAndView();
        modelo.setViewName("periodonew");
        modelo.addObject("items",new Items());
//        modelo.addObject("periodomatricula",new Periodomatricula());
//        modelo.addObject("periodosunidadtitulacion",new Periodosunidadtitulacion());
        modelo.addObject("id",id);
        
        return modelo;
    }
    
   @RequestMapping(method = RequestMethod.POST)
   public ModelAndView agregarDatos(@ModelAttribute("items") Items i, HttpServletRequest http){
       int idItem ;
       int id=Integer.parseInt(http.getParameter("id"));
       Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        //Add new Secretary object
//        Rol rol= new Rol();
//        session.load(rol, 3);
//        rol.setId(3);
        Periodomatricula pm = new Periodomatricula();
        Catalogo catalogo = new Catalogo();
        session.load(catalogo, 2);
        catalogo.setId(2);
        Items nuevoitem = new Items();
        nuevoitem.setCatalogo(catalogo);
        nuevoitem.setNombre(i.getNombre());
        nuevoitem.setFechaingresocarrera(i.getFechaingresocarrera());
        nuevoitem.setActivo(true);
        
        
        //Save the secretary in database
        session.save(nuevoitem);
        this.jdbctemplate.update(
            "insert into periodomatricula (periodomatricula, fechaingresocarrera) values (?,?)",
             i.getNombre(), i.getFechaingresocarrera()
        );
        this.jdbctemplate.update(
            "insert into periodosunidadtitulacion (nombre) values (?)",
             i.getNombre()
        );
        this.jdbctemplate.update(
            "insert into periodograduacion (periodograduacion) values (?)",
             i.getNombre()
        );
        
        //Commit the transaction
        session.getTransaction().commit();
//        String sql= "SELECT id from items WHERE nombre = '"+i.getNombre()+"'";
//        //int datosper=this.jdbctemplate.queryForInt(sql);
//
//        //SqlParameterSource NamedParameters = new MapSqlParameterSource ( "nombre", hostTemplateId);
//        String datosper=this.jdbctemplate.queryForObject(sql, String.class);
//        idItem=Integer.parseInt(datosper);
        
        
        return new ModelAndView("redirect:/administradorhome.htm?id="+id);
    }

}
