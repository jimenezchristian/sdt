/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import dao.HibernateUtil;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import model.Catalogo;
import model.Conectar;
import model.Estudiante;
import model.Items;
import model.Rol;
//import model.Tutor;
import org.hibernate.Session;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
/**
 *
 * @author user
 */
@Controller
@RequestMapping("tutornew.htm")
public class ControladorAgregarTutor {
    private JdbcTemplate jdbctemplate;
    public ControladorAgregarTutor() {
        Conectar con = new Conectar();
        this.jdbctemplate = new JdbcTemplate(con.conectar());
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView registro(HttpServletRequest http){
         int id=Integer.parseInt(http.getParameter("id"));
         
         ModelAndView modelo = new ModelAndView();
        modelo.setViewName("tutornew");
        modelo.addObject("tutor",new Items());
        modelo.addObject("id",id);
        
        return modelo;
    }
   
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView agregarDatos(@ModelAttribute("items") Items i, HttpServletRequest http){
        ModelAndView modelo= new ModelAndView();    
        int id = Integer.parseInt(http.getParameter("id"));
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Catalogo catalogo = new Catalogo();
        session.load(catalogo, 1);
        catalogo.setId(1);
        Items nuevoitem = new Items();
        nuevoitem.setCatalogo(catalogo);
        nuevoitem.setNombre(i.getNombre());
        nuevoitem.setActivo(true);
        
        //Save the employee in database
        session.save(nuevoitem);
        //Commit the transaction
        session.getTransaction().commit();
//        System.out.println("sql: "+session.getTransaction().toString());
         String sql="select id from items where nombre = '"+i.getNombre()+"'";
         List<Map<String, Object>> listidItem=this.jdbctemplate.queryForList(sql);
            int iditem = Integer.parseInt(listidItem.get(0).get("id").toString());
          
        return new ModelAndView("redirect:/datosCarrera.htm?id=" + id + "&idNombre= " + i.getNombre() + "&idItem=" + iditem);  
    }
//    @ModelAttribute("carreraList")
//    public Map<Integer, String> getCarreras() {
//        Map<Integer, String> carreras = new LinkedHashMap<>();
//        for (Tutor i : dao.TutorDao.getCarreras()) {
//            carreras.put(i.getCarrera(), i.getNombrecompleto());
//        }
//        return carreras;
//    }
}
