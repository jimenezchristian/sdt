/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Menu;
import model.Menurol;
import model.ProcesosComplexivo;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
public class ProcesoComplexivo_Controller {
     private JdbcTemplate jdbctemplate;
      public ProcesoComplexivo_Controller(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }
    @RequestMapping( value = "procesoscomplexivo.htm",method = RequestMethod.GET)
    public ModelAndView viewProcesosNew(HttpServletRequest http) {
        ModelAndView modelo= new ModelAndView();
        int id=Integer.parseInt(http.getParameter("id"));
        modelo.addObject("procesoscomplexivo",new ProcesosComplexivo());
        
        modelo.addObject("id",id);
        return modelo;

    }        
    
    
    @ModelAttribute("menuList")
    public List<Menu> getMenu() {
        List<Menu> menus = new ArrayList<>();
        for (Menurol i : dao.MenuRolDao.getMunus(1)) {
            Menu menu = dao.MenuDao.getId(i.getMenu().getId());
            menus.add(menu);
        }
        return menus;
    }
    @RequestMapping(value = "procesoscomplexivo.htm",method = RequestMethod.POST)
    public ModelAndView agregarDatos(@ModelAttribute("procesoscomplexivo") ProcesosComplexivo p, HttpServletRequest http ) throws Exception{
        
        ModelAndView modelo= new ModelAndView();
         int id= Integer.parseInt(http.getParameter("id"));
        
       InputStream inputStream = null;
        CommonsMultipartFile uploaded=p.getArchivo();
       inputStream=uploaded.getInputStream();
        this.jdbctemplate.update(
            "insert into procesos_complexivo (nombreProceso, archivo,nombreArchivo,fase) values (?,?,?,?)",
             p.getNombreProceso(),inputStream,p.getArchivo().getOriginalFilename(),p.getFase()
        );
        return new ModelAndView("redirect:/administradorhome.htm?id="+id);
    }
    //gestionar Procesos
    @RequestMapping("gestionarProcesosComplexivo.htm")
    public ModelAndView viewListaProcesos(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo= new ModelAndView();
        String sql= "select * from procesos_complexivo";
        List datosProcesos=this.jdbctemplate.queryForList(sql);
        
        modelo.addObject("datos",datosProcesos);
        modelo.addObject("id", id);
        return modelo;
    
    }
    
    @RequestMapping(value = "editarProComplexivo.htm",method = RequestMethod.GET)
    
    public ModelAndView editar(HttpServletRequest http){
        ModelAndView modelo= new ModelAndView();
        int id= Integer.parseInt(http.getParameter("id"));
        int idSec= Integer.parseInt(http.getParameter("idSec"));
        ProcesosComplexivo datos=this.seleccionarProceso(id);
        
        
        modelo.addObject("procesoEditado", new ProcesosComplexivo(datos.getNombreProceso()));
        modelo.addObject("idPro",id);
        modelo.addObject("id",idSec);
        return modelo;
    }
    
    
    @RequestMapping(value = "editarProComplexivo.htm",method = RequestMethod.POST)
    public ModelAndView editarDatos(@ModelAttribute("procesoEditado") ProcesosComplexivo p,HttpServletRequest http ) throws IOException{
           int id= Integer.parseInt(http.getParameter("id"));
        int idSec= Integer.parseInt(http.getParameter("idSec"));
        InputStream inputStream = null;
           CommonsMultipartFile uploaded=p.getArchivo();
       inputStream=uploaded.getInputStream();
        ModelAndView modelo= new ModelAndView();
        
        this.jdbctemplate.update(
            "update procesos_complexivo "
            +"set nombreProceso=?,"    
            +" archivo=?,"    
            +"nombreArchivo=?"
            +" where id= ?"   , 
             p.getNombreProceso(),inputStream,p.getArchivo().getOriginalFilename(),id
        );
        return new ModelAndView("redirect:/administradorhome.htm?id="+idSec);
    }
    
    @RequestMapping(value = "eliminarProComplexivo.htm",method = RequestMethod.GET)
     public ModelAndView eliminar(HttpServletRequest http){
         int id=Integer.parseInt(http.getParameter("id"));
          int idSec= Integer.parseInt(http.getParameter("idSec"));
         ModelAndView modelo = new ModelAndView();
         
         this.jdbctemplate.update(
         "delete from procesos_complexivo where id=?",
         id
         );
         
        return new ModelAndView("redirect:/administradorhome.htm?id="+idSec);
     }
    
     private ProcesosComplexivo seleccionarProceso(int id) {
         final ProcesosComplexivo listaProceso= new ProcesosComplexivo();
        String consulta="select * from procesos_complexivo WHERE id="+id;
        
            
       
         return (ProcesosComplexivo) jdbctemplate.query(consulta, new ResultSetExtractor<ProcesosComplexivo>(){
            public ProcesosComplexivo extractData(ResultSet rs) throws SQLException{
                if(rs.next()){
                    listaProceso.setNombreProceso(rs.getString("nombreProceso"));                    
                    
                }
                return listaProceso;
            }   
        });
    }
    @ModelAttribute("faseList")
    public Map<String, String> getFase() {
        Map<String, String> fase = new LinkedHashMap<>(); 
        fase.put("1", "1");
        fase.put("2", "2");
        fase.put("3", "3");        
        fase.put("4", "4");
        return fase;
    }
}
