/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import com.itextpdf.text.DocumentException;
import com.mysql.jdbc.Connection;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Conectar;
import model.Items;
import model.Tutor;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Usuario
 */




public class ControladorLisEstSecComplex {
    private JdbcTemplate jdbctemplate;
        private int per=0;
        private int op=0;
    static Connection conn = null;
        private String fas=""; //aucpiña
    
    public ControladorLisEstSecComplex(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }
    
        @RequestMapping(value = "listaPeriodosEstuSecComplex.htm" , method = RequestMethod.GET)
    public ModelAndView viewEstudianteSec(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo= new ModelAndView();
        int rol=Integer.parseInt(http.getParameter("rol"));
        
        modelo.addObject("id",id);
        modelo.addObject("rol",rol);
        return modelo;
    }
    
    
    @RequestMapping(value = "listaPeriodosEstuSecComplex.htm" , method = RequestMethod.POST)
    public ModelAndView viewEstudianteSecTit(HttpServletRequest http) throws FileNotFoundException {
        int haydatos=0;
         int id=Integer.parseInt(http.getParameter("id"));
         
         int periodo=Integer.parseInt(http.getParameter("periodo"));
         int opcion=Integer.parseInt(http.getParameter("opcion"));         
           
         int fase=Integer.parseInt(http.getParameter("fases"));         
         String filtrado="";
         per=periodo;
         op=opcion;
         this.fas=String.valueOf(fase);
         //fas=String.valueOf(fase); //aucapiña
         String sqlRevision="";
         int rol=Integer.parseInt(http.getParameter("rol"));
         if(rol==1){
             if (opcion == 1) {
                 sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombrecompleto as opcional, p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON (e.tutor =" + id + " and e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN Tutor i ON i.carrera = e.carrera";
                 filtrado = "Carrera";
             } else {
                 if (opcion == 2) {
                     sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombre as opcional, p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON (e.tutor =" + id + " and e.periodomatricula=" + periodo + " and e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = e.periodomatricula";
                     filtrado = "Período de Matricula";
                 } else {
                     if (opcion == 3) {
                         sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,"
                        + "e.cedula as cedula,i.nombre as opcional, p.temaproyecto as tema "
                        + "from estudiante  e INNER JOIN proyectoinves p ON (e.tutor =" + id
                        + " and p.periodoingresounidadtitulacion=" + periodo + " and e.tipo=25 ) and "
                        + "(e.id=p.estudiante) INNER JOIN Items i ON i.id = p.periodoingresounidadtitulacion";
                System.out.println("sql de titulacion: " + sqlRevision);
                         filtrado = "Período de Ingreso a Unidad de Titulación";
                     } else {
                         if (opcion == 4) {
                             sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombre as opcional, p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON (e.tutor =" + id + " and p.periodograduacion=" + periodo + " and e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = p.periodograduacion";
                             filtrado = "Período de Graduación";
                         }
                         else {
                         if (opcion == 5) {
                             
                             sqlRevision = "select distinct e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.tipofase as opcional, p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON (e.tutor =" + id + " and e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN procesoproyectos i ON i.tipofase= "+fase+ " and i.estudiante=e.id";
                             
                             filtrado = "N° de Fase";
                         }
                     }
                     }
                 }

             }
         }
         else{
             int carrera=Integer.parseInt(http.getParameter("carreras"));         
             if (opcion == 1) {
                 sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombrecompleto as opcional, p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON (e.carrera=" + carrera + " and e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN Tutor i ON i.carrera = e.carrera";
                 filtrado = "Carrera";
             } else {
                 if (opcion == 2) {
                     sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombre as opcional, p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON ( e.periodomatricula=" + periodo + " and e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = e.periodomatricula";
                     filtrado = "Período de Matricula";
                 } else {
                     if (opcion == 3) {
                         sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombre as opcional,"
                        + " p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON "
                        + "(p.periodoingresounidadtitulacion=" + periodo + " and e.tipo=25 ) and (e.id=p.estudiante) "
                        + "INNER JOIN Items i ON i.id = p.periodoingresounidadtitulacion";
                         filtrado = "Período de Ingreso a Unidad de Titulación";
                     } else {
                         if (opcion == 4) {
                             sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombre as opcional, p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON (p.periodograduacion=" + periodo + " and e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = p.periodograduacion";
                             filtrado = "Período de Graduación";
                         }else{
                             if (opcion == 5) {
                             sqlRevision = "select distinct e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.tipofase as opcional, p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON (e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN procesoproyectos i ON i.tipofase= "+fase+ " and i.estudiante=e.id";
                             filtrado = "N° de Fase";
                         }
                         }
                     }
                 }

             }
         }
         
         
        ModelAndView modelo= new ModelAndView();
        
        try{
            List datosestudiantes=this.jdbctemplate.queryForList(sqlRevision);

            if(datosestudiantes.size()>0){
                haydatos=1;
            }
            modelo.addObject("rol",rol);
            modelo.addObject("datos",datosestudiantes);
            modelo.addObject("id",id);
            modelo.addObject("existe",haydatos);
            modelo.addObject("filtrado",filtrado);
            return modelo;
        }
        catch (Exception e){
            modelo.addObject("rol",rol);
            modelo.addObject("id",id);
            modelo.addObject("existe",haydatos);
            modelo.addObject("filtrado",filtrado);
            return modelo;
        }
    }
    
    
    //---------------------------------------------------------------------------------------
   
  
  
    
    
    
//    @RequestMapping(value = "reporteGraduacion.htm", method = RequestMethod.GET)
//    @ResponseBody
//    public ModelAndView descargarPDFTitul(HttpServletRequest http, HttpServletResponse response) throws FileNotFoundException, JRException, IOException, DocumentException, SQLException {
//        int id = Integer.parseInt(http.getParameter("id"));
//        ModelAndView modelo = new ModelAndView();
//       
//        InputStream entrada = null;
//        Map<String, Object> params = null;
//        try {
//            conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/bd_sdt", "root", "");
//            conn.setAutoCommit(false);
//            if (opciones == 3) {
//                entrada = this.getClass().getResourceAsStream("/reportes/reportEstu.jrxml");
//                params = new HashMap<>();
//                params.put("idPeriodoT", per);
//                params.put("idTutor", id);
//
//            }  if (opciones == 4) {
//
//                entrada = this.getClass().getResourceAsStream("/reportes/ReportePeriodoGraduacion.jrxml");
//                params = new HashMap<>();
//                params.put("idtutor", new Integer(id)); //
//                params.put("periodo", per); //entre comillas tu parametro del reporte en mi caso es periodo
//            }
//            JasperReport jasperReport = JasperCompileManager.compileReport(entrada);
//            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
//            response.setContentType("application/x-pdf");
//            response.setHeader("Content-disposition", "inline; filename=Report.pdf");
//            final OutputStream outStream = response.getOutputStream();
//            JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
//        } catch (SQLException | JRException | IOException e) {
//            System.out.println("Error de conexión:...................................................................\n " + e.getMessage());
//            System.exit(4);
//        }
//
//        modelo.addObject("id", id);
//        modelo.addObject("periodo", per);
//        return modelo;
//    }
    
 ////////////////////////////////////////////////////////   
    @RequestMapping(value = "reportepdftitulacion.htm", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView descargarPDFTitul(HttpServletRequest http, HttpServletResponse response) throws FileNotFoundException, JRException, IOException, DocumentException, SQLException {
        int id = Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();
        System.out.println("********************************************************");
        System.out.println("********************************************************");
        System.out.println("********************************************************");
        System.out.println("********************************************************");
        System.out.println("********************************************************");
        System.out.println("\tTutor: " + id + "\n\tPeriodo: " + per + "\n\tFase: " + fas);
        System.out.println("********************************************************");
        System.out.println("********************************************************");
        System.out.println("********************************************************");
        System.out.println("********************************************************");
        System.out.println("********************************************************");
        InputStream entrada = null;
        Map<String, Object> params = null;
        try {
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/bd_sdt", "root", "");
            conn.setAutoCommit(false);
            if (op!=0) {
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                
            if (op == 3) {
                entrada = this.getClass().getResourceAsStream("/reportes/reportEstu.jrxml");
                params = new HashMap<>();
                params.put("idPeriodoT", per);
                params.put("idTutor", id);

            } else if (op == 4) {

                entrada = this.getClass().getResourceAsStream("/reportes/ReportePeriodoGraduacion.jrxml");
                params = new HashMap<>();
                params.put("idtutor", new Integer(id)); //
                params.put("periodo", per); //entre comillas tu parametro del reporte en mi caso es periodo
            } else if (op == 5) {
                entrada = this.getClass().getResourceAsStream("/reportes/Fase.jrxml");
                params = new HashMap<>();
                params.put("tutor", id);
                params.put("Fase", fas);
            }else{
                System.out.println("Ocurrio un error al descargar el reporte: ");
            }
            JasperReport jasperReport = JasperCompileManager.compileReport(entrada);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            response.setContentType("application/x-pdf");
            response.setHeader("Content-disposition", "inline; filename=Report.pdf");
            final OutputStream outStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);

            }else{
                System.out.println("No existe datos");
            }
        } catch (SQLException | JRException | IOException e) {
            System.out.println("Error de conexión:...................................................................\n " 
                    + e.getMessage());
//            System.exit(4);
        }

        modelo.addObject("id", id);
        modelo.addObject("periodo", per);
        return modelo;
    }
    
    
    
    
    
    
   ///////////////////////////////////////////////////// 
    
    @RequestMapping(value = "reportePDF.htm", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView descargarPDF(HttpServletRequest http, HttpServletResponse response) throws FileNotFoundException, JRException, IOException, DocumentException, SQLException {
        int id = Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();
     InputStream entrada = this.getClass().getResourceAsStream("/reportes/matriculados.jrxml");
    Map<String,Object> params = new HashMap<>();
    params.put("idtutor",new Integer(id));
       JasperReport jasperReport = JasperCompileManager.compileReport(entrada);
    try {
      conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/bd_sdt","root", "");
      conn.setAutoCommit(false);
    }
    catch (SQLException e) {
      System.out.println("Error de conexión: " + e.getMessage());
      System.exit(4);
    }
    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);

    response.setContentType("application/x-pdf");
    response.setHeader("Content-disposition", "inline; filename=ReporteEstudiantes.pdf");
    final OutputStream outStream = response.getOutputStream();
    JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
    //JasperExportManager.exportReportToPdfFile( jasperPrint, "C:/Users/USUARIO/Downloads/reportesdt.pdf");
        modelo.addObject("id", id);
        return modelo;
   
    }
        

    
    ///////////DIEGO
//     @RequestMapping(value = "reportepdftitulacion.htm", method = RequestMethod.GET)
//    @ResponseBody
//    public ModelAndView descargarPDFTitul(HttpServletRequest http, HttpServletResponse response) throws FileNotFoundException, JRException, IOException, DocumentException, SQLException {
//        
//        int id = Integer.parseInt(http.getParameter("id"));
////        int periodo = Integer.parseInt(http.getParameter("periodo"));
//        System.out.println("-----------------------------------------------------------------");
//        System.out.println("-----------------------------------------------------------------");
////        System.out.println("idutor: " + id + "  idperiodoT: " + periodo);
//        ModelAndView modelo = new ModelAndView();
//        InputStream entrada = this.getClass().getResourceAsStream("/reportes/reportEstu.jrxml");
//        Map<String, Object> params = new HashMap<>();
//        params.put("idPeriodoT", per);
//        params.put("idTutor", new Integer(id));
//        JasperReport jasperReport = JasperCompileManager.compileReport(entrada);
//        try {
//            conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/bd_sdt", "root", "");
//            conn.setAutoCommit(false);
//            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
//
//            response.setContentType("application/x-pdf");
//            response.setHeader("Content-disposition", "inline; filename=ReporteUnidadTitulacion.pdf");
//            final OutputStream outStream = response.getOutputStream();
//            JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
//        } catch (SQLException e) {
//            System.out.println("Error de conexión: " + e.getMessage());
//            System.exit(4);
//        }
//
//        //JasperExportManager.exportReportToPdfFile( jasperPrint, "C:/Users/USUARIO/Downloads/reportesdt.pdf");
//        modelo.addObject("id", id);
//        modelo.addObject("periodo", per);
//        return modelo;
//       
//    }
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    //////////////////////////////////////////////////////////////////////////////////////////////////////////
//    
//    
//    //VALLEJO
//    ///PERIODO DE GRADUACION 
//    @RequestMapping(value = "reporteGraduacion.htm", method = RequestMethod.GET)
//    @ResponseBody
//    public ModelAndView descargarPeriodoGraduacionPDF(HttpServletRequest http, HttpServletResponse response) throws FileNotFoundException, JRException, IOException, DocumentException, SQLException {
//        int id = Integer.parseInt(http.getParameter("id")); //
//        //int idP = Integer.valueOf(http.getParameter("idP"));
//        ModelAndView modelo = new ModelAndView();
//
//     InputStream entrada = this.getClass().getResourceAsStream("/reportes/ReportePeriodoGraduacion.jrxml");
//    Map<String,Object> params = new HashMap<>();
//    params.put("idtutor",new Integer(id)); //
//    params.put("periodo",per); //entre comillas tu parametro del reporte en mi caso es periodo
//       JasperReport jasperReport = JasperCompileManager.compileReport(entrada);
//    try {
//      conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/bd_sdt","root", "");
//      conn.setAutoCommit(false);
//    }
//    catch (SQLException e) {
//      System.out.println("Error de conexión: " + e.getMessage());
//      System.exit(4);
//    }
//    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
//
//    response.setContentType("application/x-pdf");
//    response.setHeader("Content-disposition", "inline; filename=PeriodoGraduacion.pdf");
//
//    final OutputStream outStream = response.getOutputStream();
//    JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
//    //JasperExportManager.exportReportToPdfFile( jasperPrint, "C:/Users/USUARIO/Downloads/reportesdt.pdf");
//
//        modelo.addObject("id", id); //
//        modelo.addObject("perido", per); //
//        return modelo;
//       
//    }
//    
//    
//    
//    ///AUCAPIÑA
//    // reporte de Fases
//    
//    @RequestMapping(value = "reportefPDF.htm", method = RequestMethod.GET)
//    @ResponseBody
//    public ModelAndView FasesdescargarPDF(HttpServletRequest http, HttpServletResponse response) throws FileNotFoundException, JRException, IOException, DocumentException, SQLException {
//       
//        int id = Integer.parseInt(http.getParameter("id"));
////        String tipofase=String.valueOf(http.getParameter("opcional"));
//        ModelAndView modelo = new ModelAndView();
//     InputStream entrada = this.getClass().getResourceAsStream("/reportes/Fase.jrxml");
//    Map<String,Object> params = new HashMap<>();
//    params.put("tutor",new Integer(id));
//    params.put("Fase", new String(fas));
//    JasperReport jasperReport = JasperCompileManager.compileReport(entrada);
//    try {
//      conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/bd_sdt","root", "");
//      conn.setAutoCommit(false);
//    }
//    catch (SQLException e) {
//      System.out.println("Error de conexión: " + e.getMessage());
//      System.exit(4);
//    }
//    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
//    response.setContentType("application/x-pdf");
//    response.setHeader("Content-disposition", "inline; filename=FasesReport.pdf");
//
//    final OutputStream outStream = response.getOutputStream();
//    JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
//    //JasperExportManager.exportReportToPdfFile( jasperPrint, "C:/Users/USUARIO/Downloads/reportesdt.pdf");
//
//
//        modelo.addObject("id", id);
//        modelo.addObject("tipofase",fas);
//        return modelo;
//        
//       
//    }
    
    
    
 

    @ModelAttribute("periodosList")
    public Map<Integer, String> getPeriodos() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getPeriodos()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }
    
    @ModelAttribute("carrerasList")
    public Map<Integer, String> getCarreras() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Tutor i : dao.TutorDao.getCarreras()) {
            periodos.put(i.getCarrera(), i.getNombrecompleto());
        }
        return periodos;
    }
    
    @ModelAttribute("opcionesList")
    public Map<Integer, String> getOpciones() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        periodos.put(0,"Seleccione...");
        periodos.put(1,"Titulación");
        periodos.put(2,"Periodo de Matrícula");
        periodos.put(3,"Periodo de Unidad de Titulación");
        periodos.put(4,"Periodo de Graduación ");
        periodos.put(5,"Fases Proyectos Investigación ");
        return periodos;
    }
    
    @ModelAttribute("fasesList")
    public List getFases() {
        String sql="select distinct fase from procesos";
        List fases = this.jdbctemplate.queryForList(sql);
        return fases;
    }
}
