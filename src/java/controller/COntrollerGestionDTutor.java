/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Diego
 */
public class COntrollerGestionDTutor {
    private JdbcTemplate jdbctemplate;

    public COntrollerGestionDTutor() {
        Conectar con = new Conectar();
        this.jdbctemplate = new JdbcTemplate(con.conectar());
    }
    //metodo para cargar en el pryecto nuevo
    @RequestMapping(value = "listaresponsable.htm", method = RequestMethod.GET)
    public ModelAndView viewEstudianteNew(HttpServletRequest http) {
        int id = Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();
        String sql = "SELECT * from datostutor  WHERE idtutor_fk="+id;
        List datosestudiantes = this.jdbctemplate.queryForList(sql);
        modelo.addObject("datos", datosestudiantes);
        modelo.addObject("id", id);
        return modelo;
    }

}
