/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Items;
import model.Proyectoinves;
import model.Tutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
public class ControladorGestionRegistroTesis {

    private JdbcTemplate jdbctemplate;
    int r = 0;

    public ControladorGestionRegistroTesis() {
        Conectar con = new Conectar();
        this.jdbctemplate = new JdbcTemplate(con.conectar());
    }

    @RequestMapping(value = "gestionarRegistroEstTesis.htm", method = RequestMethod.GET)
    public ModelAndView viewEstudianteNew(HttpServletRequest http) {
        int id = Integer.parseInt(http.getParameter("id"));
        int rol = Integer.parseInt(http.getParameter("rol"));
        ModelAndView modelo = new ModelAndView();
        modelo.addObject("id", id);
        modelo.addObject("rol", rol);
        return modelo;

    }

    @RequestMapping(value = "gestionarRegistroEstTesis.htm", method = RequestMethod.POST)
    public ModelAndView viewEstudiantes(HttpServletRequest http) {
        int id = Integer.parseInt(http.getParameter("id"));
        int rol = Integer.parseInt(http.getParameter("rol"));

        String sql = "";
        ModelAndView modelo = new ModelAndView();
        if (rol == 1) {
            sql = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, p.temaproyecto as tema, ne.nombre as niveles,"
                    + " it.nombre as modalidad, e.folio as folio, e.matricula as matricula, pm.periodomatricula as periodo,"
                    + " p.nombresevaluador1 as evaluador1, p.nombresevaluador2 as evaluador2"
                    + " from nivelestudios ne, items it, periodomatricula pm, estudiante  e JOIN proyectoinves p"
                    + " where e.nivelestudios= ne.id AND it.id=e.modalidad AND pm.id=p.periodorealizacionpi AND e.tutor =" + id + "  and (e.id=p.estudiante)";
        } else if (rol == 2) {
            int carrera = Integer.parseInt(http.getParameter("carreras"));
            sql = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, p.temaproyecto as tema, ne.nombre as niveles,"
                    + " it.nombre as modalidad, e.folio as folio, e.matricula as matricula, pm.periodomatricula as periodo,"
                    + " p.nombresevaluador1 as evaluador1, p.nombresevaluador2 as evaluador2"
                    + " from nivelestudios ne, items it, periodomatricula pm, estudiante  e JOIN proyectoinves p"
                    + " where e.nivelestudios= ne.id AND it.id=e.modalidad AND pm.id=p.periodorealizacionpi AND (e.id=p.estudiante and e.tipo=25) and e.carrera=" + carrera;
        }

        List datosestudiantes = this.jdbctemplate.queryForList(sql);
        int haydatos = 0;
        if (datosestudiantes.size() > 0) {
            haydatos = 1;
        }
        modelo.addObject("datos", datosestudiantes);
        modelo.addObject("existe", haydatos);
        modelo.addObject("id", id);
        modelo.addObject("rol", rol);
        return modelo;

    }

    @RequestMapping(value = "editarRegistroEst.htm", method = RequestMethod.GET)

    public ModelAndView editar(HttpServletRequest http) {
        ModelAndView modelo = new ModelAndView();
        int id = Integer.parseInt(http.getParameter("id"));
        int idEst = Integer.parseInt(http.getParameter("idEst"));
        int rol = Integer.parseInt(http.getParameter("rol"));
        Proyectoinves datos = this.seleccionarEstudiante(idEst);

        modelo.addObject("registroEditado", new Proyectoinves(datos.getFoliopi(), datos.getMatriculapi(), datos.getPeriodoingresounidadtitulacion(), datos.getTemaproyecto(), datos.getLineainvestigacion(), datos.getTutorasignado(), datos.getPeriodorealizacionpi(), datos.getNombresevaluador1(), datos.getNombresevaluador2(), datos.getPeriodograduacion(), datos.getFechagrado(), datos.getNumacta(), datos.getFechainvestidura(), datos.getNumresolucion(), datos.getObservaciones()));
        modelo.addObject("id", id);
        modelo.addObject("rol", rol);
        return modelo;
    }

    @RequestMapping(value = "editarRegistroEst.htm", method = RequestMethod.POST)
    public ModelAndView editarDatos(@ModelAttribute("registroEditado") Proyectoinves e, HttpServletRequest http) {
        int id = Integer.parseInt(http.getParameter("id"));
        int idEst = Integer.parseInt(http.getParameter("idEst"));
        int rol = Integer.parseInt(http.getParameter("rol"));
        ModelAndView modelo = new ModelAndView();

        this.jdbctemplate.update(
                "update proyectoinves "
                + "set foliopi=?,"
                + " matriculapi=?,"
                + " periodoingresounidadtitulacion=?,"
                + " temaproyecto=?,"
                + " lineainvestigacion=?,"
                + "tutorasignado=?,"
                + " periodorealizacionpi=?,"
                + " nombresevaluador1=?,"
                + " nombresevaluador2=?,"
                + " periodograduacion=?,"
                + " fechagrado=?,"
//                + " numacta=?,"
//                + " fechainvestidura=?,"
                + " numresolucion=?,"
                + " observaciones=?"
                + " where estudiante= ?",
                e.getFoliopi(), e.getMatriculapi(), e.getPeriodoingresounidadtitulacion(), e.getTemaproyecto(),
                e.getLineainvestigacion(), e.getTutorasignado(), e.getPeriodorealizacionpi(), e.getNombresevaluador1(),
                e.getNombresevaluador2(), e.getPeriodograduacion(), e.getFechagrado(),
//                e.getNumacta(), 
//                e.getFechainvestidura(),
                e.getNumresolucion(), e.getObservaciones(), idEst
        );
        if (rol == 1) {
            return new ModelAndView("redirect:/tutorhome.htm?id=" + id);
        } else if (rol == 2) {
            return new ModelAndView("redirect:/secretariahome.htm?id=" + id);
        }
        return null;
    }

    @RequestMapping(value = "eliminarRegistroEst.htm", method = RequestMethod.GET)
    public ModelAndView eliminar(HttpServletRequest http) {
        int id = Integer.parseInt(http.getParameter("id"));
        int idEst = Integer.parseInt(http.getParameter("idEst"));
        ModelAndView modelo = new ModelAndView();

        this.jdbctemplate.update(
                "delete from proyectoinves where estudiante=?",
                idEst
        );
        modelo.addObject("idEst", idEst);
        return new ModelAndView("redirect:/tutorhome.htm?id=" + id);
    }

    private Proyectoinves seleccionarEstudiante(int idEst) {
        final Proyectoinves listaUsuarios = new Proyectoinves();
        String consulta = "select * from proyectoinves WHERE estudiante=" + idEst;

        return (Proyectoinves) jdbctemplate.query(consulta, new ResultSetExtractor<Proyectoinves>() {
            public Proyectoinves extractData(ResultSet rs) throws SQLException {
                if (rs.next()) {
                    listaUsuarios.setFoliopi(rs.getString("foliopi"));
                    listaUsuarios.setMatriculapi(rs.getString("matriculapi"));
                    listaUsuarios.setPeriodoingresounidadtitulacion(rs.getInt("periodoingresounidadtitulacion"));
                    listaUsuarios.setTemaproyecto(rs.getString("temaproyecto"));
                    listaUsuarios.setLineainvestigacion(rs.getString("lineainvestigacion"));
                    listaUsuarios.setTutorasignado(rs.getString("tutorasignado"));
                    listaUsuarios.setPeriodorealizacionpi(rs.getInt("periodorealizacionpi"));
                    listaUsuarios.setNombresevaluador1(rs.getString("nombresevaluador1"));
                    listaUsuarios.setNombresevaluador2(rs.getString("nombresevaluador2"));
                    listaUsuarios.setPeriodograduacion(rs.getInt("periodograduacion"));
                    listaUsuarios.setFechagrado(rs.getString("fechagrado"));
                    listaUsuarios.setNumacta(rs.getInt("numacta"));

//                    if (rs.getString("fechainvestidura").isEmpty()) {
//                        listaUsuarios.setFechainvestidura(null);
//                    } else {
//                        listaUsuarios.setFechainvestidura(rs.getString("fechainvestidura"));
//                    }
                        listaUsuarios.setFechainvestidura(rs.getString("fechainvestidura"));

                    listaUsuarios.setNumresolucion(rs.getString("numresolucion"));
                    listaUsuarios.setObservaciones(rs.getString("observaciones"));

                }
                return listaUsuarios;
            }
        });
    }

    @ModelAttribute("periodosList")

    public Map<Integer, String> getPeriodos() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        periodos.put(0, "Seleccione..");
        for (Items i : dao.CatalogoDao.getPeriodos()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }

    @ModelAttribute("carrerasList")
    public Map<Integer, String> getCarreras() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Tutor i : dao.TutorDao.getCarreras()) {
            periodos.put(i.getCarrera(), i.getNombrecompleto());
        }
        return periodos;
    }
}
