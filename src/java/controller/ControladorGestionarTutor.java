/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Datostutor;
import model.Items;
import model.Tutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
/**
 *
 * @author user
 */
public class ControladorGestionarTutor {
     private JdbcTemplate jdbctemplate;
   
    public ControladorGestionarTutor(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }
    @RequestMapping(value = "gestionarTutor.htm",method = RequestMethod.GET)
    public ModelAndView viewTutorNew(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
         
        ModelAndView modelo= new ModelAndView();
        
        String sql= "SELECT * from tutor";
        
        List datosper=this.jdbctemplate.queryForList(sql);
        
        modelo.addObject("datos",datosper);
        modelo.addObject("id", id);
        
        return modelo;
    
    }
    
//    @RequestMapping(value = "tutornew.htm",method = RequestMethod.GET)
//    
//    public ModelAndView editar(HttpServletRequest http){
//        ModelAndView modelo= new ModelAndView();
//        int id= Integer.parseInt(http.getParameter("id"));
//        int idItems= Integer.parseInt(http.getParameter("idItem"));
//        
//        Items datos=this.seleccionarItems(idItems);
//        
//        
//        modelo.addObject("carrera", new Tutor( datos.getNombre() datos.));
//        modelo.addObject("id",id);
//        
//        return modelo;
//    }
//    
//    
//    //Agregar datos
//     @RequestMapping(value = "editarTutor.htm",method = RequestMethod.POST)
//    public ModelAndView editarDatos(@ModelAttribute("tutorEditado") Datostutor dt,HttpServletRequest http ){
//           int id= Integer.parseInt(http.getParameter("id"));
//        int idTutor= Integer.parseInt(http.getParameter("idTutor"));
//        
//        ModelAndView modelo= new ModelAndView();
//        
//
//        this.jdbctemplate.update(
//            "update datostutor "
//            +"set nombre=?,"     
//            +" apellido=?,"
//            +" cedula=?,"
//            +" periodoresponsable=? "    
//            +" where id= ?"   , 
//             dt.getNombre(), dt.getApellido(), dt.getCedula(), dt.getPeriodoresponsable(), idTutor );
//        
//        
//            return new ModelAndView("redirect:/administradorhome.htm?id="+id);
//        
//        
//    }
    
    
    @RequestMapping(value = "eliminarTutor.htm",method = RequestMethod.GET)
     public ModelAndView eliminar(HttpServletRequest http){
         int id=Integer.parseInt(http.getParameter("id"));
         int idCarr = Integer.parseInt(http.getParameter("idCarr"));
         String idNombre= http.getParameter("idNombre");
         ModelAndView modelo = new ModelAndView();
         
         this.jdbctemplate.update(
         "delete from items where id=?",
         idCarr
         );
         
         this.jdbctemplate.update(
         "delete from tutor where nombrecompleto =?",
         idNombre
         );
         
         
         modelo.addObject("idNombre", idNombre);
        return new ModelAndView("redirect:/administradorhome.htm?id="+id);
     }

    private Items seleccionarItems(int idCarr) {
         final Items listaUsuarios= new Items();
        String consulta="select * from items WHERE id="+idCarr;
        
            
       
         return (Items) jdbctemplate.query(consulta, new ResultSetExtractor<Items>(){
            public Items extractData(ResultSet rs) throws SQLException{
                if(rs.next()){
                    listaUsuarios.setNombre(rs.getString("nombre"));
                    //listaUsuarios.setCatalogo(rs.getString(idItem));
                   
                   
                    
                }
                return listaUsuarios;
            }   
        });
    }
}
