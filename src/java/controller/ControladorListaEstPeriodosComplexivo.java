/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mysql.jdbc.Connection;
import static controller.ControladorListaEstPeriodos.conn;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Conectar;
import model.Items;
import model.Periodosunidadtitulacion;
import model.Tutor;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
public class ControladorListaEstPeriodosComplexivo {

    private JdbcTemplate jdbctemplate;
    private int per = 0;
    private int pert = 0;
    private int op = 0;
    int fas = 0;
    private String fech=null;
    public ControladorListaEstPeriodosComplexivo() {
        Conectar con = new Conectar();
        this.jdbctemplate = new JdbcTemplate(con.conectar());
    }

    @RequestMapping(value = "listaperiodosEstComplexivo.htm", method = RequestMethod.GET)
    public ModelAndView viewEstudianteNew(HttpServletRequest http) {
        int id = Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();
        int rol = Integer.parseInt(http.getParameter("rol"));

        modelo.addObject("id", id);
        modelo.addObject("rol", rol);
        return modelo;
    }

    @RequestMapping(value = "listaperiodosEstComplexivo.htm", method = RequestMethod.POST)
    public ModelAndView viewEstudiante(HttpServletRequest http) {
        int haydatos = 0;
        int id = Integer.parseInt(http.getParameter("id"));
        int opcion = Integer.parseInt(http.getParameter("opcion"));

        int periodo = Integer.parseInt(http.getParameter("periodo"));
        int rol = Integer.parseInt(http.getParameter("rol"));
        int fase = Integer.parseInt(http.getParameter("fases"));
        String fecha = http.getParameter("fecha");
        int pt = Integer.parseInt(http.getParameter("periodos"));
        fech=fecha;
        op = opcion;
        pert = pt;
        String filtrado = "";
        String sqlRevision = "";
        per = periodo;
        fas=fase;
        System.out.println("................... i ......" + id + "..........................");
        System.out.println("....................p....." + per + "..........................");
        System.out.println("....................pt...." + pert + "..........................");
        System.out.println("........................fase: "+fas+".......................");
        System.out.println("........................fecha: "+fech+".......................");
        ModelAndView modelo = new ModelAndView();
        if (rol == 1) {
            if (opcion == 1) {
                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, i.nombrecompleto as opcional,p.fechaexamencomplexivo as fecha from estudiante  e INNER JOIN examencomplexivo p ON (e.tutor =" + id + " and e.tipo=26) and (e.id=p.estudiante) INNER JOIN Tutor i ON i.carrera= e.carrera";
                filtrado = "Carrera";
            } else if (opcion == 2) {
                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, i.nombre as opcional,p.fechaexamencomplexivo as fecha from estudiante  e INNER JOIN examencomplexivo p ON (e.tutor =" + id + " and e.periodomatricula=" + periodo + " and e.tipo=26) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = e.periodomatricula";
                filtrado = "Período de Matricula";
            } else if (opcion == 3) {
                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, i.nombre as opcional,p.fechaexamencomplexivo as fecha from estudiante  e INNER JOIN examencomplexivo p ON (e.tutor =" + id + " and p.periodoingresounidadtit=" + pert + " and e.tipo=26) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = p.periodoingresounidadtit";
                filtrado = "Período de Ingreso a Unidad de Titulación";
            } else if (opcion == 4) {
                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, i.nombre as opcional,p.fechaexamencomplexivo as fecha from estudiante  e INNER JOIN examencomplexivo p ON (e.tutor =" + id + " and p.periodocapacitacion=" + periodo + " and e.tipo=26) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = p.periodocapacitacion";
                filtrado = "Período de Capacitación";
            } else if (opcion == 5) {

                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, p.fechainvestidura as opcional,p.fechaexamencomplexivo as fecha from estudiante  e  JOIN examencomplexivo p WHERE (e.tutor =" + id + " and p.fechainvestidura='" + fecha + "' and e.tipo=26) and (e.id=p.estudiante)";
                filtrado = "Fecha Investidura";
            } else if (opcion == 6) {

                sqlRevision = "select distinct e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, i.tipofase as opcional,p.fechaexamencomplexivo as fecha from estudiante  e  INNER JOIN examencomplexivo p ON (e.tutor =" + id + " and e.tipo=26) and (e.id=p.estudiante) and ( p.periodoingresounidadtit=" + periodo + ") INNER JOIN procesosexamen i ON i.tipofase= " + fase + " and i.estudiante=e.id";
                filtrado = "N° de Fase";
            }
        } else {
            int carrera = Integer.parseInt(http.getParameter("carreras"));
            if (opcion == 1) {
                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, i.nombrecompleto as opcional,p.fechaexamencomplexivo as fecha from estudiante  e INNER JOIN examencomplexivo p ON (e.carrera=" + carrera + " and e.tipo=26) and (e.id=p.estudiante) INNER JOIN Tutor i ON i.carrera = e.carrera";
                filtrado = "Carrera";
            } else if (opcion == 2) {
                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, i.nombre as opcional,p.fechaexamencomplexivo as fecha from estudiante  e INNER JOIN examencomplexivo p ON (e.periodomatricula=" + periodo + " and e.tipo=26) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = e.periodomatricula";
                filtrado = "Período de Matricula";
            } else if (opcion == 3) {
                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, i.nombre as opcional,p.fechaexamencomplexivo as fecha from estudiante  e INNER JOIN examencomplexivo p ON ( p.periodoingresounidadtit=" + periodo + " and e.tipo=26) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = p.periodoingresounidadtit";
                filtrado = "Período de Ingreso a Unidad de Titulación";
            } else if (opcion == 4) {
                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, i.nombre as opcional,p.fechaexamencomplexivo as fecha from estudiante  e INNER JOIN examencomplexivo p ON ( p.periodocapacitacion=" + periodo + " and e.tipo=26) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = p.periodocapacitacion";
                filtrado = "Período de Capacitación";
            } else if (opcion == 5) {

                sqlRevision = sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, p.fechainvestidura as opcional,p.fechaexamencomplexivo as fecha from estudiante  e  JOIN examencomplexivo p WHERE ( p.fechainvestidura=" + fecha + " and e.tipo=26) and (e.id=p.estudiante)";
                filtrado = "Fecha Investidura";
            } else if (opcion == 6) {

                sqlRevision = "select distinct e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, i.tipofase as opcional,p.fechaexamencomplexivo as fecha from estudiante  e  INNER JOIN examencomplexivo p ON (e.tipo=26) and (e.id=p.estudiante) and ( p.periodoingresounidadtit=" + periodo + ") INNER JOIN procesosexamen i ON i.tipofase= " + fase + " and i.estudiante=e.id";
                filtrado = "N° de Fase";
            }
        }
        System.out.println("Sql: \n"+sqlRevision);
        System.out.println("Filtro: \n"+filtrado);

        try {
            List datosestudiantes = this.jdbctemplate.queryForList(sqlRevision);
            if (datosestudiantes.size() > 0) {
                haydatos = 1;
            }

            modelo.addObject("datos", datosestudiantes);
            modelo.addObject("id", id);
            modelo.addObject("filtrado", filtrado);
            modelo.addObject("existe", haydatos);
            modelo.addObject("rol", rol);
            return modelo;
        } catch (Exception e) {
            modelo.addObject("id", id);
            modelo.addObject("existe", haydatos);
            modelo.addObject("filtrado", filtrado);
            modelo.addObject("rol", rol);
            return modelo;
        }
    }

    @RequestMapping(value = "reportePDFComplexivo.htm", method = RequestMethod.GET)
    //    codigo diego
//    @RequestMapping(value = "reportepdftitulacion.htm", method = RequestMethod.GET)   
    @ResponseBody
    public ModelAndView descargarPDFTitul(HttpServletRequest http, HttpServletResponse response) throws FileNotFoundException, JRException, IOException, DocumentException, SQLException {
        int id = Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();
        System.out.println("********************************************************");
        System.out.println("********************************************************");
        System.out.println("\tTutor: " + id + "\n\tPeriodo: " + per + "\n\tFase: " + fas);
        System.out.println("********************************************************");
        System.out.println("********************************************************");
        InputStream entrada = null;
        Map<String, Object> params = null;
        try {
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/bd_sdt", "root", "");
            conn.setAutoCommit(false);
            if (op != 0) {
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                if (op == 2) {
                    entrada = this.getClass().getResourceAsStream("/reportes/periodoMatriculaComplexivo.jrxml");
                    params = new HashMap<>();
                    params.put("periodo", per);
                    params.put("idtutor", id);
                }
                if (op == 3) {
                    entrada = this.getClass().getResourceAsStream("/reportes/reportexamencomplex.jrxml");
                    params = new HashMap<>();
                    params.put("idPeriodoT", pert);
                    params.put("idTutor", id);

                } else if (op == 4) {
                    entrada = this.getClass().getResourceAsStream("/reportes/periodoCapacitacion.jrxml");
                    params = new HashMap<>();
                    params.put("idtutor", new Integer(id)); //
                    params.put("periodoCap", per); //entre comillas tu parametro del reporte en mi caso es periodo
                }else
                
                if (op==5) {
                    entrada = this.getClass().getResourceAsStream("/reportes/fechaInvestidura.jrxml");
                    params = new HashMap<>();
                    params.put("idtutor", id); //
                    params.put("fechainves", fech);
                }
                else if (op == 6) {
                    if (fas==1) {
                        System.out.println("------------------------------------");
                        System.out.println(fas);
                        System.out.println(id);
                        System.out.println(op);
                        System.out.println(per);
                        entrada = this.getClass().getResourceAsStream("/reportes/Fase1Complexivo.jrxml");
                        params = new HashMap<>();
                        params.put("Fase", fas);
                        params.put("tutor", id);
                        params.put("pintitu", per);
                    }
                    if (fas==2) {
                        entrada = this.getClass().getResourceAsStream("/reportes/Fase2Complexivo.jrxml");
                        params = new HashMap<>();
                        params.put("tutor", id);
                        params.put("Fase", fas);
                        params.put("pintitu", per);
                    }
                    if (fas==3) {
                        entrada = this.getClass().getResourceAsStream("/reportes/Fase3Complexivo.jrxml");
                        params = new HashMap<>();
                        params.put("tutor", id);
                        params.put("Fase", fas);
                        params.put("pintitu", per);
                    }
                    if (fas==4) {
                        entrada = this.getClass().getResourceAsStream("/reportes/Fase4Complexivo.jrxml");
                        params = new HashMap<>();
                        params.put("tutor", id);
                        params.put("Fase", fas);
                        params.put("pintitu", per);
                    }
//      
                } else {
                    System.out.println("Ocurrio un error al descargar el reporte: ");
                }
                JasperReport jasperReport = JasperCompileManager.compileReport(entrada);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
                response.setContentType("application/x-pdf");
                response.setHeader("Content-disposition", "inline; filename=ReportComplexivo.pdf");
                final OutputStream outStream = response.getOutputStream();
                JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);

            } else {
                System.out.println("No existe datos");
            }
        } catch (SQLException | JRException | IOException e) {
            System.out.println("Error de conexión:...................................................................\n "
                    + e.getMessage());
//            System.exit(4);
        }

        modelo.addObject("id", id);
        modelo.addObject("periodo", per);
        return modelo;
    }
    
    @RequestMapping(value = "reportefasesTotalPDF.htm", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView FasesdesTotalcargarPDF(HttpServletRequest http, HttpServletResponse response) throws FileNotFoundException, JRException, IOException, DocumentException, SQLException {
        int id = Integer.parseInt(http.getParameter("id"));//        String tipofase=String.valueOf(http.getParameter("opcional"));
        ModelAndView modelo = new ModelAndView();
     InputStream entrada = this.getClass().getResourceAsStream("/reportes/FaseComplexivoTotal.jrxml");
    Map<String,Object> params = new HashMap<>();
    params.put("tutor",new Integer(id));
    params.put("pintitu",new Integer(per));
    JasperReport jasperReport = JasperCompileManager.compileReport(entrada);
    try {
      conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/bd_sdt","root", "");
      conn.setAutoCommit(false);
    }
    catch (SQLException e) {
      System.out.println("Error de conexión: " + e.getMessage());
      System.exit(4);
    }
    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
    response.setContentType("application/x-pdf");
    response.setHeader("Content-disposition", "inline; filename=FasesTotalReportComplexivo.pdf");

    final OutputStream outStream = response.getOutputStream();
    JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
    //JasperExportManager.exportReportToPdfFile( jasperPrint, "C:/Users/USUARIO/Downloads/reportesdt.pdf");


        modelo.addObject("id", id);
        modelo.addObject("periodoingresounidadtit", per);
        return modelo;
    }

    @ModelAttribute("periodosList")
    public Map<Integer, String> getPeriodos() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getPeriodos()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }

    @ModelAttribute("carrerasList")
    public Map<Integer, String> getCarreras() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Tutor i : dao.TutorDao.getCarreras()) {
            periodos.put(i.getCarrera(), i.getNombrecompleto());
        }
        return periodos;
    }

    @ModelAttribute("opcionesList")
    public Map<Integer, String> getOpciones() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        periodos.put(0, "Seleccione...");
        periodos.put(1, "Titulación");
        periodos.put(2, "Periodo de Matrícula");
        periodos.put(3, "Periodo de Unidad de Titulación");
        periodos.put(4, "Periodo de Capacitación ");
        periodos.put(5, "Fecha Investidura ");
        periodos.put(6, "Fases de Exámen Complexivo ");
        return periodos;
    }

    @ModelAttribute("periodosTituList")
    public Map<Integer, String> getPeriodosTitulacion() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Periodosunidadtitulacion i : dao.PeriodosunidadtitulacionDao.getPeriodosTitulacion()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }

    @ModelAttribute("fasesList")
    public List getFases() {
        String sql = "select distinct fase from procesos_complexivo";
        List fases = this.jdbctemplate.queryForList(sql);
        return fases;
    }
}
