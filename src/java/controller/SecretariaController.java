/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.Menu;
import model.Menurol;
import model.Secretaria;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SecretariaController {

    @RequestMapping(value = "secretariahome.htm")
    public ModelAndView viewHome(HttpServletRequest http) {
        int id=Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();
        Secretaria s=dao.SecretariaDao.getSecretaria(id);
         
         modelo.addObject("usuario", s.getNombrecompleto());
        modelo.addObject("id", id);
        return modelo;
    }
    @ModelAttribute("menuList")
    public List<Menu> getMenu() {
        List<Menu> menus=new ArrayList<>();
        for (Menurol i : dao.MenuRolDao.getMunus(3)) {
            Menu menu=dao.MenuDao.getId(i.getMenu().getId());
            menus.add(menu);
        }
        return menus;
    }
}
