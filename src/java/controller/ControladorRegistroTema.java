/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateUtil;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Estudiante;
import model.Items;
import model.Proyectoinves;
import model.Rol;
import model.Tutor;
import org.hibernate.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller
@RequestMapping("tesis.htm")
public class ControladorRegistroTema {
    @RequestMapping(method =RequestMethod.GET)
    public ModelAndView view(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
         int idTutor=Integer.parseInt(http.getParameter("idTutor"));
         ModelAndView modelo= new ModelAndView();
             modelo.addObject("proyectoinves",new Proyectoinves());
             modelo.addObject("id", idTutor);
            
         return modelo;
    }
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView agregarTema(@ModelAttribute("proyectoinves") Proyectoinves p, HttpServletRequest http){
        int id=Integer.parseInt(http.getParameter("id"));
        int idTutor=Integer.parseInt(http.getParameter("idTutor"));
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        //Add new Employee object
        Estudiante est= new Estudiante();
        session.load(est, id);
        est.setId(id);
        Proyectoinves nuevoproyecto = new Proyectoinves();
        nuevoproyecto.setFoliopi(p.getFoliopi());
        nuevoproyecto.setMatriculapi(p.getMatriculapi());
        nuevoproyecto.setPeriodoingresounidadtitulacion(p.getPeriodoingresounidadtitulacion());
        nuevoproyecto.setTemaproyecto(p.getTemaproyecto());
        nuevoproyecto.setLineainvestigacion(p.getLineainvestigacion());                        
        nuevoproyecto.setTutorasignado(p.getTutorasignado());
        nuevoproyecto.setPeriodorealizacionpi(p.getPeriodorealizacionpi());
        nuevoproyecto.setNombresevaluador1(p.getNombresevaluador1());
        nuevoproyecto.setNombresevaluador2(p.getNombresevaluador2());
        nuevoproyecto.setPeriodograduacion(p.getPeriodograduacion());
        nuevoproyecto.setFechagrado(p.getFechagrado());
        nuevoproyecto.setNumacta(p.getNumacta());
        nuevoproyecto.setObservaciones(p.getObservaciones());
        nuevoproyecto.setEstudiante(est);
         Tutor t= new Tutor();
         session.load(t, idTutor);
         t.setId(idTutor);
         
         nuevoproyecto.setTutor(t);
         
        //Save the employee in database
        session.save(nuevoproyecto);
 
        //Commit the transaction
        session.getTransaction().commit();
        return new ModelAndView("redirect:/tutorhome.htm?id="+idTutor);
        
    }
    
    @ModelAttribute("periodosList")

    public Map<Integer, String> getPeriodos() {
        
        Map<Integer, String> periodos = new LinkedHashMap<>();
        periodos.put(0,"Seleccione..");
        for (Items i : dao.CatalogoDao.getPeriodos()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }
}
