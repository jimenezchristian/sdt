/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import model.Conectar;
import model.Estudiante;
import model.Menu;
import model.Menurol;
import org.recursos.clasesauxi.Persona;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EstudianteController {
    private JdbcTemplate jdbctemplate;
    
    public EstudianteController (){
         Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
         
    }

    @RequestMapping(value = "estudiantehome.htm")
    public ModelAndView viewHome(HttpServletRequest http) {
        ModelAndView modelo = new ModelAndView();
        int id=Integer.parseInt(http.getParameter("id"));
        Estudiante e=dao.EstudianteDao.getEstudiante(id);
         
         modelo.addObject("usuario", e.getNombre()+" "+ e.getApellido());
        modelo.addObject("id",id);
        
        return modelo;
    }

    @RequestMapping(value = "estudiantelist.htm", method = RequestMethod.GET)
    
    public ModelAndView getEstudiantes(HttpServletRequest http) {
             int id=Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo= new ModelAndView();
        
        String sql="select * from estudiante where tipo=25 and tutor = "+id+" and not exists( select estudiante from proyectoinves where estudiante.id=proyectoinves.estudiante)";
        List datosestudiantes=this.jdbctemplate.queryForList(sql);
         modelo.addObject("idTutor", id);
         modelo.addObject("datos",datosestudiantes);
        
        return modelo;

    }
    @RequestMapping(value = "estudiantelistexamen.htm", method = RequestMethod.GET)

    public ModelAndView getEstudiantesExamen(HttpServletRequest http) {
        int id = Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();

        String sql = "select * from estudiante where tipo=26 and tutor = " + id + " and not exists( select estudiante from examencomplexivo where estudiante.id=examencomplexivo.estudiante )";
        List datosexamen = this.jdbctemplate.queryForList(sql);
        modelo.addObject("idTutor", id);
        modelo.addObject("datosex", datosexamen);

        return modelo;

    }

    

    @ModelAttribute("menuList")
    public List<Menu> getMenu() {
        List<Menu> menus = new ArrayList<>();
        for (Menurol i : dao.MenuRolDao.getMunus(4)) {
            Menu menu = dao.MenuDao.getId(i.getMenu().getId());
            menus.add(menu);
        }
        return menus;
    }
}
