/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Estudiante;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller
public class ControladorListaEstudiantesTutor {
        private JdbcTemplate jdbctemplate;
    
    public ControladorListaEstudiantesTutor (){
         Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
         
    }
    @RequestMapping("listaEstudiantesTutor.htm")
    public ModelAndView viewEstudianteNew(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo= new ModelAndView();
        
        String sqlEst= "select e.id as id,e.nombre as nombre, e.apellido as apellido, i.nombre as carrera, e.correo as correo from estudiante e inner join items i on e.carrera=i.id and e.tutor="+id+" and e.tipo=25 inner join proyectoinves p on e.id=p.estudiante";
      
      //Recuoperamos los procesos
     String sql= "select * from procesos";
     
     //ArrayList listarvision= seleccionarRevision();
     String sqlRevision ="select e.id as idEstudiante ,r.estado as estadoRev,r.id_Proceso as procesoRev, r.archivo as archivoRev from procesoproyectos  r JOIN estudiante e where e.id=r.estudiante ";
     List datosestudiantes=this.jdbctemplate.queryForList(sqlEst);
     List listaRev=this.jdbctemplate.queryForList(sqlRevision);
        
        List procesos=this.jdbctemplate.queryForList(sql);


         modelo.addObject("datos",datosestudiantes);
         modelo.addObject("datosRevision",listaRev);
      modelo.addObject("datosprocesos",procesos);//enviamos a la vista
      modelo.addObject("id",id);
      modelo.setViewName("listaEstudiantesTutor");
      
        return modelo;

    }
    
}
