/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Conectar;
import model.Procesoproyectos;
import org.apache.commons.io.IOUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller
@RequestMapping("descarga.htm")
public class ControladorDescarga {
    private JdbcTemplate jdbctemplate;
    int bandera;
     public ControladorDescarga(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }
     @RequestMapping(method = RequestMethod.GET)
    public ModelAndView descragra(HttpServletRequest http,HttpServletResponse response){
         ModelAndView modelo= new ModelAndView();
        int id= Integer.parseInt(http.getParameter("id"));
        int idProceso= Integer.parseInt(http.getParameter("idProceso"));
        int tipo=Integer.parseInt(http.getParameter("Tipo"));
        
         Procesoproyectos datos=this.seleccionarUsuario(id,idProceso,response,tipo);
       
        
     
       return new ModelAndView("redirect:/listaEstudiantesTesis.htm");
    }
    
    
    private Procesoproyectos seleccionarUsuario(int id,int idProceso,final HttpServletResponse response,int tipo) {
         final Procesoproyectos listaUsuarios= new Procesoproyectos();
         String consulta="";
         if(tipo==25){
              consulta="select * from procesoproyectos WHERE estudiante="+id+" and id_Proceso= "+idProceso;
         }
         else{
             if(tipo==26){
              consulta="select * from procesosexamen WHERE estudiante="+id+" and id_ProcesoCompl= "+idProceso;
             }
         }
         
        
        
            
       
         return (Procesoproyectos) jdbctemplate.query(consulta, new ResultSetExtractor<Procesoproyectos>(){
            public Procesoproyectos extractData(ResultSet rs) throws SQLException{
                while(rs.next()){
                   String nombreArchivo=rs.getString("nombreArchivo");

                    Blob data = rs.getBlob("archivo");
                   
                    byte[]re=data.getBytes(1, (int)data.length());
                    try{
                        
                        
                        response.setContentType("application/octet-stream");
                        response.setContentLength(re.length);
                        response.setHeader("Content-Disposition","attachment;filename=\""+nombreArchivo+"\"");
                        FileCopyUtils.copy(re,response.getOutputStream());
                        
                    } 
                    catch (IOException ex) {
                        Logger.getLogger(ControladorDescarga.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                return listaUsuarios;
            }   
        });
    }
}
