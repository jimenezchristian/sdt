/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.sql.Connection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Items;
import model.Tutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControladorGraficoComplex {
    private JdbcTemplate jdbctemplate;
    private int per 
            = 0;
    int haydatos = 0;
    static Connection conn = null;

    public ControladorGraficoComplex() {
        Conectar con = new Conectar();
        this.jdbctemplate = new JdbcTemplate(con.conectar());
    }

    @RequestMapping(value = "graficoestadisticocomplex.htm", method = RequestMethod.GET)
    public ModelAndView viewEstudianteNew(HttpServletRequest http) {
        int id = Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();
        modelo.addObject("id", id);
        return modelo;
    }

    @RequestMapping(value = "graficoestadisticocomplex.htm", method = RequestMethod.POST)
    public ModelAndView viewEstudiante(HttpServletRequest http) {
        http.getParameterMap();
        int id = Integer.parseInt(http.getParameter("id"));
//         int carrera=Integer.parseInt(http.getParameter("carreras"));
//        int tipo = Integer.parseInt(http.getParameter("tipo"));
        int periodo = Integer.parseInt(http.getParameter("periodo"));
        /*String sqlultimoPro= "SELECT MAX(id)as id FROM procesos";
         List<Map<String, Object>> idProceso=this.jdbctemplate.queryForList(sqlultimoPro); 
         String sqlcomprobar= "SELECT *  FROM estudiantes";*/
//         String sqlRevision ="select i.nombrecompleto as nombre  from tutor i INNER JOIN estudiante e ON e.tutor="+id+" and i.carrera = e.carrera";
//         String sqlPeriodo ="select i.nombre as periodo  from items i INNER JOIN estudiante e ON e.periodomatricula="+periodo+" and i.id = e.periodomatricula";
//         String sqlPrueba="SELECT e.graduacion as conclusion, count(*) as graduacion FROM estudiante e INNER JOIN Items i ON (e.periodomatricula=i.id and e.tipo = "+tipo +") and (e.periodomatricula = "+periodo+" and e.tutor = "+id+" ) group by graduacion ";
//         String sqlNograduados="select nombre, apellido, cedula from estudiante where (tipo="+tipo+" and graduacion = 0) and (tutor = "+id+" and periodomatricula = "+periodo+")";
//         String sqlGraduados="select nombre, apellido, cedula from estudiante where (tipo="+tipo+" and graduacion = 1) and (tutor = "+id+" and periodomatricula = "+periodo+")";
        String sqlNumNograduados = "SELECT COUNT(*) AS nograduados FROM examencomplexivo p, estudiante e WHERE p.fechainvestidura is  null  AND e.periodomatricula=" + periodo + " AND e.tutor=" + id + " AND e.id=p.estudiante  AND e.tipo=26";
        String sqlLimite = "SELECT DATE_ADD((p.fechaingresocarrera),INTERVAL 6 YEAR) AS limite FROM estudiante e, periodomatricula p WHERE e.periodomatricula=p.id AND e.tutor=" + id + " AND e.periodomatricula =" + periodo + "  AND e.tipo=26";
        String sqlGraf = "SELECT COUNT(*) AS total ,t.nombrecompleto, pm.periodomatricula FROM estudiante e, tutor t ,periodomatricula pm,examencomplexivo p WHERE e.tutor=" + id + " AND e.periodomatricula=" + periodo + " AND t.id=e.tutor AND pm.id=e.periodomatricula  AND e.tipo=26 and p.estudiante=e.id";
        String sqlFechaInvestidura = "SELECT p.fechainvestidura  AS graduados FROM examencomplexivo p, estudiante e WHERE p.fechainvestidura is not null  AND e.periodomatricula=" + periodo + " AND e.tutor=" + id + " AND e.id=p.estudiante  AND e.tipo=26";
//        String sqlNombresNoGraduados = "SELECT e.cedula,e.nombre,e.apellido FROM proyectoinves p, estudiante e WHERE p.fechainvestidura='0000-00-00' AND e.periodomatricula=" + periodo + " AND e.tutor=" + id + " AND e.id=p.estudiante AND e.tipo=25";
        String titulotipo = "";

        System.out.println("---------------------------------");

        System.out.println("sqlLimite: " + sqlLimite);
        System.out.println("sqlGraf: " + sqlGraf);
        System.out.println("sqlfi: " + sqlFechaInvestidura);
        System.out.println("sqlNoGraduados: " + sqlNumNograduados);
        titulotipo = "Exámen Complexivo";
        try {
            
            List contarnograd = this.jdbctemplate.queryForList(sqlNumNograduados);
            List grafLista = this.jdbctemplate.queryForList(sqlGraf);
            List limite = this.jdbctemplate.queryForList(sqlLimite);
            List investidura = this.jdbctemplate.queryForList(sqlFechaInvestidura);

            int nogra = 0, ng = 0;
            int gra = 0, g = 0;
            String cover = null;
            String sqlnomG = null;
            String sqlnomN = null;
            String numng=null;
            String numg=null;
            cover = limite.get(1).toString().substring(8, 12);
            for (int i = 0; i < investidura.size(); i++) {
                String tmp = investidura.get(i).toString().substring(11, 15);
                System.out.println("**Fecha calc:**" + cover);
                System.out.println("F, inves: " + tmp);
                System.out.println("------Size-----"+investidura.size());
                if (cover.equals(tmp)) {
                    System.out.println("Graduados: ");
                    sqlnomG = "SELECT e.cedula,e.nombre,e.apellido FROM examencomplexivo p, estudiante e WHERE p.fechainvestidura LIKE('" + tmp + "%') AND e.periodomatricula=" + periodo + " AND e.tutor=" + id + " AND e.id=p.estudiante AND e.tipo=26";
                    numg="SELECT COUNT(*) AS numg FROM examencomplexivo p, estudiante e WHERE p.fechainvestidura LIKE('" + tmp + "%') AND e.periodomatricula=" + periodo + " AND e.tutor=" + id + " AND e.id=p.estudiante AND e.tipo=26" ;   
                    System.out.println("Nombre g>> "+sqlnomG);
                    System.out.println("Num g>> "+numg);
                    g++;
                    gra = 1;
                    haydatos = 1;
                } else {
                    System.out.println("No graduados:");
                    sqlnomN = "SELECT e.cedula,e.nombre,e.apellido FROM examencomplexivo p, estudiante e WHERE (p.fechainvestidura NOT LIKE('"+cover+"%') or p.fechainvestidura is  null )  AND e.periodomatricula=" + periodo + " AND e.tutor=" + id + " AND e.id=p.estudiante AND e.tipo=26";
                    numng="SELECT COUNT(*) AS numng FROM examencomplexivo p, estudiante e WHERE (p.fechainvestidura NOT LIKE('"+cover+"%') OR p.fechainvestidura is  null )  AND e.periodomatricula=" + periodo + "  AND e.tutor=" + id + " AND e.id=p.estudiante AND e.tipo=26";
                    ng++;
                    nogra = 1;
                    haydatos = 1;
                }
            }
             
            List graduadosLista = this.jdbctemplate.queryForList(sqlnomG);
            
            List nograduadosLista = this.jdbctemplate.queryForList(sqlnomN);
            
            List numG = this.jdbctemplate.queryForList(numg);
            List numN = this.jdbctemplate.queryForList(numng);

//            System.out.println("tamaño de la lista limite: " + limite.size());
            if (contarnograd.isEmpty()) {
                haydatos = 0;
            }
            
            if(nograduadosLista.isEmpty())
            {
                nograduadosLista.add(0);
            }
            if(numN.isEmpty())
            {
                numN.add(0);
            }
           
            ModelAndView modelo = new ModelAndView();
            modelo.addObject("graf", grafLista);
            modelo.addObject("numg",numG);
            modelo.addObject("numng",numN);
            modelo.addObject("prueba", contarnograd);
            modelo.addObject("nograduados", nograduadosLista);
            modelo.addObject("graduados", graduadosLista);
            modelo.addObject("nogra", nogra);
            modelo.addObject("gra", gra);
            modelo.addObject("titulo", titulotipo);
            modelo.addObject("existe", haydatos);
            modelo.addObject("id", id);
            return modelo;
        } catch (Exception e) {
            System.out.println("EXCEPCION -------->"+ e);
            ModelAndView modelo = new ModelAndView();
//            modelo.addObject("graf", grafLista);
            //modelo.addObject("nograduados", contarnograd);
            //modelo.addObject("graduados", investidura);
//            modelo.addObject("nogra", nogra);
//            modelo.addObject("gra", gra);
//            modelo.addObject("titulo", titulotipo);
            haydatos = 0;
            modelo.addObject("existe", haydatos);
            modelo.addObject("id", id);
            return modelo;
        } 
        
    }

    @ModelAttribute("periodosList")
    public Map<Integer, String> getPeriodos() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getPeriodos()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }
//    @ModelAttribute("carrerasList")
//    public Map<Integer, String> getCarreras() {
//        Map<Integer, String> periodos = new LinkedHashMap<>();
//        for (Tutor i : dao.TutorDao.getCarreras()) {
//            periodos.put(i.getCarrera(), i.getNombrecompleto());
//        }
//        return periodos;
//    }

    @ModelAttribute("tipoList")
    public Map<Integer, String> getTipo() {
        Map<Integer, String> tipo = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getTipo()) {
            tipo.put(i.getId(), i.getNombre());
        }
        return tipo;
    }
}
