/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Examencomplexivo;
import model.Items;
import model.Tutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller
public class ControladorGestionRegistroExamen {
    private JdbcTemplate jdbctemplate;
   
    public ControladorGestionRegistroExamen(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }
    
    @RequestMapping(value = "gestionarRegistroEstExamen.htm",method = RequestMethod.GET)
    public ModelAndView viewEstudianteNew(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
         int rol=Integer.parseInt(http.getParameter("rol"));
        ModelAndView modelo= new ModelAndView();                              
        modelo.addObject("id", id);
        modelo.addObject("rol", rol);
        return modelo;
    
    }
    
    @RequestMapping(value = "gestionarRegistroEstExamen.htm",method = RequestMethod.POST)
    public ModelAndView viewEstudiante(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
         int rol=Integer.parseInt(http.getParameter("rol"));
        ModelAndView modelo= new ModelAndView();
        String sql="";
        if(rol==1){
             sql ="select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, p.fechaexamencomplexivo as fecha,"
                     +" ne.nombre as niveles, it.nombre as modalidad, p.folioex as folio, p.matriculaex as matricula"
                     +" from nivelestudios ne, items it, estudiante  e JOIN examencomplexivo p"
                     +" where e.nivelestudios= ne.id AND it.id=e.modalidad AND e.tutor ="+id+"  and (e.id=p.estudiante)";
        }
        if(rol==2){
            int carrera=Integer.parseInt(http.getParameter("carreras"));
             sql ="select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, p.fechaexamencomplexivo as fecha, "
                     +" ne.nombre as niveles, it.nombre as modalidad, p.folioex as folio, p.matriculaex as matricula"
                     +" from nivelestudios ne, items it,estudiante  e JOIN examencomplexivo p"
                     +" where e.nivelestudios= ne.id AND it.id=e.modalidad AND e.id=p.estudiante and e.carrera="+carrera;
        }
        
        List datosestudiantes=this.jdbctemplate.queryForList(sql);
        int haydatos = 0;
        if(datosestudiantes.size()>0 ){
            haydatos=1;
        }
        modelo.addObject("datos",datosestudiantes);
        modelo.addObject("existe",haydatos);
        modelo.addObject("id", id);
        modelo.addObject("rol", rol);
        return modelo;
    
    }
    
    
    
    
    
    @RequestMapping(value = "editarRegistroEstExamen.htm",method = RequestMethod.GET)
    
    public ModelAndView editar(HttpServletRequest http){
        ModelAndView modelo= new ModelAndView();
        int id= Integer.parseInt(http.getParameter("id"));
        int idEst= Integer.parseInt(http.getParameter("idEst"));
         int rol=Integer.parseInt(http.getParameter("rol"));
        Examencomplexivo datos=this.seleccionarEstudiante(idEst);
        modelo.addObject("registroExamenEditado", new Examencomplexivo(datos.getFolioex(),datos.getMatriculaex(),datos.getPeriodoingresounidadtit(),datos.getPeriodocapacitacion(),datos.getFechaexamencomplexivo(),datos.getFechainvestidura(),datos.getFechagracia()));
        modelo.addObject("id",id);
        modelo.addObject("rol", rol);
        return modelo;
    }
    
    
    @RequestMapping(value = "editarRegistroEstExamen.htm",method = RequestMethod.POST)
    public ModelAndView editarDatos(@ModelAttribute("registroExamenEditado") Examencomplexivo e,HttpServletRequest http){
           int id= Integer.parseInt(http.getParameter("id"));
        int idEst= Integer.parseInt(http.getParameter("idEst"));
        int rol=Integer.parseInt(http.getParameter("rol"));
        ModelAndView modelo= new ModelAndView();
        
        this.jdbctemplate.update(
            "update examencomplexivo "
            +"set folioex=?,"    
            +" matriculaex=?,"
            +" periodoingresounidadtit=?,"        
            +" periodocapacitacion=?,"    
            +" fechaexamencomplexivo=?," 
             +" fechainvestidura=?,"
            +" fechagracia=?"  
            +" where estudiante= ?"   , 
             e.getFolioex(),e.getMatriculaex(),e.getPeriodoingresounidadtit(),e.getPeriodocapacitacion(),e.getFechaexamencomplexivo(),e.getFechainvestidura(),e.getFechagracia(),idEst
        );
        if(rol==1){
            return new ModelAndView("redirect:/tutorhome.htm?id="+id);
        }
        else{
            if(rol==2){
                return new ModelAndView("redirect:/secretariahome.htm?id="+id);
            }
        }
        return null;
    }
    
    @RequestMapping(value = "eliminarRegistroEstExamen.htm",method = RequestMethod.GET)
     public ModelAndView eliminar(HttpServletRequest http){
         int id=Integer.parseInt(http.getParameter("id"));
          int idEst= Integer.parseInt(http.getParameter("idEst"));
         ModelAndView modelo = new ModelAndView();
         
         this.jdbctemplate.update(
         "delete from examencomplexivo where estudiante=?",
         idEst
         );
         modelo.addObject("idEst", idEst);
        return new ModelAndView("redirect:/tutorhome.htm?id="+id);
     }
    
    private Examencomplexivo seleccionarEstudiante(int idEst) {
        final Examencomplexivo listaUsuarios= new Examencomplexivo();
        String consulta="select * from examencomplexivo WHERE estudiante="+idEst;
        
            
       
         return (Examencomplexivo) jdbctemplate.query(consulta, new ResultSetExtractor<Examencomplexivo>(){
            public Examencomplexivo extractData(ResultSet rs) throws SQLException{
                if(rs.next()){
                    listaUsuarios.setFolioex(rs.getString("folioex"));
                    listaUsuarios.setMatriculaex(rs.getString("matriculaex"));
                    listaUsuarios.setPeriodoingresounidadtit(rs.getInt("periodoingresounidadtit"));
                    listaUsuarios.setPeriodocapacitacion(rs.getInt("periodocapacitacion"));
                    
                    listaUsuarios.setFechaexamencomplexivo(rs.getString("fechaexamencomplexivo"));
                   
                    listaUsuarios.setFechainvestidura(rs.getString("fechainvestidura"));
                    
                    listaUsuarios.setFechagracia(rs.getString("fechagracia"));
            
                }
                return listaUsuarios;
            }   
        });
    }
        @ModelAttribute("periodosList")

    public Map<Integer, String> getPeriodos() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getPeriodos()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }
    
            @ModelAttribute("carrerasList")
    public Map<Integer, String> getCarreras() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Tutor i : dao.TutorDao.getCarreras()) {
            periodos.put(i.getCarrera(), i.getNombrecompleto());
        }
        return periodos;
    }
}
