/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import dao.HibernateUtil;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import model.Conectar;
import model.Items;
import model.Rol;
import model.Secretaria;
import model.Tutor;
import org.hibernate.Session;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
/**
 *
 * @author user
 */
@Controller
@RequestMapping("secretarianew.htm")
public class ControladorAgregarSecr {
     private JdbcTemplate jdbctemplate;
    public ControladorAgregarSecr(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }   
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView registro(HttpServletRequest http){
         int id=Integer.parseInt(http.getParameter("id"));
         
         ModelAndView modelo = new ModelAndView();
        modelo.setViewName("secretarianew");
        modelo.addObject("secretaria",new Secretaria());
        modelo.addObject("id",id);
        
        return modelo;
    }
    
   @RequestMapping(method = RequestMethod.POST)
   public ModelAndView agregarDatos(@ModelAttribute("secretaria") Secretaria s, HttpServletRequest http){
       
       int id=Integer.parseInt(http.getParameter("id"));
       Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        //Add new Secretary object
        Rol rol= new Rol();
        session.load(rol, 3);
        rol.setId(3);
        Secretaria nuevasecr = new Secretaria();
        nuevasecr.setNombrecompleto(s.getNombrecompleto());
        nuevasecr.setCedula(s.getCedula());
        nuevasecr.setClave(s.getClave());
        nuevasecr.setRol(rol);
         
        //Save the secretary in database
        session.save(nuevasecr);
 
        //Commit the transaction
        session.getTransaction().commit();
        return new ModelAndView("redirect:/administradorhome.htm?id="+id);
    }
    
}
