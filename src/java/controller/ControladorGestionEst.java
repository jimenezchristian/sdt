/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Estudiante;
import model.Items;
import model.Tutor;
import org.recursos.clasesauxi.UtilsSecure;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller
public class ControladorGestionEst {
    private JdbcTemplate jdbctemplate;
   
    public ControladorGestionEst(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }
    @RequestMapping(value = "gestionarEstudiantes.htm",method = RequestMethod.GET)
    public ModelAndView viewEstudianteNew(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
         
        ModelAndView modelo= new ModelAndView();
        
        String sql= "SELECT * from estudiante";
        
        List datosestudiantes=this.jdbctemplate.queryForList(sql);
        
        modelo.addObject("datos",datosestudiantes);
        modelo.addObject("id", id);
        
        return modelo;
    
    }
    @RequestMapping(value = "editarEst.htm",method = RequestMethod.GET)
    
    public ModelAndView editar(HttpServletRequest http){
        ModelAndView modelo= new ModelAndView();
        int id= Integer.parseInt(http.getParameter("id"));
        int idEst= Integer.parseInt(http.getParameter("idEst"));
        
        Estudiante datos=this.seleccionarEstudiante(idEst);
        
        
        modelo.addObject("estudianteEditado", new Estudiante( datos.getNombre(), datos.getApellido(), datos.getCedula(),datos.getFolio(),datos.getMatricula(),datos.getDireccion(),datos.getCarrera(), datos.getFechaingresocarrera(),datos.getPeriodomatricula(),datos.getModalidad(), datos.getNivelestudios(),datos.getConvencional(), datos.getMovil(), datos.getGenero(),datos.getEtnia(),datos.getDiscapacidad(),datos.getCorreo(),datos.getTipo(),UtilsSecure.Encriptar(datos.getClave())));
        modelo.addObject("id",id);
        
        return modelo;
    }
    
    @RequestMapping(value = "editarPerfilEst.htm", method = RequestMethod.GET)
    public ModelAndView editarEst(HttpServletRequest http) {
        ModelAndView modelo = new ModelAndView();
        int id = Integer.parseInt(http.getParameter("id"));
        System.out.println("Id de estudiante: " + id);

        Estudiante datos = this.seleccionarEstudiante(id);
        modelo.addObject("estudianteEditado1",
                new Estudiante(datos.getNombre(),
                        datos.getApellido(),
                        datos.getCedula(),
                        //datos.getClave(),
                        UtilsSecure.Desencriptar(datos.getClave()),
                        datos.getFolio(),
                        datos.getMatricula(),
                        datos.getDireccion(),
                        datos.getCarrera(),
//                        datos.getFechaingresocarrera(),
                        datos.getPeriodomatricula(),
                        datos.getModalidad(),
                        datos.getNivelestudios(),
                        datos.getConvencional(),
                        datos.getMovil(),
                        datos.getGenero(),
                        datos.getEtnia(),
                        datos.getDiscapacidad(),
                        datos.getCorreo(),
                        datos.getTipo()));
        modelo.addObject("id", id);

        return modelo;
    }

    
    @RequestMapping(value = "editarPerfilEst.htm", method = RequestMethod.POST)
    public ModelAndView editarDatosEst(@ModelAttribute("estudianteEditado1") Estudiante e,
            HttpServletRequest http) {
        System.out.println("----------------------DATOS-----------------");
        int id = Integer.parseInt(http.getParameter("id"));
//        int idEst = Integer.parseInt(http.getParameter("idEst"));
        System.out.println("Id Estudiante actual: " + id);
        ModelAndView modelo = new ModelAndView();
        int idtutor = 4;
        System.out.println("Id Tutor: " + idtutor);
//        String sql="update set estudiante  movil='"+e.getMovil()+"', correo='"+ e.getCorreo()+"',clave='"+e.getClave()+"' where id= "+id+";
        this.jdbctemplate.update(
                "update  estudiante " + "set  movil=?," + " correo=?," + " clave=?" + 
                        " where id= ?",
                                e.getMovil(), e.getCorreo(), UtilsSecure.Encriptar(e.getClave()), id
//                e.getMovil(), e.getCorreo(), e.getClave(), id
        );
        return new ModelAndView("redirect:/estudiantehome.htm?id=" + id);
    }

    
    
    //Agregar datos
    @RequestMapping(value = "editarEst.htm",method = RequestMethod.POST)
    public ModelAndView editarDatos(@ModelAttribute("estudianteEditado") Estudiante e,HttpServletRequest http ){
           int id= Integer.parseInt(http.getParameter("id"));
        int idEst= Integer.parseInt(http.getParameter("idEst"));
        
        ModelAndView modelo= new ModelAndView();
        
        String sql="select id from tutor where carrera = "+e.getCarrera();
         List<Map<String, Object>> listidTutor=this.jdbctemplate.queryForList(sql);
            int idtutor= Integer.parseInt(listidTutor.get(0).get("id").toString());
            
        
        this.jdbctemplate.update(
            "update estudiante "
            +"set nombre=?,"    
            +" apellido=?," 
             +" cedula=?,"
            +" folio=?," 
            +" matricula=?," 
             +" direccion=?," 
             +" carrera=?," 
              +"periodomatricula=?,"      
              +" modalidad=?,"        
              +" nivelestudios=?,"         
              +" convencional=?,"
              +" movil=?,"
              +" genero=?,"
              +" etnia=?,"
              +" discapacidad=?,"
            +" correo=?,"   
            +" tipo=?,"    
            +" tutor=?"   
            +" where id= ?"   , 
             e.getNombre(),e.getApellido(),e.getCedula(),
             e.getFolio(),e.getMatricula(),e.getDireccion(),e.getCarrera()
                ,e.getPeriodomatricula(),e.getModalidad(), e.getNivelestudios(), e.getConvencional(),e.getMovil(),e.getGenero(),e.getEtnia(),e.getDiscapacidad(),e.getCorreo(),e.getTipo(),idtutor,idEst
        );
        
        
            return new ModelAndView("redirect:/secretariahome.htm?id="+id);
        
        
    }
    
    
    @RequestMapping(value = "eliminarEst.htm",method = RequestMethod.GET)
     public ModelAndView eliminar(HttpServletRequest http){
         int id=Integer.parseInt(http.getParameter("id"));
          int idEst= Integer.parseInt(http.getParameter("idEst"));
         ModelAndView modelo = new ModelAndView();
         
         this.jdbctemplate.update(
         "delete from estudiante where id=?",
         idEst
         );
         modelo.addObject("idEst", idEst);
        return new ModelAndView("redirect:/secretariahome.htm?id="+id);
     }
    
    
    
    private Estudiante seleccionarEstudiante(int idEst) {
         final Estudiante listaUsuarios= new Estudiante();
        String consulta="select * from estudiante WHERE id="+idEst;
        
            
       
         return (Estudiante) jdbctemplate.query(consulta, new ResultSetExtractor<Estudiante>(){
            public Estudiante extractData(ResultSet rs) throws SQLException{
                if(rs.next()){
                    listaUsuarios.setNombre(rs.getString("nombre"));
                    listaUsuarios.setApellido(rs.getString("apellido"));
                    listaUsuarios.setCedula(rs.getString("cedula"));
                    listaUsuarios.setFolio(rs.getString("folio"));
                    listaUsuarios.setMatricula(rs.getString("matricula"));
                    listaUsuarios.setDireccion(rs.getString("direccion"));
                    listaUsuarios.setCarrera(rs.getInt("carrera"));
                    listaUsuarios.setFechaingresocarrera(rs.getString("fechaingresocarrera"));
                    listaUsuarios.setPeriodomatricula(rs.getInt("periodomatricula"));
                    listaUsuarios.setModalidad(rs.getInt("modalidad"));
                    listaUsuarios.setNivelestudios(rs.getInt("nivelestudios"));
                    listaUsuarios.setConvencional(rs.getString("convencional"));
                    listaUsuarios.setMovil(rs.getString("movil"));
                    listaUsuarios.setCorreo(rs.getString("correo"));
                    listaUsuarios.setGenero(rs.getString("genero"));
                    listaUsuarios.setEtnia(rs.getString("etnia"));
                    listaUsuarios.setDiscapacidad(rs.getString("discapacidad"));
                    listaUsuarios.setTipo(rs.getInt("tipo"));
                   listaUsuarios.setClave(rs.getString("clave"));

                   
                    
                }
                return listaUsuarios;
            }   
        });
    }
    
    
    
    @ModelAttribute("modalidadList")

    public Map<Integer, String> getModalidades() {
        Map<Integer, String> modalidades = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getModalidades()) {
            modalidades.put(i.getId(), i.getNombre());
        }
        return modalidades;
    }

    @ModelAttribute("carreraList")
    public Map<Integer, String> getCarreras() {
        Map<Integer, String> carreras = new LinkedHashMap<>();
        for (Tutor i : dao.TutorDao.getCarreras()) {
            carreras.put(i.getCarrera(), i.getNombrecompleto());
        }
        return carreras;
    }

    @ModelAttribute("nivelestudiosList")
    public Map<Integer, String> getNivelEstudios() {
        Map<Integer, String> nivelestudios = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getNivelEstudios()) {
            nivelestudios.put(i.getId().intValue(), i.getNombre());
        }
        return nivelestudios;
    }

    @ModelAttribute("periodosList")
    public Map<Integer, String> getPeriodos() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getPeriodos()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }
    
    
@ModelAttribute("tipoList")
    public Map<Integer, String> getTipo() {
        Map<Integer, String> tipo = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getTipo()) {
            tipo.put(i.getId(), i.getNombre());
        }
        return tipo;
    }
    @ModelAttribute("generoList")
    public Map<String, String> getGenero() {
        Map<String, String> genero = new LinkedHashMap<>(); 
        genero.put("F", "Femenino");
        genero.put("M", "Masculino");        
        return genero;
    }
        @ModelAttribute("disList")
    public Map<String, String> getDiscapacidad() {
        Map<String, String> disc = new LinkedHashMap<>(); 
        disc.put("Si", "Si");
        disc.put("No", "No");        
        return disc;
    }
        @ModelAttribute("etniasList")
    public Map<String, String> getEtnias() {
        Map<String, String> disc = new LinkedHashMap<>(); 
        disc.put("Mestizo", "Mestizo");
        disc.put("Indígena", "Indígena");        
        disc.put("Afroecuatoriano", "Afroecuatoriano");        
        return disc;
    }
}
