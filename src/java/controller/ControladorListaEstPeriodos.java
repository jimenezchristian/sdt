/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.itextpdf.text.DocumentException;
import com.mysql.jdbc.Connection;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Conectar;
import model.Items;
import model.Periodosunidadtitulacion;
import model.Tutor;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller
public class ControladorListaEstPeriodos {

    private JdbcTemplate jdbctemplate;
    private int per = 0;
    private int pert = 0;
    private int op = 0;
    private String fas = null;
    static Connection conn = null;

    public ControladorListaEstPeriodos() {
        Conectar con = new Conectar();
        this.jdbctemplate = new JdbcTemplate(con.conectar());
    }

    @RequestMapping(value = "listaperiodosEst.htm", method = RequestMethod.GET)
    public ModelAndView viewEstudianteNew(HttpServletRequest http) {
        int id = Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();
        int rol = Integer.parseInt(http.getParameter("rol"));
        modelo.addObject("id", id);
        modelo.addObject("rol", rol);
        return modelo;
    }

    @RequestMapping(value = "listaperiodosEst.htm", method = RequestMethod.POST)
    public ModelAndView viewEstudiante(HttpServletRequest http) throws FileNotFoundException {
        int haydatos = 0;
        int id = Integer.parseInt(http.getParameter("id"));
        int periodo = Integer.parseInt(http.getParameter("periodo"));
        int pt = Integer.parseInt(http.getParameter("periodos"));
        int opcion = Integer.parseInt(http.getParameter("opcion"));
        int fase = Integer.parseInt(http.getParameter("fases"));
        String filtrado = "";
        per = periodo;
        op = opcion;
        pert = pt;
        System.out.println("*****[-----------ID------" + id);
        System.out.println("*****[-----------PERIODO------" + per);
        System.out.println("*****[-----------PERIODO titulacion------" + pt);
        System.out.println("*****[------------OPCION---------" + op);
        try {
            this.fas = String.valueOf(fase);
            System.out.println("**************[Fase:****] " + fas);
        } catch (Exception e) {
            System.out.println("-------------------------- Error casting of type int to String ----------------------------- ");
        }
        String sqlRevision = "";
        int rol = Integer.parseInt(http.getParameter("rol"));

        if (rol == 1) {
            if (opcion == 1) {
                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido "
                        + "as apellido,e.cedula as cedula,i.nombrecompleto as opcional,"
                        + " p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p"
                        + " ON (e.tutor =" + id + " and e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN Tutor i ON i.carrera = e.carrera";
                filtrado = "Carrera";
            } else if (opcion == 2) {
                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombre as opcional, p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON (e.tutor =" + id + " and e.periodomatricula=" + periodo + " and e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = e.periodomatricula";
                filtrado = "Período de Matricula";
            } else if (opcion == 3) {
                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,"
                        + "e.cedula as cedula,i.nombre as opcional, p.temaproyecto as tema "
                        + "from estudiante  e INNER JOIN proyectoinves p ON (e.tutor =" + id
                        + " and p.periodoingresounidadtitulacion=" + pt + " and e.tipo=25 ) and "
                        + "(e.id=p.estudiante) INNER JOIN Items i ON i.id = p.periodoingresounidadtitulacion";
                System.out.println("sql de titulacion: " + sqlRevision);
                filtrado = "Período de Ingreso a Unidad de Titulación";
            } else if (opcion == 4) {
                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombre as opcional, p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON (e.tutor =" + id + " and p.periodograduacion=" + periodo + " and e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = p.periodograduacion";
                filtrado = "Período de Graduación";
            } else if (opcion == 5) {

                sqlRevision = "select distinct e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.tipofase as opcional, p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON (e.tutor =" + id + " and e.tipo=25 ) and (e.id=p.estudiante) and (p.periodoingresounidadtitulacion=" + periodo + ") INNER JOIN procesoproyectos i ON i.tipofase= " + fase + " and i.estudiante=e.id";
                filtrado = "N° de Fase";
            }
        } else {
            int carrera = Integer.parseInt(http.getParameter("carreras"));
            if (opcion == 1) {
                System.out.println("****************************1");

                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombrecompleto as opcional,"
                        + " p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON (e.carrera=" + carrera + " and e.tipo=25"
                        + " ) and (e.id=p.estudiante) INNER JOIN Tutor i ON i.carrera = e.carrera";
                filtrado = "Carrera";
            } else if (opcion == 2) {
                System.out.println("****************************2");

                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombre as opcional,"
                        + " p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON ( e.periodomatricula=" + periodo
                        + " and e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = e.periodomatricula";
                filtrado = "Período de Matricula";
            } else if (opcion == 3) {
                System.out.println("****************************3");

                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombre as opcional,"
                        + " p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON "
                        + "(p.periodoingresounidadtitulacion=" + pt + " and e.tipo=25 ) and (e.id=p.estudiante) "
                        + "INNER JOIN Items i ON i.id = p.periodoingresounidadtitulacion";

                filtrado = "Período de Ingreso a Unidad de Titulación";
            } else if (opcion == 4) {

                System.out.println("****************************4");
                sqlRevision = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombre as opcional, p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON (p.periodograduacion=" + periodo + " and e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN Items i ON i.id = p.periodograduacion";
                filtrado = "Período de Graduación";
            } else if (opcion == 5) {
                sqlRevision = "select distinct e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.tipofase as opcional, p.temaproyecto as tema from estudiante  e INNER JOIN proyectoinves p ON (e.tipo=25 ) and (e.id=p.estudiante) INNER JOIN procesoproyectos i ON i.tipofase= " + fase + " and i.estudiante=e.id";
                filtrado = "N° de Fase";
            }
        }
        System.out.println("Sql revicion: " + sqlRevision + " \n Fisltrado: " + filtrado + "\nOpcion: " + opcion);
        ModelAndView modelo = new ModelAndView();
        try {
            List datosestudiantes = this.jdbctemplate.queryForList(sqlRevision);
            if (datosestudiantes.size() > 0) {
                haydatos = 1;
            }
            modelo.addObject("rol", rol);
            modelo.addObject("datos", datosestudiantes);
            modelo.addObject("id", id);
            modelo.addObject("existe", haydatos);
            modelo.addObject("filtrado", filtrado);
            return modelo;
        } catch (Exception e) {
            modelo.addObject("rol", rol);
            modelo.addObject("id", id);
            modelo.addObject("existe", haydatos);
            modelo.addObject("filtrado", filtrado);
            return modelo;
        }
    }

//    codigo diego
    @RequestMapping(value = "reportepdftitulacion.htm", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView descargarPDFTitul(HttpServletRequest http, HttpServletResponse response) throws FileNotFoundException, JRException, IOException, DocumentException, SQLException {
        int id = Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();
        System.out.println("********************************************************");
        System.out.println("\tTutor: " + id + "\n\tPeriodo: " + per + "\n\tFase: " + fas);
        System.out.println("********************************************************");
        InputStream entrada = null;
        Map<String, Object> params = null;
        try {
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/bd_sdt", "root", "");
            conn.setAutoCommit(false);
            if (op != 0) {
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~ great ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                if (op == 1) {
                    entrada = this.getClass().getResourceAsStream("/reportes/matriculados.jrxml");
                    params = new HashMap<>();
                    params.put("idtutor", id);
                } else if (op == 2) {
                    entrada = this.getClass().getResourceAsStream("/reportes/matriculados1.jrxml");
                    params = new HashMap<>();
                    params.put("periodo", per);
                    params.put("idtutor", id);
                } else if (op == 3) {
                    entrada = this.getClass().getResourceAsStream("/reportes/reportEstu.jrxml");
                    params = new HashMap<>();
                    params.put("idPeriodoT", pert);
                    params.put("idTutor", id);

                } else if (op == 4) {

                    entrada = this.getClass().getResourceAsStream("/reportes/Graduacion.jrxml");
                    params = new HashMap<>();
                    params.put("idtutor", new Integer(id)); //
                    params.put("periodo", per); //entre comillas tu parametro del reporte en mi caso es periodo
                } else if (op == 5) {
                    if (fas.equals("1")) {
                        System.out.println("------------------------------------");
                        System.out.println(fas);
                        System.out.println(id);
                        System.out.println(op);
                        System.out.println(per);
                        entrada = this.getClass().getResourceAsStream("/reportes/Fase1.jrxml");
                        params = new HashMap<>();

                        params.put("Fase", fas);
                        params.put("tutor", id);
                        params.put("peintitu", per);
                    }
                    if (fas.equals("2")) {
                        entrada = this.getClass().getResourceAsStream("/reportes/Fase2.jrxml");
                        params = new HashMap<>();
                        params.put("tutor", id);
                        params.put("Fase", fas);
                        params.put("peintitu", per);
                    }
                    if (fas.equals("3")) {
                        entrada = this.getClass().getResourceAsStream("/reportes/Fase3.jrxml");
                        params = new HashMap<>();
                        params.put("tutor", id);
                        params.put("Fase", fas);
                        params.put("peintitu", per);
                    }
                    if (fas.equals("4")) {
                        entrada = this.getClass().getResourceAsStream("/reportes/Fase4.jrxml");
                        params = new HashMap<>();
                        params.put("tutor", id);
                        params.put("Fase", fas);
                        params.put("peintitu", per);
                    }
//                        else {
//                        }
                } else {
                    System.out.println("Ocurrio un error al descargar el reporte: ");
                }
                JasperReport jasperReport = JasperCompileManager.compileReport(entrada);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
                response.setContentType("application/x-pdf");
                response.setHeader("Content-disposition", "inline; filename=Report.pdf");
                final OutputStream outStream = response.getOutputStream();
                JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);

            } else {
                System.out.println("No existe datos");
            }
        } catch (SQLException | JRException | IOException e) {
            System.out.println("Error de conexión:...................................................................\n "
                    + e.getMessage());
//            System.exit(4);
        }

        modelo.addObject("id", id);
        modelo.addObject("periodo", per);
        return modelo;
    }
    
    @RequestMapping(value = "reporteTotalFases.htm", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView FasesTotaldescargarPDF(HttpServletRequest http, HttpServletResponse response) throws FileNotFoundException, JRException, IOException, DocumentException, SQLException {
        int id = Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();
     InputStream entrada = this.getClass().getResourceAsStream("/reportes/FasesTotal.jrxml");
    Map<String,Object> params = new HashMap<>();
    params.put("tutor",new Integer(id));
    params.put("peinunititu",new Integer(per));
    JasperReport jasperReport = JasperCompileManager.compileReport(entrada);
    try {
      conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/bd_sdt","root", "");
      conn.setAutoCommit(false);
    }
    catch (SQLException e) {
      System.out.println("Error de conexión: " + e.getMessage());
      System.exit(4);
    }
    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
    response.setContentType("application/x-pdf");
    response.setHeader("Content-disposition", "inline; filename=TotalFases.pdf");

    final OutputStream outStream = response.getOutputStream();
    JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
    //JasperExportManager.exportReportToPdfFile( jasperPrint, "C:/Users/USUARIO/Downloads/reportesdt.pdf");


        modelo.addObject("id", id);
        modelo.addObject("periodoingresounidadtitulacion",per);
        return modelo;
    }

    @RequestMapping(value = "reportePDF.htm", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView descargarPDF(HttpServletRequest http, HttpServletResponse response)
            throws FileNotFoundException, JRException, IOException, DocumentException, SQLException {
        int id = Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();
        InputStream entrada = this.getClass().getResourceAsStream("/reportes/matriculados.jrxml");
        Map<String, Object> params = new HashMap<>();
        params.put("idtutor", new Integer(id));
        JasperReport jasperReport = JasperCompileManager.compileReport(entrada);
        try {
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/bd_sdt", "root", "");
            conn.setAutoCommit(false);
        } catch (SQLException e) {
            System.out.println("Error de conexión: " + e.getMessage());
            System.exit(4);
        }
        try {
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            response.setContentType("application/x-pdf");
            response.setHeader("Content-disposition", "inline; filename=helloWorldReport.pdf");

            final OutputStream outStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
            //JasperExportManager.exportReportToPdfFile( jasperPrint, "C:/Users/USUARIO/Downloads/reportesdt.pdf");

        } catch (JRException | IOException e) {
            System.out.println("errrrrrrror: " + e.getMessage());
        }

        modelo.addObject("id", id);
        return modelo;
    }

    @ModelAttribute("periodosList")
    public Map<Integer, String> getPeriodos() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getPeriodos()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }
//    metodo para obtener periodo de titulacion

    @ModelAttribute("periodosTituList")
    public Map<Integer, String> getPeriodosTitulacion() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Periodosunidadtitulacion i : dao.PeriodosunidadtitulacionDao.getPeriodosTitulacion()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }

    @ModelAttribute("carrerasList")
    public Map<Integer, String> getCarreras() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Tutor i : dao.TutorDao.getCarreras()) {
            periodos.put(i.getCarrera(), i.getNombrecompleto());
        }
        return periodos;
    }

    @ModelAttribute("opcionesList")
    public Map<Integer, String> getOpciones() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        periodos.put(0, "Seleccione...");
        periodos.put(1, "Titulación");
        periodos.put(2, "Periodo de Matrícula");
        periodos.put(3, "Periodo de Unidad de Titulación");
        periodos.put(4, "Periodo de Graduación ");
        periodos.put(5, "Fases Proyectos Investigación ");
        return periodos;
    }

    @ModelAttribute("fasesList")
    public List getFases() {
        String sql = "select distinct fase from procesos";
        List fases = this.jdbctemplate.queryForList(sql);
        return fases;
    }

}
