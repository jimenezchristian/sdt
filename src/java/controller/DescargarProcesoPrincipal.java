/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Conectar;
import model.Procesos;
import org.apache.commons.io.IOUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import static org.springframework.util.FileCopyUtils.BUFFER_SIZE;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
/**
 *
 * @author user
 */
public class DescargarProcesoPrincipal {
    private JdbcTemplate jdbctemplate;
    
    public DescargarProcesoPrincipal(){
         Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
         
    }
 @RequestMapping(value = "descargaProcesoPrincipal.htm",method = RequestMethod.GET)
    public ModelAndView descargaArchivo(HttpServletRequest http, HttpServletResponse response){
         ModelAndView modelo= new ModelAndView();
        //int id= Integer.parseInt(http.getParameter("idDescarga"));
        int idEst= Integer.parseInt(http.getParameter("id"));
        int tipo= Integer.parseInt(http.getParameter("tipo"));
        Procesos datos=this.seleccionarArchivo( response,http,tipo);
       return new ModelAndView("redirect:/vistaprocesosest.htm?id="+idEst);
    }
     private Procesos seleccionarArchivo( final HttpServletResponse response, HttpServletRequest http,int tipo) {
         final Procesos listaArchivo= new Procesos();
         String consulta="";
         
              consulta="select * from procesoprincipal WHERE tipo = "+tipo;
         
  
         return (Procesos) jdbctemplate.query(consulta, new ResultSetExtractor<Procesos>(){
            public Procesos extractData(ResultSet rs) throws SQLException{
                while(rs.next()){
                   String nombreArchivo=rs.getString("nombreArchivo");
                    
                    Blob data = rs.getBlob("archivo");
                   
                    byte[]re=data.getBytes(1, (int)data.length());
                    
                    
                    try{

                        response.setContentType("application/octet-stream");
                        response.setContentLength(re.length);
                        response.setHeader("Content-Disposition","attachment;filename=\""+nombreArchivo+"\"");
                        FileCopyUtils.copy(re,response.getOutputStream());
                        
                    } 
                    catch (IOException ex) {
                        Logger.getLogger(ControladorDescarga.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                return listaArchivo;
            }   
        });
    }
}
