/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Items;
import model.Tutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller
public class ControladorGraficos {
    private JdbcTemplate jdbctemplate;
    private int per=0;
    int haydatos=0;
    static Connection conn = null;
    
    public ControladorGraficos(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }   
    @RequestMapping(value = "graficos.htm" , method = RequestMethod.GET)
    public ModelAndView viewEstudianteNew(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo= new ModelAndView();
        modelo.addObject("id",id);
        return modelo;
    }
    @RequestMapping(value = "graficos.htm" , method = RequestMethod.POST)
    public ModelAndView viewEstudiante(HttpServletRequest http)  {
        
         int id=Integer.parseInt(http.getParameter("id"));
         int carrera=Integer.parseInt(http.getParameter("carreras"));
         int tipo=Integer.parseInt(http.getParameter("tipo"));
         int periodo=Integer.parseInt(http.getParameter("periodo"));
         /*String sqlultimoPro= "SELECT MAX(id)as id FROM procesos";
         List<Map<String, Object>> idProceso=this.jdbctemplate.queryForList(sqlultimoPro); 
         String sqlcomprobar= "SELECT *  FROM estudiantes";*/
         
         String sqlRevision ="select i.nombrecompleto as nombre  from tutor i INNER JOIN estudiante e ON e.carrera="+carrera+" and i.carrera = e.carrera";
         String sqlPeriodo ="select i.nombre as periodo  from items i INNER JOIN estudiante e ON e.periodomatricula="+periodo+" and i.id = e.periodomatricula";
         String sqlPrueba="SELECT e.graduacion as conclusion, count(*) as graduacion FROM estudiante e INNER JOIN Items i ON (e.periodomatricula=i.id and e.tipo = "+tipo +") and (e.periodomatricula = "+periodo+" and e.carrera = "+carrera+" ) group by graduacion ";
         String sqlNograduados="select nombre, apellido, cedula from estudiante where (tipo="+tipo+" and graduacion = 0) and (carrera = "+carrera+" and periodomatricula = "+periodo+")";
         String sqlGraduados="select nombre, apellido, cedula from estudiante where (tipo="+tipo+" and graduacion = 1) and (carrera = "+carrera+" and periodomatricula = "+periodo+")";
         String titulotipo="";
                 System.out.println("---------------------------------");
         System.out.println("sqlRevision: "+sqlRevision);
         System.out.println("sqlPeriodo: "+sqlPeriodo);
         System.out.println("sqlPrueba: "+sqlPrueba);
         System.out.println("sqlNograduados: "+sqlNograduados);
         System.out.println("sqlGraduados: "+sqlGraduados);
         if(tipo==25){
             titulotipo = "Proyectos de Investigación";
         }
         if(tipo==26){
             titulotipo = "Exámen Complexivo";
         }
        
        
        try{
         List carrerasLista=this.jdbctemplate.queryForList(sqlRevision);
         List <Map<String,Object>>periodosLista=this.jdbctemplate.queryForList(sqlPeriodo);
         String pe=periodosLista.get(0).get("periodo").toString();
         List pruebaLista=this.jdbctemplate.queryForList(sqlPrueba);
         List nograduadosLista=this.jdbctemplate.queryForList(sqlNograduados);
         List graduadosLista=this.jdbctemplate.queryForList(sqlGraduados);
         int nogra=0;
         int gra=0;
         if(pruebaLista.size()>0){
             haydatos=1;
             if(nograduadosLista.size()>0){
                 nogra=1;
             }
             if(graduadosLista.size()>0){
                 gra=1;
             }
         }
         if(pruebaLista.isEmpty()){
             haydatos=0;
         }
         
         ModelAndView modelo= new ModelAndView();
         modelo.addObject("datos",carrerasLista);
         modelo.addObject("periodo",pe);
         modelo.addObject("prueba",pruebaLista);
         modelo.addObject("nograduados",nograduadosLista);
         modelo.addObject("graduados",graduadosLista);
         modelo.addObject("nogra",nogra);
         modelo.addObject("gra",gra);
         modelo.addObject("titulo",titulotipo);
         modelo.addObject("existe",haydatos);
         modelo.addObject("id",id);
         
         return modelo;
        }
        catch(Exception e){
            
         
         ModelAndView modelo= new ModelAndView();
         haydatos=0;
         modelo.addObject("existe",haydatos);
         modelo.addObject("id",id);
         
         return modelo;
        }
    }         
    @ModelAttribute("periodosList")
    public Map<Integer, String> getPeriodos() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getPeriodos()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }
    
    @ModelAttribute("carrerasList")
    public Map<Integer, String> getCarreras() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Tutor i : dao.TutorDao.getCarreras()) {
            periodos.put(i.getCarrera(), i.getNombrecompleto());
        }
        return periodos;
    }
    
    @ModelAttribute("tipoList")
    public Map<Integer, String> getTipo() {
        Map<Integer, String> tipo = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getTipo()) {
            tipo.put(i.getId(), i.getNombre());
        }
        return tipo;
    }
    
}
