/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Conectar;
import model.Periodomatricula;
import model.Items;
import model.Periodosunidadtitulacion;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
/**
 *
 * @author user
 */
public class ControladorGestionPeriodo {
    private JdbcTemplate jdbctemplate;
   
    public ControladorGestionPeriodo(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }
    @RequestMapping(value = "gestionarPeriodo.htm",method = RequestMethod.GET)
    public ModelAndView viewPeriodoNew(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
         
        ModelAndView modelo= new ModelAndView();
        
        String sql= "SELECT * from items WHERE catalogo = 2";
        
        List datosper=this.jdbctemplate.queryForList(sql);
        
        modelo.addObject("datos",datosper);
        modelo.addObject("id", id);
        
        return modelo;
    
    }
//    @RequestMapping(value = "editarSecretaria.htm",method = RequestMethod.GET)
//    
//    public ModelAndView editar(HttpServletRequest http){
//        ModelAndView modelo= new ModelAndView();
//        int id= Integer.parseInt(http.getParameter("id"));
//        int idSecr= Integer.parseInt(http.getParameter("idSecr"));
//        
//        Secretaria datos=this.seleccionarSecretaria(idSecr);
//        
//        
//        modelo.addObject("secretariaEditado", new Secretaria( datos.getNombrecompleto(), datos.getCedula(), datos.getClave()));
//        modelo.addObject("id",id);
//        
//        return modelo;
//    }
//    
//    
//    //Agregar datos
//     @RequestMapping(value = "editarSecretaria.htm",method = RequestMethod.POST)
//    public ModelAndView editarDatos(@ModelAttribute("secretariaEditado") Secretaria s,HttpServletRequest http ){
//           int id= Integer.parseInt(http.getParameter("id"));
//        int idSecr= Integer.parseInt(http.getParameter("idSecr"));
//        
//        ModelAndView modelo= new ModelAndView();
//        
//
//        this.jdbctemplate.update(
//            "update secretaria "
//            +"set nombrecompleto=?,"     
//             +" cedula=?,"
//            +" clave=? "    
//            +" where id= ?"   , 
//             s.getNombrecompleto(),s.getCedula(),s.getClave(),idSecr );
//        
//        
//            return new ModelAndView("redirect:/administradorhome.htm?id="+id);
//        
//        
//    }
    
    
    @RequestMapping(value = "eliminarPeriodo.htm",method = RequestMethod.GET)
     public ModelAndView eliminar(HttpServletRequest http){
         int id=Integer.parseInt(http.getParameter("id"));
         //int idPer = Integer.parseInt(http.getParameter("idPer"));
         String idNombre= (http.getParameter("idNombre"));
         ModelAndView modelo = new ModelAndView();
         
         this.jdbctemplate.update(
         "delete from items where nombre=?",
         idNombre
         );
         
         this.jdbctemplate.update(
         "delete from periodomatricula where periodomatricula = '"+
         idNombre +"'"
         );
         
         this.jdbctemplate.update(
         "delete from periodosunidadtitulacion where nombre = '" +
         idNombre +"'"
         );
         this.jdbctemplate.update(
         "delete from periodograduacion where periodograduacion = '" +
         idNombre +"'"
         );
         
         modelo.addObject("idNombre", idNombre);
        return new ModelAndView("redirect:/administradorhome.htm?id="+id);
     }

    private Items seleccionarSecretaria(int idPer) {
         final Items listaUsuarios= new Items();
        String consulta="select * from items WHERE id="+idPer;
        
            
       
         return (Items) jdbctemplate.query(consulta, new ResultSetExtractor<Items>(){
            public Items extractData(ResultSet rs) throws SQLException{
                if(rs.next()){
                    listaUsuarios.setNombre(rs.getString("nombre"));
                    
                }
                return listaUsuarios;
            }   
        });
    }
    
}
