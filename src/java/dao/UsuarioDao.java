/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import model.Administrador;
import model.Estudiante;
import model.Tutor;
import model.Secretaria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.recursos.clasesauxi.UtilsSecure;

public class UsuarioDao {

    public static Tutor getTutor(String cedula, String clave) {
        Tutor tutor = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Tutor where cedula='" + cedula + "' and clave='" + clave + "'");
            System.out.println("query tutor: "+q.toString());
            tutor = (Tutor) q.uniqueResult();
        } catch (HibernateException e) {
            System.out.println("Query error de tutor: "+e.getMessage()+" "+e.getCause());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return tutor;
    }

    public static Estudiante getEstudiante(String cedula, String clave) {
        Estudiante estudiante = null;
        Session s = null;
        String encry=UtilsSecure.Encriptar(clave);
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Estudiante where cedula='" + 
                    cedula + "' and clave='" + encry + "'");
//            Query q = s.createQuery("from Estudiante where cedula='" + cedula + "' and clave='" + clave + "'");
            System.out.println(q);
            estudiante = (Estudiante) q.uniqueResult();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return estudiante;
    }

    public static Secretaria getSecretaria(String cedula, String clave) {
        Secretaria secreataria = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Secretaria where cedula='" + cedula + "' and clave='" + clave + "'");
            System.out.println("query tutor: "+q.toString());
            secreataria = (Secretaria) q.uniqueResult();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return secreataria;
    }

    public static Administrador getAdministrador(String cedula, String clave) {
        Administrador administrador = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Administrador where cedula='" + cedula + "' and clave='" + clave + "'");
            administrador = (Administrador) q.uniqueResult();
            
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return administrador;
    }
    
    public static void main(String[] args) {
//        System.out.println(getEstudiante("1104391774", "admin").getApellido());
    }
}
