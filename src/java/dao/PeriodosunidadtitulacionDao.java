/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import model.Periodosunidadtitulacion;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
/**
 *
 * @author Diego
 */
public class PeriodosunidadtitulacionDao {
         public static List<Periodosunidadtitulacion> getPeriodosTitulacion() {
        List<Periodosunidadtitulacion> items = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Periodosunidadtitulacion");
            items = q.list();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return items;
    }
}
