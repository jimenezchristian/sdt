/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.Estudiante;
import model.Secretaria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EstudianteDao {
    
    public static Estudiante getEstudiante(int id) {
        Estudiante estudiante = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Estudiante where id='" + id + "'");
            
            estudiante = (Estudiante) q.uniqueResult();
            
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return estudiante;
    }

    public static boolean guardar(Estudiante estudiante) {
        Transaction t = null;
        Session s = null;
        boolean res = false;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            t = s.beginTransaction();
            s.save(estudiante);
            t.commit();
            res = true;
        } catch (HibernateException e) {
            if (t != null) {
                t.rollback();
            }
            res = false;
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return res;
    }

//    public static List<Estudiante> estudiante(Integer id) {
//        List<Estudiante> list = null;
//        Session s = null;
//        try {
//            s = HibernateUtil.getSessionFactory().openSession();
//            Query q = s.createQuery("from Estudiante where tutor =" + id+" ORDER BY nombre");
//            list = q.list();
//        } catch (HibernateException e) {
//        } finally {
//            if (s != null) {
//                s.close();
//            }
//        }
//        return list;
//        
//    }
    public static List<Estudiante> findEstudiante(Integer id, String palabra,int idTutor) {
        List<Estudiante> lst = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            String sql = ("from Estudiante where tutor = "+idTutor+" and (nombre LIKE '%" + palabra + "%' or apellido LIKE '%" + palabra + "%' OR cedula LIKE '%" + palabra + "%' ORDER BY apellido)");
            
            Query query = session.createQuery(sql);
            lst = query.list();
            System.out.println("Tamnio de la lista " + lst.size() + " con este id");
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lst;

    }
    
    public static List<Estudiante> seleccionarEstudiantes(int id) {
        List<Estudiante> lst = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            String sql = ("from Estudiante where tutor="+id);
            
            Query query = session.createQuery(sql);
            lst = query.list();
            System.out.println(lst+" estos esta consultando ////////////////////////////////////");
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lst;
    }
    public static List<Estudiante> seleccionarTodosEstudiantes() {
        List<Estudiante> lst = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            String sql = ("from Estudiante where tipo = 25 ");
            Query query = session.createQuery(sql);
            lst = query.list();
                
            
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lst;
    }
    
    public static List<Estudiante> seleccionarTodosEstudiantesComplexivo() {
        List<Estudiante> lst = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            String sql = ("from Estudiante where tipo=26");
            Query query = session.createQuery(sql);
            lst = query.list();
            
            
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lst;
    }
    
    public static Estudiante getEstudiantesExamen(int id) {
        Estudiante estudiante = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Estudiante where id='" + id + "'");
            
            estudiante = (Estudiante) q.uniqueResult();
            
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return estudiante;
    }
        
}
