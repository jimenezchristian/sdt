/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.Conectar;

import model.Procesos;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author USUARIO
 */
public class ProcesoDao {
    private JdbcTemplate jdbctemplate;
      public ProcesoDao(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }
    public  List seleccionarProcesos() {
         List lst=null;
        try {
            String sql= "select id,nombreProceso from procesos ";
        
        lst=this.jdbctemplate.queryForList(sql);
        
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lst;
    }
}
