/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import model.Secretaria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author usario
 */
public class SecretariaDao {
     public static Secretaria getSecretaria(int id) {
        Secretaria secretaria = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Secretaria where id='" + id + "'");
            
            secretaria = (Secretaria) q.uniqueResult();
            
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return secretaria;
    }
    
}
