/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import model.Rol;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;


public class RolDao {

    public static Rol getId(int id) {
        Rol rol = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Rol where id=" + id);
            rol = (Rol) q.uniqueResult();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return rol;
    }
//
//    public static void main(String[] args) {
//        System.out.println(getId(4).getNombre());
//    }
}
