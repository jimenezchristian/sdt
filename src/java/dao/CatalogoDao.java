/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.Items;
import model.Tutor;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

public class CatalogoDao {

    public static List<Items> getModalidades() {
        List<Items> items = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Items where catalogo=3 and activo=1");
            items = q.list();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return items;
    }

     public static List<Items> getCarreras() {
        List<Items> items = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Items where catalogo=2 and activo=1");
            items = q.list();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return items;
    }
     
     public static List<Items> getNivelEstudios() {
        List<Items> items = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Items where catalogo=4 and activo=1");
            items = q.list();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return items;
    }
     
     public static List<Items> getPeriodos() {
        List<Items> items = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Items where catalogo=2 and activo=1");
            items = q.list();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return items;
    } 
     
     public static List<Items> getTipo() {
        List<Items> items = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Items where catalogo=5 and activo=1");
            items = q.list();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return items;
    } 
     
     
     
     
}
