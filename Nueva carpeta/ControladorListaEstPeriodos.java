/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/*import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;*/
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Conectar;
import model.Items;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author USUARIO
 */
@Controller
public class ControladorListaEstPeriodos {
        private JdbcTemplate jdbctemplate;
        private int per=0;
    
    public ControladorListaEstPeriodos(){
        Conectar con= new Conectar();
         this.jdbctemplate= new JdbcTemplate(con.conectar());
    }
    
        @RequestMapping(value = "listaperiodosEst.htm" , method = RequestMethod.GET)
    public ModelAndView viewEstudianteNew(HttpServletRequest http) {
         int id=Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo= new ModelAndView();
        
        
        modelo.addObject("id",id);
        
        return modelo;
    }
    
    @RequestMapping(value = "listaperiodosEst.htm" , method = RequestMethod.POST)
    public ModelAndView viewEstudiante(HttpServletRequest http) throws FileNotFoundException {
        int haydatos=0;
         int id=Integer.parseInt(http.getParameter("id"));
         
         int periodo=Integer.parseInt(http.getParameter("periodo"));
         per=periodo;
         
        ModelAndView modelo= new ModelAndView();
        
        String sqlRevision ="select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula, p.temaproyecto as tema from estudiante  e JOIN proyectoinves p where (e.tutor ="+id+" and e.periodounidadtitulacion="+periodo+" and e.tipo=25) and (e.id=p.estudiante) ";
        List datosestudiantes=this.jdbctemplate.queryForList(sqlRevision);
        if(datosestudiantes.size()>0){
            haydatos=1;
        }
            
        
        modelo.addObject("datos",datosestudiantes);
        modelo.addObject("id",id);
        modelo.addObject("existe",haydatos);
        
        /// 
        ///////////////codigo pdf//////////////////////////
    
        
        return modelo;
    }
    
    
    @RequestMapping(value = "reportePDF.htm", method = RequestMethod.GET)
    public ModelAndView descargarPDF(HttpServletRequest http, HttpServletResponse response) throws FileNotFoundException, JRException, IOException {
        int id = Integer.parseInt(http.getParameter("id"));
        ModelAndView modelo = new ModelAndView();
// Se crea el documento
        /*Document documento = new Document();

        try {

            response.setContentType("application/octet-stream");

            response.setHeader("Content-Disposition", "attachment;filename=reportePDF.pdf");
            //FileCopyUtils.copy(documento.getB,response.getOutputStream());

// Se asocia el documento al OutputStream y se indica que el espaciado entre
// lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento, response.getOutputStream()).setInitialLeading(20);
            // Se abre el documento.
            documento.open();

            documento.add(new Paragraph("Estudiantes registrados con trabajos de Titulación",
                    FontFactory.getFont("arial", // fuente
                            19, // tamaño
                            Font.BOLD, // estilo
                            BaseColor.CYAN)));             // color
            documento.add(new Paragraph("\n"));

            PdfPTable tabla = new PdfPTable(7);
            
            tabla.addCell("Nombres");
            tabla.addCell("Apellidos");
            tabla.addCell("Cedula");
            tabla.addCell("tema de tesis");
            tabla.addCell("Área");
            tabla.addCell("Evaluador 1");
            tabla.addCell("Evaluador 2");
            String sqlProfesor = "select nombrecompleto from tutor where id=" + id;
            String sqlPDF = "select e.id as id,e.nombre as nombre ,e.apellido as apellido,e.cedula as cedula,i.nombre as periodo ,p.temaproyecto as tema, p.lineainvestigacion as lineainvestigacion,p.nombresevaluador1 as eval1,nombresevaluador2 as eval2 from estudiante  e inner JOIN proyectoinves p on (e.tutor =" + id + " and e.periodounidadtitulacion=" + per + " and e.tipo=25) and e.id=p.estudiante inner join items i on e.periodounidadtitulacion =i.id ";
            List<Map<String, Object>> tutor = this.jdbctemplate.queryForList(sqlProfesor);
            List<Map<String, Object>> datosestudiantes2 = this.jdbctemplate.queryForList(sqlPDF);
            documento.add(new Paragraph("Tutor:",
                    FontFactory.getFont("arial", // fuente
                            13, // tamaño
                            Font.BOLD // estilo
                    )));             // color
            documento.add(new Paragraph(tutor.get(0).get("nombrecompleto").toString()));
            documento.add(new Paragraph("\n"));

            documento.add(new Paragraph("Período:",
                    FontFactory.getFont("arial", // fuente
                            13, // tamaño
                            Font.BOLD // estilo
                    )));             // color

            documento.add(new Paragraph(datosestudiantes2.get(0).get("periodo").toString()));
            documento.add(new Paragraph("\n"));
            documento.add(new Paragraph("\n"));

            for (int i = 0; i < datosestudiantes2.size(); i++) {
                tabla.addCell(datosestudiantes2.get(i).get("nombre").toString());
                tabla.addCell(datosestudiantes2.get(i).get("apellido").toString());
                tabla.addCell(datosestudiantes2.get(i).get("cedula").toString());
                tabla.addCell(datosestudiantes2.get(i).get("tema").toString());
                tabla.addCell(datosestudiantes2.get(i).get("lineainvestigacion").toString());
                tabla.addCell(datosestudiantes2.get(i).get("eval1").toString());
                tabla.addCell(datosestudiantes2.get(i).get("eval2").toString());
            }
            documento.add(tabla);
            documento.close();
        } catch (IOException ex) {

        }*/
/////ireport
       JasperReport jasperReport = JasperCompileManager.compileReport("bd_sdt.jrxml");
    Map<String,Object> params = new HashMap<>();
    
    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());

    response.setContentType("application/x-pdf");
    response.setHeader("Content-disposition", "inline; filename=helloWorldReport.pdf");

    final OutputStream outStream = response.getOutputStream();
    JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);


        modelo.addObject("id", id);
        return modelo;
    }

    @ModelAttribute("periodosList")
    public Map<Integer, String> getPeriodos() {
        Map<Integer, String> periodos = new LinkedHashMap<>();
        for (Items i : dao.CatalogoDao.getPeriodos()) {
            periodos.put(i.getId(), i.getNombre());
        }
        return periodos;
    }
}
