public class calcHeap {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// s * int (32bit)
		long s = 1000;
		long size = s * s * s;
		calcIntegerHeap(size);

	}

	/**
	 * Gibt das Speichervolumen in MB zur�ck
	 * @param size (count of values)
	 * @return size memoryvolume (in MB)
	 */
	public static long calcIntegerHeap(long size) {
		// Bit-size
		long l = 32 * size;
		System.out.println("b: " + l);
		// Byte-size
		l = l / 8;
		System.out.println("B: " + l);
		// KByte-size
		l = l / 1024;
		System.out.println("KB: " + l);
		// MB-size
		l = l / 1024;
		System.out.println("MB: " + l);
		// count of values
		System.out.println(size + " values");
		// r�ckgabe in MB
		return l;

	}

}
