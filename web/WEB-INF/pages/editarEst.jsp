
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->

<html lang="en">

    <head>

        <!-- Basic -->
        <title>Editar Estudiante</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Margo - Responsive HTML5 Template">
        <meta name="author" content="iThemesLab">

        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/recursos/asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/recursos/font-awesome/css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/style.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/responsive.css" media="screen">

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/animate.css" media="screen">

        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/red.css" title="red" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/jade.css" title="jade" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/blue.css" title="blue" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/beige.css" title="beige" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/cyan.css" title="cyan" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/green.css" title="green" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/orange.css" title="orange" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/peach.css" title="peach" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/pink.css" title="pink" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/purple.css" title="purple" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/sky-blue.css" title="sky-blue" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/yellow.css" title="yellow" media="screen" />


        <!-- Margo JS  -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.migrate.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/modernizrr.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.appear.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/count-to.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.textillate.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.lettering.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.parallax.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.slicknav.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/readonly.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(function () {
                $("#datepicker").datepicker();
            });
        </script>
        <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    </head>
    <script>
        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '< Ant',
            nextText: 'Sig >',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            weekHeader: 'Sm',
            dateFormat: "yy-mm-dd",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
        $(function () {
            $("#datepicker").datepicker();
        });
    </script>
    <body>

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->
            <div class="hidden-header"></div>
            <header class="clearfix">
                <!-- Start Header ( Logo & Naviagtion ) -->
                <div class="navbar navbar-default navbar-top">
                    <div class="container">
                        <div class="navbar-header">
                            <!-- Stat Toggle Nav Link For Mobiles -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                            <!-- End Toggle Nav Link For Mobiles -->
                            <!--<a class="navbar-brand" href="index.html"><img alt="" src="recursos/img/uta.png" width="20%"></a>-->
                        </div>
                        <div class="navbar-collapse collapse">
                            <!-- Start Navigation List -->
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a class="active" href="tutorhome.htm">Home</a>
                                </li>
                                <li>
                                    <a href="contactos.htm">Contactos</a>
                                </li>
                            </ul>
                            <!-- End Navigation List -->
                        </div>
                    </div>


                    <!-- Mobile Menu Start -->
                    <ul class="wpb-mobile-menu">
                        <li>
                            <a class="active" href="tutorhome.htm">Home</a>
                        </li>
                        <li>
                            <a href="contactos.htm">Contactos</a>
                        </li>
                    </ul>
                    <!-- Mobile Menu End -->

                </div>
                <!-- End Header ( Logo & Naviagtion ) -->

            </header>
            <!-- End Header -->

            <div class="hr5" style="margin-top:60px; margin-bottom:50px;"></div>
            <!-- Start Content -->
            <div id="content">
                <div class="container">

                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="text-center"> Datos del Estudiante</h1> 

                            <form:form method="post" cssClass="form-horizontal" commandName="estudianteEditado" enctype="multipart/form-data" id="formulario">
                                <div class="form-group">
                                    <form:label path="nombre" cssClass="col-lg-2 control-label">Nombres</form:label>
                                        <div class="col-lg-4">
                                        <form:input path="nombre" cssClass="form-control"  required="required"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="apellido" cssClass="col-lg-2 control-label">Apellidos</form:label>
                                        <div class="col-lg-4">
                                        <form:input path="apellido" cssClass="form-control"  required="required"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="cedula" cssClass="col-lg-2 control-label">Cédula</form:label>
                                        <div class="col-lg-3">
                                        <form:input path="cedula" cssClass="form-control" type="text"  required="required"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="folio" cssClass="col-lg-2 control-label">Folio Ingreso a Carrera</form:label>
                                        <div class="col-lg-3">
                                        <form:input path="folio" cssClass="form-control"  required="required" maxlength="11"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="matricula" cssClass="col-lg-2 control-label">Matricula Ingreso a Carrera</form:label>
                                        <div class="col-lg-3">
                                        <form:input path="matricula" cssClass="form-control"  required="required" maxlength="11"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="direccion" cssClass="col-lg-2 control-label">Dirección</form:label>
                                        <div class="col-lg-8">
                                        <form:input path="direccion" cssClass="form-control" required="required"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="carrera" cssClass="col-lg-2 control-label">Carrera</form:label>
                                        <div class="col-lg-3">
                                        <form:select path="carrera"  items="${carreraList}" cssClass="form-control" /> 
                                    </div>
                                </div>
                                <!--                                <div class="form-group">
                                <form:label path="fechaingresocarrera" cssClass="col-lg-2 control-label">Fecha Ingreso a Carrera</form:label>
                                    <div class="col-lg-3">
                                <form:input path="fechaingresocarrera" cssClass="form-control"  required="required" id="datepicker"/>
                            </div>
                        </div>-->
                                <div class="form-group">
                                    <form:label path="periodomatricula" cssClass="col-lg-2 control-label">Período de Ingreso a Carrera</form:label>
                                        <div class="col-lg-4">
                                        <form:select path="periodomatricula" items="${periodosList}" cssClass="form-control"/>      
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="modalidad" cssClass="col-lg-2 control-label">Modalidad</form:label>
                                        <div class="col-lg-10">
                                        <form:radiobuttons path="modalidad" items="${modalidadList}" cssClass="checkbox-inline" required="required"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="nivelestudios" cssClass="col-lg-2 control-label">Nivel de Estudios</form:label>
                                        <div class="col-lg-3">
                                        <form:select path="nivelestudios" items="${nivelestudiosList}" cssClass="form-control"/> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="convencional" cssClass="col-lg-2 control-label"> Teléfono Convencional</form:label>
                                        <div class="col-lg-2">
                                        <form:input path="convencional" cssClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="movil" cssClass="col-lg-2 control-label"> Teléfono Movil</form:label>
                                        <div class="col-lg-2">
                                        <form:input path="movil" cssClass="form-control"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="genero" cssClass="col-lg-2 control-label">Género</form:label>
                                        <div class="col-lg-3">
                                        <form:select path="genero" items="${generoList}" cssClass="form-control"/> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="etnia" cssClass="col-lg-2 control-label">Etnia</form:label>
                                        <div class="col-lg-3">
                                        <form:select path="etnia" items="${etniasList}" cssClass="form-control"/> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="discapacidad" cssClass="col-lg-2 control-label">Discapacidad</form:label>
                                        <div class="col-lg-3">
                                        <form:select path="discapacidad" items="${disList}" cssClass="form-control"/> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="correo" cssClass="col-lg-2 control-label">Correo electrónico</form:label>
                                        <div class="col-lg-2">
                                        <form:input path="correo" cssClass="form-control"  required="required"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <form:label path="tipo" cssClass="col-lg-2 control-label">Modalidad de Titulación</form:label>
                                        <div class="col-lg-3">
                                        <form:select path="tipo" items="${tipoList}" cssClass="form-control"/> 
                                    </div>
                                </div>    
                                <div class="form-group">
                                    <label for="ejemplo_email_3" class="col-lg-2 control-label"></label>
                                    <div class="col-lg-10">
                                        <button type="submit" class="btn btn-default"><span class="fa fa-save"></span> Guardar</button>
                                    </div>
                                </div>
                            </form:form>  


                            <a href="<c:url value="secretariahome.htm?id=${id}"/>" class="btn btn-success">  Cancelar</a>

                        </div>
                    </div>
                    <!-- Divider -->
                    <div class="hr5" style="margin-top:60px; margin-bottom:50px;"></div>

                    <!--Start Clients Carousel-->
                    <!-- End Clients Carousel -->
                </div>
            </div>
            <!-- End Content -->


            <!-- Start Footer -->
            <footer>
                <div class="container">
                    <div class="row footer-widgets">
                        <!-- Start Contact Widget -->
                        <div class="col-md-4">
                            <div class="footer-widget contact-widget">
                                <h4><img src="recursos/img/uta.png" class="img-responsive" alt="Footer Logo"/></h4>
                                <h3>Facultad de Ciencias Humanas y de la Educación</h3>
                                <p></p>
                                <ul>
                                    <li><span>Número:</span> 032 523039</li>
                                    <li><span>Email:</span>fche@uta.edu.ec</li>
                                    <li><span>Website:</span> http://www.uta.edu.ec/</li>
                                </ul>
                            </div>
                        </div>
                        <!-- .col-md-3 -->
                        <!-- End Contact Widget -->


                    </div>
                    <!-- .row -->

                    <!-- Start Copyright -->
                    <div class="copyright-section">
                        <div class="row">
                            <div class="col-md-6">
                                <p>UNIVERSIDAD TÉCNICA DE AMBATO</a> </p>
                            </div>
                        </div>
                    </div>
                    <!-- End Copyright -->

                </div>
            </footer>
            <!-- End Footer -->

        </div>
        <!-- End Container -->

        <!-- Go To Top Link -->
        <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

        <!-- Style Switcher -->

        <script type="text/javascript" src="recursos/js/script.js"></script>
        <div class="switcher-box">
            <a class="open-switcher show-switcher"><i class="fa fa-cog fa-2x"></i></a>
            <h4>Estilos</h4>
            <span>Colores</span>
            <ul class="colors-list">
                <li>
                    <a onClick="setActiveStyleSheet('blue'); return false;" title="Blue" class="blue"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('sky-blue'); return false;" title="Sky Blue" class="sky-blue"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('cyan'); return false;" title="Cyan" class="cyan"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('jade');
                            return false;" title="Jade" class="jade"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('green');
                            return false;" title="Green" class="green"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('purple');
                            return false;" title="Purple" class="purple"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('pink'); return false;" title="Pink" class="pink"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('red');
                            return false;" title="Red" class="red"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('orange');
                            return false;" title="Orange" class="orange"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('yellow'); return false;" title="Yellow" class="yellow"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('peach');
                            return false;" title="Peach" class="peach"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('beige');
                            return false;" title="Biege" class="beige"></a>
                </li>
            </ul>
        </div>
    </body>
    <script>
        <c:if test="${rol==1}">
        alert(${rol} + " es uno");
        document.getElementById("folio").readOnly = true;
        document.getElementById("matricula").readOnly = true;
        document.getElementById("observaciones").readOnly = true;
        </c:if>
        <c:if test="${rol==2}">
        alert(${rol} + " es dos");
        document.getElementById("folio").readOnly = false;
        document.getElementById("matricula").readOnly = false;
        </c:if>
    </script>

    <script >
        document.getElementById("periodo").disabled = true;
    </script>
    <script>
        $('#formulario').submit(function () {
            $('#periodo').removeAttr('disabled');
        });
    </script>
</html>