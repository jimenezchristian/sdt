
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Sistema de Titulación</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Margo - Responsive HTML5 Template">
        <meta name="author" content="iThemesLab">

        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="css/animate.css" media="screen">

        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="css/colors/red.css" title="red" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/colors/jade.css" title="jade" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="blue" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/colors/beige.css" title="beige" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/colors/cyan.css" title="cyan" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/colors/green.css" title="green" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/colors/orange.css" title="orange" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/colors/peach.css" title="peach" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/colors/pink.css" title="pink" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/colors/purple.css" title="purple" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/colors/sky-blue.css" title="sky-blue" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/colors/yellow.css" title="yellow" media="screen" />


        <!-- Margo JS  -->
        <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/jquery.migrate.js"></script>
        <script type="text/javascript" src="js/modernizrr.js"></script>
        <script type="text/javascript" src="asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="js/jquery.appear.js"></script>
        <script type="text/javascript" src="js/count-to.js"></script>
        <script type="text/javascript" src="js/jquery.textillate.js"></script>
        <script type="text/javascript" src="js/jquery.lettering.js"></script>
        <script type="text/javascript" src="js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="js/jquery.parallax.js"></script>
        <script type="text/javascript" src="js/jquery.slicknav.js"></script>

        <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body>

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->
            <div class="hidden-header"></div>
            <header class="clearfix">
                <!-- Start Header ( Logo & Naviagtion ) -->
                <div class="navbar navbar-default navbar-top">
                    <div class="container">
                        <div class="navbar-header">
                            <!-- Stat Toggle Nav Link For Mobiles -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                            <!-- End Toggle Nav Link For Mobiles -->
                            <a class="navbar-brand" href="index.html"><img alt="" src="img/uta.png" width="20%"></a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <!-- Start Navigation List -->
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a class="active" href="home.htm">Home</a>
                                </li>
                                <li>
                                    <a href="contact.html">Contactos</a>
                                </li>
                            </ul>
                            <!-- End Navigation List -->
                        </div>
                    </div>

                    <!-- Mobile Menu Start -->
                    <ul class="wpb-mobile-menu">
                        <li>
                            <a class="active" href="index.html">Home</a>
                        </li>
                        <li>
                            <a href="contact.html">Contactos</a>
                        </li>
                    </ul>
                    <!-- Mobile Menu End -->

                </div>
                <!-- End Header ( Logo & Naviagtion ) -->

            </header>
            <!-- End Header -->

            <div class="hr5" style="margin-top:60px; margin-bottom:50px;"></div>
            <!-- Start Content -->
            <div id="content">
                <div class="container">

                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="text-center"> Datos del Estudiante</h1>
                            <form:form method="post" cssClass="form-horizontal" >
                                <div class="form-group">
                                    <form:label path="nombre" cssClass="col-lg-2 control-label">Nombres</form:label>
                                        <div class="col-lg-4">
                                        <form:input path="nombre" cssClass="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="apellido" cssClass="col-lg-2 control-label">Apellidos</form:label>
                                        <div class="col-lg-4">
                                        <form:input path="apellido" cssClass="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="cedula" cssClass="col-lg-2 control-label">Cédula</form:label>
                                        <div class="col-lg-3">
                                        <form:input path="cedula" cssClass="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="direccion" cssClass="col-lg-2 control-label">Dirección</form:label>
                                        <div class="col-lg-8">
                                        <form:input path="direccion" cssClass="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="carrera" cssClass="col-lg-2 control-label">Carrera</form:label>
                                        <div class="col-lg-3">
                                        <form:select path="carrera" items="${carreraList}" cssClass="form-control"/> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="modalidad" cssClass="col-lg-2 control-label">Modalidad</form:label>
                                        <div class="col-lg-10">
                                        <form:radiobuttons path="modalidad" items="${modalidadList}" cssClass="checkbox-inline"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="nivelestudios" cssClass="col-lg-2 control-label">Nivel de Estudios</form:label>
                                        <div class="col-lg-3">
                                        <form:select path="carrera" items="${nivelestudiosList}" cssClass="form-control"/> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="convencional" cssClass="col-lg-2 control-label"> Teléfono Convencional</form:label>
                                        <div class="col-lg-2">
                                        <form:input path="convencional" cssClass="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="movil" cssClass="col-lg-2 control-label"> Teléfono Movil</form:label>
                                        <div class="col-lg-2">
                                        <form:input path="movil" cssClass="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="correo" cssClass="col-lg-2 control-label">Correo electrónico</form:label>
                                        <div class="col-lg-2">
                                        <form:input path="correo" cssClass="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="clave" cssClass="col-lg-2 control-label">Clave</form:label>
                                        <div class="col-lg-2">
                                        <form:input path="clave" cssClass="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ejemplo_email_3" class="col-lg-2 control-label"></label>
                                    <div class="col-lg-10">
                                        <button type="submit" class="btn btn-default"><span class="fa fa-save"></span> Guardar</button>
                                    </div>
                                </div>
                            </form:form>  
                        </div>
                    </div>
                    <!-- Divider -->
                    <div class="hr5" style="margin-top:60px; margin-bottom:50px;"></div>

                    <!--Start Clients Carousel-->
                    <!-- End Clients Carousel -->
                </div>
            </div>
            <!-- End Content -->


            <!-- Start Footer -->
            <footer>
                <div class="container">
                    <div class="row footer-widgets">
                        <!-- Start Contact Widget -->
                        <div class="col-md-4">
                            <div class="footer-widget contact-widget">
                                  <h4><img src="recursos/img/uta.png" class="img-responsive" alt="Footer Logo" width="50%%"/></h4>
                                                            <h3>Facultad de Ciencias Humanas y de la Educación</h3>
                                                                <p></p>
                                                                <ul>
                                                                    <li><span>Número:</span> 032 523039</li>
                                                                    <li><span>Email:</span>fche@uta.edu.ec</li>
                                                                    <li><span>Website:</span> http://www.uta.edu.ec/</li>
                                                                </ul>
                            </div>
                        </div>
                        <!-- .col-md-3 -->
                        <!-- End Contact Widget -->


                    </div>
                    <!-- .row -->

                    <!-- Start Copyright -->
                    <div class="copyright-section">
                        <div class="row">
                            <div class="col-md-6">
                                <p>UNIVERSIDAD TÉCNICA DE AMBATO</a> </p>
                            </div>
                        </div>
                    </div>
                    <!-- End Copyright -->

                </div>
            </footer>
            <!-- End Footer -->

        </div>
        <!-- End Container -->

        <!-- Go To Top Link -->
        <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

        <!-- Style Switcher -->

        <script type="text/javascript" src="js/script.js"></script>

    </body>

</html>