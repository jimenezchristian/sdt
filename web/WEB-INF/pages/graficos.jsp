
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Gráficos</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Margo - Responsive HTML5 Template">
        <meta name="author" content="iThemesLab">

        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/recursos/font-awesome/css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/style.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/responsive.css" media="screen">

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/animate.css" media="screen">

        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/red.css" title="red" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/jade.css" title="jade" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/blue.css" title="blue" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/beige.css" title="beige" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/cyan.css" title="cyan" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/green.css" title="green" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/orange.css" title="orange" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/peach.css" title="peach" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/pink.css" title="pink" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/purple.css" title="purple" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/sky-blue.css" title="sky-blue" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/yellow.css" title="yellow" media="screen" />
        
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <!-- Margo JS  -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.migrate.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/modernizrr.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.appear.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/count-to.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.textillate.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.lettering.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.parallax.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.slicknav.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css"/>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>                
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>                
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>

        <script type="text/javascript">
            google.load("visualization", "1", {packages: ["corechart"]});
            google.setOnLoadCallback(drawChart);

            function drawChart() {
            
            <c:if test="${existe == 1}">
                var data = google.visualization.arrayToDataTable([
                ['Periodos', 'Numeros'],
            <c:forEach items="${prueba}" var="dato">
                <c:if test="${dato.conclusion==0}">
                ['No graduados',${dato.graduacion}],
                </c:if>
                <c:if test="${dato.conclusion==1}">
                ['Graduados',${dato.graduacion}],
                </c:if>
            </c:forEach>

                ]);


            <c:forEach items="${datos}" var="dato">
                var options = {
                    title: "Carrera: " + '<c:out value="${dato.nombre}"/>' + "\n Periodo: " + '<c:out value="${periodo}"/>',
                    is3D: true

                };
            </c:forEach>

                
                var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                chart.draw(data, options);
                </c:if>
            }
        </script>

        <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body>

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->
            <div class="hidden-header"></div>
            <header class="clearfix">
                <!-- Start Header ( Logo & Naviagtion ) -->
                <div class="navbar navbar-default navbar-top">
                    <div class="container">
                        <div class="navbar-header">
                            <!-- Stat Toggle Nav Link For Mobiles -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                            <!-- End Toggle Nav Link For Mobiles -->
                            <!--<a class="navbar-brand" href="index.html"><img alt="" src="recursos/img/uta.png" width="20%"></a>-->
                        </div>
                        <div class="navbar-collapse collapse">
                            <!-- Start Navigation List -->
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a class="active" href="tutorhome.htm">Home</a>
                                </li>
                                <li>
                                    <a href="contactos.htm">Contactos</a>
                                </li>
                            </ul>
                            <!-- End Navigation List -->
                        </div>
                    </div>

                    <!-- Mobile Menu Start -->
                    <ul class="wpb-mobile-menu">
                        <li>
                            <a class="active" href="tutorhome.htm">Home</a>
                        </li>
                        <li>
                            <a href="contactos.htm">Contactos</a>
                        </li>
                    </ul>
                    <!-- Mobile Menu End -->

                </div>
                <!-- End Header ( Logo & Naviagtion ) -->

            </header>
            <!-- End Header -->

            <div class="hr5" style="margin-top:60px; margin-bottom:50px;"></div>
            <!-- Start Content -->
            <div id="content">
                <div class="container">

                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="text-center">Gráficos de Estudiantes No Graduados/Graduados</h1>                                                                                                                                             
                            <form:form  method="post" >

                                <select name="tipo" id="tipo"  >
                                    <c:forEach var="item" items="${tipoList}">

                                        <option value="${item.key}" >${item.value} </option>
                                    </c:forEach>
                                </select>
                                <select name="carreras" id="carreras"  >
                                    <c:forEach var="item" items="${carrerasList}">

                                        <option value="${item.key}" >${item.value} </option>
                                    </c:forEach>
                                </select>
                                <select name="periodo" id="periodos">
                                    <c:forEach var="item" items="${periodosList}">

                                        <option value="${item.key}" >${item.value} </option>
                                    </c:forEach>
                                </select>

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Buscar</button>
                                </span>
                            </form:form >

                            <br>
                            <c:if test="${existe ==1}">
                                <h3 class="text-center"> <c:out value="${titulo}"/></h3>
                                <c:if test="${nogra ==1}">
                                    <h4 class="text-center"> Lista de Estudiantes No Graduados</h4></br>
                                    <div class="col-lg-12 ">
                                    <table class="table " id="modelo" style="width:600px; margin: 0px auto;">
                                        <thead>
                                            <tr w>                            
                                                <th>Nombres</th>
                                                <th>Apellidos</th>
                                                <th>Cedula</th>                                                        
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${nograduados}" var="dato">

                                                <tr>                                
                                                    <td><c:out value="${dato.nombre}"></c:out>
                                                        </td >
                                                        <td><c:out value="${dato.apellido}"></c:out>
                                                        </td>
                                                        <td><c:out value="${dato.cedula}"></c:out>
                                                        </td>                                
                                                    </tr>

                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    </c:if>
                                    <c:if test="${gra ==1}">
                                        <br><h4 class="text-center"> Lista de Estudiantes Graduados</h4></br>
                                    <table class="table " id="modelo2" style="width:600px; margin: 0px auto;">
                                        <thead>
                                            <tr>                            
                                                <th>Nombres</th>
                                                <th>Apellidos</th>
                                                <th>Cedula</th>                                                        
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${graduados}" var="dato">

                                                <tr>                                
                                                    <td><c:out value="${dato.nombre}"></c:out>
                                                        </td>
                                                        <td><c:out value="${dato.apellido}"></c:out>
                                                        </td>
                                                        <td><c:out value="${dato.cedula}"></c:out>
                                                        </td>                                
                                                    </tr>

                                            </c:forEach>
                                        </tbody>
                                    </table>
                                        <br>
                                        <br>
                                    </c:if>
                                    </div> 
                            </div>
                                    <h3 class="text-center"> Gráfico Estadístico</h3>

                                    <div id="piechart" style="width: 620px; height: 500px;" class="center-block"></div>
                            </c:if>
                            <a href="<c:url value="secretariahome.htm?id=${id}"/>" class="btn btn-success">  Regresar</a>


                        </div>
                    </div>
                    <!-- Divider -->
                    <div class="hr5" style="margin-top:60px; margin-bottom:50px;"></div>

                    <!--Start Clients Carousel-->
                    <!-- End Clients Carousel -->
                </div>
            </div>
            <!-- End Content -->


            <!-- Start Footer -->
            <footer>
                <div class="container">
                    <div class="row footer-widgets">
                        <!-- Start Contact Widget -->
                        <div class="col-md-4">
                            <div class="footer-widget contact-widget">
                                <!--<h4><img src="recursos/img/uta.png" class="img-responsive" alt="Footer Logo" width="50%%"/></h4>-->
                                <!--                                <h3>UNIVERSIDAD TECNICA DE AMBATO</h3>
                                                                <p></p>
                                                                <ul>
                                                                    <li><span>Número:</span> 032 523039</li>
                                                                    <li><span>Email:</span> company@company.com</li>
                                                                    <li><span>Website:</span> http://www.uta.edu.ec/</li>
                                                                </ul>-->
                            </div>
                        </div>
                        <!-- .col-md-3 -->
                        <!-- End Contact Widget -->


                    </div>
                    <!-- .row -->

                    <!-- Start Copyright -->
                    <div class="copyright-section">
                        <div class="row">
                            <div class="col-md-6">
                                <p>UTA</a> </p>
                            </div>
                        </div>
                    </div>
                    <!-- End Copyright -->

                </div>
            </footer>
            <!-- End Footer -->

        </div>
        <!-- End Container -->

        <!-- Go To Top Link -->
        <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

        <!-- Style Switcher -->

        <script type="text/javascript" src="recursos/js/script.js"></script>
        <div class="switcher-box">
            <a class="open-switcher show-switcher"><i class="fa fa-cog fa-2x"></i></a>
            <h4>Estilos</h4>
            <span>Colores</span>
            <ul class="colors-list">
                <li>
                    <a onClick="setActiveStyleSheet('blue'); return false;" title="Blue" class="blue"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('sky-blue');
                            return false;" title="Sky Blue" class="sky-blue"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('cyan'); return false;" title="Cyan" class="cyan"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('jade'); return false;" title="Jade" class="jade"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('green'); return false;" title="Green" class="green"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('purple'); return false;" title="Purple" class="purple"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('pink'); return false;" title="Pink" class="pink"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('red'); return false;" title="Red" class="red"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('orange'); return false;" title="Orange" class="orange"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('yellow'); return false;" title="Yellow" class="yellow"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('peach'); return false;" title="Peach" class="peach"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('beige'); return false;" title="Biege" class="beige"></a>
                </li>
            </ul>
        </div>
    </body>    
<script>
    $(document).ready(function() {
    $('#modelo').DataTable({
        "language": {
            "lengthMenu": "Presentar _MENU_ Estudiantes por página",
            "search":"Buscar Estudiante",
            "info":  'Total de Estudiantes No Graduados: _TOTAL_',
            "paginate": {
                "first":      "Primera",
                "last":       "Última",
                "next":       "Siguiente",
                "previous":   "Anterior"
            }
        }
    } );
    $('#modelo2').DataTable({
        "language": {
            "lengthMenu": "Presentar _MENU_ Estudiantes por página",
            "search":"Buscar Estudiante",
            "info":  'Total de Estudiantes Graduados: _TOTAL_',
            "paginate": {
                "first":      "Primera",
                "last":       "Última",
                "next":       "Siguiente",
                "previous":   "Anterior"
            }
        }
    } );
    } );
</script>
</html>