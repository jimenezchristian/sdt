
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Estudiantes Tesis</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Margo - Responsive HTML5 Template">
        <meta name="author" content="iThemesLab">

        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/recursos/asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/recursos/font-awesome/css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/style.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/responsive.css" media="screen">

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/animate.css" media="screen">

        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/red.css" title="red" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/jade.css" title="jade" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/blue.css" title="blue" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/beige.css" title="beige" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/cyan.css" title="cyan" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/green.css" title="green" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/orange.css" title="orange" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/peach.css" title="peach" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/pink.css" title="pink" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/purple.css" title="purple" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/sky-blue.css" title="sky-blue" media="screen" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/colors/yellow.css" title="yellow" media="screen" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <!-- Margo JS  -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.migrate.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/modernizrr.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.appear.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/count-to.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.textillate.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.lettering.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.parallax.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/recursos/js/jquery.slicknav.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>




        <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body>

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->
            <div class="hidden-header"></div>
            <header class="clearfix">
                <!-- Start Header ( Logo & Naviagtion ) -->
                <div class="navbar navbar-default navbar-top">
                    <div class="container">
                        <div class="navbar-header">
                            <!-- Stat Toggle Nav Link For Mobiles -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                            <!-- End Toggle Nav Link For Mobiles -->
                            <!--<a class="navbar-brand" href="index.html"><img alt="" src="recursos/img/uta.png" width="20%"></a>-->
                        </div>
                        <div class="navbar-collapse collapse">
                            <!-- Start Navigation List -->
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a class="active" href="tutorhome.htm?id=${id}">Home</a>
                                </li>
                                <li>
                                    <a href="contactos.htm">Contactos</a>
                                </li>
                            </ul>
                            <!-- End Navigation List -->
                        </div>
                    </div>

                    <!-- Mobile Menu Start -->
                    <ul class="wpb-mobile-menu">
                        <li>
                            <a class="active" href="tutorhome.htm?id=${id}">Home</a>
                        </li>
                        <li>
                            <a href="contactos.htm">Contactos</a>
                        </li>
                    </ul>
                    <!-- Mobile Menu End -->

                </div>
                <!-- End Header ( Logo & Naviagtion ) -->

            </header>
            <!-- End Header -->

            <div class="hr5" style="margin-top:60px; margin-bottom:50px;"></div>
            <!-- Start Content -->
            <div id="content">
                <div class="container">

                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="text-center">Estudiantes Matriculados con Proyectos de Investigación</h1> 
                            <c:if test="${existe==1}">
                                <!--<a href="<c:url value="reportePDF.htm?id=${id}"/>" class="text-right">  <p>Descargar Reporte en PDF</p></a>-->
                                <a href="<c:url value="reportepdftitulacion.htm?id=${id}"/>" class="text-right">  <p>Descargar Reporte en PDF</p></a>
                                <a href="<c:url value="reporteTotalFases.htm?id=${id}"/>" class="text-right">  <p>Descargar Reporte Total Fases</p></a>
                            </c:if>                                                                                    

                            <form:form  method="post" >
                                <select   id="opciones" name="opcion">
                                    <c:forEach var="item" items="${opcionesList}">

                                        <option value="${item.key}" >${item.value} </option>
                                    </c:forEach>
                                </select>
                                </br> 

                                </br>
                                <select name="periodo" id="periodos" style="display: none;" >
                                    <c:forEach var="itemp" items="${periodosList}">

                                        <option value="${itemp.key}" >${itemp.value} </option>
                                    </c:forEach>
                                </select>

                                <!--listar       periodosTituList-->
                                <select name="periodos" id="periodostitulacion" style="display: none;" >
                                    <c:forEach var="item" items="${periodosTituList}">

                                        <option value="${item.key}" > ${item.value} </option>
                                    </c:forEach>

                                    <c:if test="${rol == 2}">
                                        <select name="carreras" id="carreras" style="display: none;" >
                                            <c:forEach var="item" items="${carrerasList}">

                                                <option value="${item.key}" >${item.value} </option>
                                            </c:forEach>
                                        </select>
                                    </c:if>

                                </select>


                                <select name="fases" id="fases" style="display: none;" >
                                    <c:forEach var="item" items="${fasesList}">

                                        <option value="${item.fase}" > Fase ${item.fase} </option>
                                    </c:forEach>
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Buscar</button>
                                </span>
                            </form:form >

                            <br>
                            <div id="tabla">
                                <table class="table table-bordered table-stiped table-hover" id="modelo">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Nombres</th>
                                            <th>Apellidos</th>
                                            <th>Cedula</th>
                                            <th>Filtrado: <c:out value="${filtrado}"></c:out></th>
                                                <th>Temas de Tesis</th>

                                            </tr>
                                        </thead>
                                        <tbody>

                                        <c:forEach items="${datos}" var="dato">



                                            <tr>
                                                <td><c:out value="${dato.id}"></c:out>
                                                    </td>
                                                    <td><c:out value="${dato.nombre}"></c:out>
                                                    </td>
                                                    <td><c:out value="${dato.apellido}"></c:out>
                                                    </td>
                                                    <td><c:out value="${dato.cedula}"></c:out>
                                                    </td>
                                                    <td><c:out value="${dato.opcional}"></c:out>
                                                    </td>
                                                    <td>
                                                    <c:out value="${dato.tema}"></c:out>
                                                    </td>

                                                </tr>

                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <c:if test="${rol==1}">
                                <a href="<c:url value="tutorhome.htm?id=${id}"/>" class="btn btn-success">  Regresar</a>
                            </c:if>            
                            <c:if test="${rol==2}">
                                <a href="<c:url value="secretariahome.htm?id=${id}"/>" class="btn btn-success">  Regresar</a>
                            </c:if>


                        </div>
                    </div>
                    <!-- Divider -->
                    <div class="hr5" style="margin-top:60px; margin-bottom:50px;"></div>

                    <!--Start Clients Carousel-->
                    <!-- End Clients Carousel -->
                </div>
            </div>
            <!-- End Content -->


            <!-- Start Footer -->
            <footer>
                <div class="container">
                    <div class="row footer-widgets">
                        <!-- Start Contact Widget -->
                        <div class="col-md-4">
                            <div class="footer-widget contact-widget">
                                <h4><img src="recursos/img/uta.png" class="img-responsive" alt="Footer Logo" width="50%%"/></h4>
                                <h3>Facultad de Ciencias Humanas y de la Educación</h3>
                                <p></p>
                                <ul>
                                    <li><span>Número:</span> 032 523039</li>
                                    <li><span>Email:</span>fche@uta.edu.ec</li>
                                    <li><span>Website:</span> http://www.uta.edu.ec/</li>
                                </ul>
                            </div>
                        </div>
                        <!-- .col-md-3 -->
                        <!-- End Contact Widget -->


                    </div>
                    <!-- .row -->

                    <!-- Start Copyright -->
                    <div class="copyright-section">
                        <div class="row">
                            <div class="col-md-6">
                                <p>UNIVERSIDAD TÉCNICA DE AMBATO</a> </p>
                            </div>
                        </div>
                    </div>
                    <!-- End Copyright -->

                </div>
            </footer>
            <!-- End Footer -->

        </div>
        <!-- End Container -->

        <!-- Go To Top Link -->
        <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

        <!-- Style Switcher -->

        <script type="text/javascript" src="recursos/js/script.js"></script>
        <div class="switcher-box">
            <a class="open-switcher show-switcher"><i class="fa fa-cog fa-2x"></i></a>
            <h4>Estilos</h4>
            <span>Colores</span>
            <ul class="colors-list">
                <li>
                    <a onClick="setActiveStyleSheet('blue'); return false;" title="Blue" class="blue"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('sky-blue'); return false;" title="Sky Blue" class="sky-blue"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('cyan'); return false;" title="Cyan" class="cyan"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('jade'); return false;" title="Jade" class="jade"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('green'); return false;" title="Green" class="green"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('purple'); return false;" title="Purple" class="purple"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('pink'); return false;" title="Pink" class="pink"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('red'); return false;" title="Red" class="red"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('orange'); return false;" title="Orange" class="orange"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('yellow'); return false;" title="Yellow" class="yellow"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('peach'); return false;" title="Peach" class="peach"></a>
                </li>
                <li>
                    <a onClick="setActiveStyleSheet('beige'); return false;" title="Biege" class="beige"></a>
                </li>
            </ul>
        </div>
    </body>
    <script>
        $(document).ready(function () {
            $('#modelo').DataTable();
            $("#opciones").change(function () {
                var valor = $(this).val();
                // alert("este es el valor "+ valor);
                if (valor === "1") {
                    $('#periodos').hide();
                    $('#carreras').show();
                    $('#fases').hide();
                } else {
                    if (valor === "2" || valor === "4") {
                        $('#carreras').hide();
                        $('#periodos').show();
                        $('#fases').hide();
                    } else {
                        if (valor === "5") {
                            $('#periodos').hide();
                            $('#periodos').show();
                            $('#fases').show();
                            $('#periodostitulacion').hide();
                        } else {
                            if (valor === "3") {
                                $('#carreras').hide();
                                $('#periodos').hide();
                                $('#periodostitulacion').show();
                                $('#fases').hide();
                            } else {
                                $('#carreras').hide();
                                $('#periodos').hide();
                                $('#fases').hide();
                            }
                        }
                    }
                }
            });
        });


    </script>


</html>
