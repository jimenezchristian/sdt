/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : bd_sdt

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-12-15 00:45:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `catalogo`
-- ----------------------------
DROP TABLE IF EXISTS `catalogo`;
CREATE TABLE `catalogo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `codigo` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of catalogo
-- ----------------------------
INSERT INTO `catalogo` VALUES ('1', 'Carreras', 'Carrera posibles de la Universidad', 'CARRERA');
INSERT INTO `catalogo` VALUES ('2', 'Periodos', 'Peridos de La universidad', 'PERIODO');
INSERT INTO `catalogo` VALUES ('3', 'Modalidad', 'Modalidades que existen en la facultad', 'MODAL');
INSERT INTO `catalogo` VALUES ('4', 'Nivel de Estudio', 'Ciclo en que cursa el estudiante', 'NIVESTU');

-- ----------------------------
-- Table structure for `estudiante`
-- ----------------------------
DROP TABLE IF EXISTS `estudiante`;
CREATE TABLE `estudiante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) DEFAULT NULL,
  `apellido` varchar(30) DEFAULT NULL,
  `cedula` int(10) DEFAULT NULL,
  `direccion` text,
  `carrera` int(11) DEFAULT NULL,
  `modalidad` int(11) DEFAULT NULL,
  `nivelestudios` int(11) DEFAULT NULL,
  `convencional` varchar(30) DEFAULT NULL,
  `movil` varchar(30) DEFAULT NULL,
  `correo` varchar(30) DEFAULT NULL,
  `clave` varchar(20) DEFAULT NULL,
  `rol` int(11) NOT NULL DEFAULT '4',
  `tutor` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cedula_UNIQUE` (`cedula`),
  KEY `fk_sdt_infpersonalestudiante_std_rol1_idx` (`rol`),
  KEY `fk_estudiante_tutor1_idx` (`tutor`),
  CONSTRAINT `fk_estudiante_tutor1` FOREIGN KEY (`tutor`) REFERENCES `tutor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sdt_infpersonalestudiante_std_rol1` FOREIGN KEY (`rol`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estudiante
-- ----------------------------
INSERT INTO `estudiante` VALUES ('9', 'Cando Tibanquiza', 'Patricia Soledad', '1804715512', 'Pelileo', null, null, null, '\r\n', '980934836', 'solecando@hotmail.com', '1804715512', '4', '1');
INSERT INTO `estudiante` VALUES ('10', 'Carrasco Llerena', 'Mayra Estefania\r\n', '1804641486', 'Ambato- Gertrudis Esparza', null, null, null, '2415662', '987433383', 'mcarrasco1486@uta.edu.ec', '1804641486', '4', '1');
INSERT INTO `estudiante` VALUES ('11', 'Castro Guerrero \r\n', 'Adela Mirella\r\n', '1804745287', 'Picaihua', null, null, null, '2762231', '992760123', 'actualizadoadelacastro2009@gma', '1804745287', '4', '1');
INSERT INTO `estudiante` VALUES ('12', 'Castro Padilla \r\n', 'Fidel Francisco\r\n', '1804642807', 'Parr. Juan Benigno Vela', null, null, null, '\r\n', '933332446', 'fidel.castro_p@hotmail.com', '1804642807', '4', '1');
INSERT INTO `estudiante` VALUES ('13', 'Chango Chimborazo', 'Kleber Ricardo', '1804351854', 'Leon Cevallos Y Letamendi', null, null, null, '2848467\r\n', '992610834', 'rikrdo45ch@gmail.com', '1804351854', '4', '1');
INSERT INTO `estudiante` VALUES ('14', 'Chicaiza Sambonino', 'Lorena Marisol', '1803886124', null, null, null, null, '\r\n', '', '', '1803886124', '4', '1');
INSERT INTO `estudiante` VALUES ('15', 'Chicaiza Lema ', 'Marco Rosendo', '502675713', 'Urbanización Amores Salgado', null, null, null, '\r\n', '987508581', 'camilochicaiza77@gmail.com', '502675713', '4', '1');
INSERT INTO `estudiante` VALUES ('16', 'Chifla Andaluz', 'Wagner Patricio', '1804726022', 'Pelileo', null, null, null, '2830941', '986811920', 'wagnerchifla@gmail.com', '1804726022', '4', '1');
INSERT INTO `estudiante` VALUES ('17', 'Chipantiza Masabanda', 'Deisy Marianela', '1804357919', 'Patate Av. Via San Jorge barrio Tahuaicha ', null, null, null, '2870742', '990592251', 'deysi.m90@gmail.com', '1804357919', '4', '1');
INSERT INTO `estudiante` VALUES ('18', 'Cruz Llundo ', 'Marcela Maribel', '1803995941', 'Atocha', null, null, null, '2426016', '984730974', 'fido.didor@hotmail.com', '1803995941', '4', '1');
INSERT INTO `estudiante` VALUES ('19', 'Cuyo Chugchilan ', 'Maria Yolanda', '1803951860', 'Huachi Grande', null, null, null, '', '99874367', 'yolandaortiz.1989@gmail.com', '1803951860', '4', '1');
INSERT INTO `estudiante` VALUES ('20', 'Fraga Vinueza ', 'Mariela Elizabeth', '1718093956', 'Panamericana Sur 30  El Obelisco', null, null, null, '', '997800929', 'mary19el@yahoo.es', '1718093956', '4', '1');
INSERT INTO `estudiante` VALUES ('21', 'Freire Molina ', 'Adriana Fernanda', '1804770137', 'La Pradera -Ambato', null, null, null, '2417469', '999831503', 'adri.freire784@gmail.com', '1804770137', '4', '1');
INSERT INTO `estudiante` VALUES ('22', 'Frias Córdova', 'Paola Elizabeth', '1804280160', 'Parroquia el Rosario - Pelileo ', null, null, null, '', '998131055', 'paolafrias75@yahoo.es', '1804280160', '4', '1');
INSERT INTO `estudiante` VALUES ('23', 'Gavilanes Pilamunga ', 'Dario Javier', '1804792743', 'Santa Rosa', null, null, null, '', '995386717', 'javo.gavilanes106@gmail.com', '1804792743', '4', '1');
INSERT INTO `estudiante` VALUES ('24', 'Guamán Ramírez ', 'Evelin Lizbeth', '1805089800', 'Via A Guaranda Km 2 1/2', null, null, null, '', '999779535', 'evelingr@gmail.com', '1805089800', '4', '1');
INSERT INTO `estudiante` VALUES ('25', 'Guevara Rojas ', 'Flavio José', '1600651309', null, null, null, null, '', '', '', '1600651309', '4', '1');
INSERT INTO `estudiante` VALUES ('26', 'Hidalgo Pazmiño', 'Byron Renato', '1804433702', 'Tres Carabelas Y Psj Babahoyo', null, null, null, '2415169', '995415408', 'renasmyta@gmail.com', '1804433702', '4', '1');
INSERT INTO `estudiante` VALUES ('27', 'Jácome Mena ', 'Jaime Javier', '1804585667', 'Narváez Y Sucre', null, null, null, '32873738', '984530506', 'javo_1965@yahoo.es', '1804585667', '4', '1');
INSERT INTO `estudiante` VALUES ('28', 'Lara Alvarez ', 'Jenny Estefania', '1804276937', 'Huachi Chico', null, null, null, '32587536', '958935700', 'tefalara@hotmail.com', '1804276937', '4', '1');
INSERT INTO `estudiante` VALUES ('29', 'Laura Telenchana ', 'Violeta Macarena', '1804389748', 'Paso Lateral Barrio San Jose', null, null, null, '2436616', '969553830', 'viovidita38@gmail.com', '1804389748', '4', '1');
INSERT INTO `estudiante` VALUES ('30', 'Lescano Sánchez ', 'Fabricio Misael', '1804644134', 'Av Galo Vela Y Platón', null, null, null, '32762306', '987744404', 'fabriciolescano89@hotmail.com', '1804644134', '4', '1');
INSERT INTO `estudiante` VALUES ('31', 'Lescano Feijoo', 'Esther Catalina', '1804614178', 'Huachi Chico', null, null, null, '2758251', '999034023', 'kata.lescano@hotmail.com', '1804614178', '4', '1');
INSERT INTO `estudiante` VALUES ('32', 'Llivicura Guevara ', 'Cristina Maribel', '1804252896', 'Huachi Chico', null, null, null, '', '983776906', 'krismabel1992@gmail.com', '1804252896', '4', '1');
INSERT INTO `estudiante` VALUES ('33', 'López Flores ', 'Gabriela Estefania', '1803450186', 'Simón Bolívar- Cacique Álvarez  y Av. Los Chasquis ', null, null, null, '32844627', '958814416', 'gabelly1@hotmail.es', '1803450186', '4', '1');
INSERT INTO `estudiante` VALUES ('34', 'Manobanda Chango ', 'Elisa Mariela', '1804007118', 'Parque Industrial Barrio Divino Niño', null, null, null, '', '992641298', 'elisa28101@gmail.com', '1804007118', '4', '1');
INSERT INTO `estudiante` VALUES ('35', 'Manzano Acosta ', 'Teresa De Lourdes', '1804765913', 'Pichincha Alta Y Jose Mires', null, null, null, '32401243', '998841046', 'lulymanzanoacosta@hotmail.es', '1804765913', '4', '1');
INSERT INTO `estudiante` VALUES ('36', 'Martínez Pérez ', 'Sonia Gabriela', '1804675641', 'Patate San Jorge', null, null, null, '33063206', '0998832572 - 0987195575', 'sonymar_1987@hotmail.com', '1804675641', '4', '1');
INSERT INTO `estudiante` VALUES ('37', 'Montenegro Padilla ', 'Soraida Jacqueline', '1720091980', 'Parroquia Checa- Barrio la Delicia  calle Felipe Báez y Samuel ', null, null, null, '22300035', '996057474', 'babyblak_30_86@hotmail.com', '1720091980', '4', '1');
INSERT INTO `estudiante` VALUES ('38', 'Moya Pilco', 'Tatiana Maribel', '1804796686', 'Pillaro - El Rosario San Miguelito', null, null, null, '', '991128466', 'tatymoya_leo@hotmail.com', '1804796686', '4', '1');
INSERT INTO `estudiante` VALUES ('39', 'Naranjo Maliza', 'Alexandra Paulina', '1803005220', null, null, null, null, '', '', '', '1803005220', '4', '1');
INSERT INTO `estudiante` VALUES ('40', 'Ortíz López ', 'Delia Maria', '1804150587', 'La Península', null, null, null, '2445206', '992208334', 'sofy89karo@hotmail.com', '1804150587', '4', '1');
INSERT INTO `estudiante` VALUES ('41', 'Ortiz Robles ', 'Lenin Sebastian', '1804096608', 'Augusto Martínez', null, null, null, '', '995193050', 'lenyseb10@gmail.com', '1804096608', '4', '1');
INSERT INTO `estudiante` VALUES ('42', 'Padilla Chiluisa ', 'Marco Rodrigo', '503697047', 'Latacunga - Alaquez', null, null, null, '2262376', '984534477', 'mark8927@hotmail.com', '503697047', '4', '1');
INSERT INTO `estudiante` VALUES ('43', 'Palate Guamán ', 'Byro Danilo', '1804921003', 'Picaihua - San Juan Vía Las Viñas', null, null, null, '', '984992894', 'danilopalate@gmail.com', '1804921003', '4', '1');
INSERT INTO `estudiante` VALUES ('44', 'Parra Guamán ', 'Catherine Liseth', '1721519773', 'Nueva Ambato', null, null, null, '', '995451628', 'cate.liss.9@gmail.com', '1721519773', '4', '1');
INSERT INTO `estudiante` VALUES ('45', 'Peñaranda  Pérez ', 'Christian Alejandro', '1804756771', 'Huachi Grande', null, null, null, '2440775', '987409170', 'cristian2012dj@gmail.com', '1804756771', '4', '1');
INSERT INTO `estudiante` VALUES ('46', 'Pérez Ortíz ', 'Johanna Vanesa', '1803730934', 'Ambato', null, null, null, '2586839', '988626036', 'vanesa9284@hotmail', '1803730934', '4', '1');
INSERT INTO `estudiante` VALUES ('47', 'Pintado Guamaní ', 'Jennifer Pamela', '1804410908', 'Huachi Chico', null, null, null, '', '987899692', 'edupi@gmail.com', '1804410908', '4', '1');
INSERT INTO `estudiante` VALUES ('48', 'Proaño Calvache ', 'Mirian Del Rocio', '1802204659', 'Antonio Clavijo E Ibañez Casa 10', null, null, null, '2588149', '991073047', 'mirian.proao@gmail.com', '1802204659', '4', '1');
INSERT INTO `estudiante` VALUES ('49', 'Quishpe Amagua', 'Jose Ramiro', '1704440682', 'Quito- Parroquia Santa Prisca calle Destacamento Las Casas oe4-116 y Carvajal', null, null, null, '2521543', '983749636', 'rami1954@hotmail.es', '1704440682', '4', '1');
INSERT INTO `estudiante` VALUES ('50', 'Remache Guerrero', 'Cintya Gabriela', '1804620423', 'Caserío Pilco', null, null, null, '2779483', '981081788', 'gabyremache18@gmail.com', '1804620423', '4', '1');
INSERT INTO `estudiante` VALUES ('51', 'Rodríguez Moreno ', 'Edisson Fabian', '1804722781', 'Alonso Palcios Y Via Condesan', null, null, null, '2772404', '987903851', 'edisonr15@hotmail.com', '1804722781', '4', '1');
INSERT INTO `estudiante` VALUES ('52', 'Rosero Ojeda ', 'Teresa Del Rocio', '1804603676', 'Quero', null, null, null, '2746037', '982377269', 'teresitarosero1990@hotmail.es', '1804603676', '4', '1');
INSERT INTO `estudiante` VALUES ('53', 'Sánchez Hurtado ', 'Lorena Marisol', '1804417945', 'Tres Carabelas Y Psj Babahoyo', null, null, null, '2763282', '998656626', 'loresanchez19@gmail.com', '1804417945', '4', '1');
INSERT INTO `estudiante` VALUES ('54', 'Sánchez Alvarez ', 'Viviana De Los Angeles', '1804251328', 'Av. Quiz Quiz', null, null, null, '2844046', '998647789', 'vivialv_91vc@hotmail.com', '1804251328', '4', '1');
INSERT INTO `estudiante` VALUES ('55', 'Santamaría López ', 'Karina Alexandra', '1804449872', 'Ambato', null, null, null, '2852898', '987939125', 'alexiita_uta@hotmail.com', '1804449872', '4', '1');
INSERT INTO `estudiante` VALUES ('56', 'Sulca Suque ', 'Mayra Lizbeth', '1804436242', 'Cevallos - Barrio La Union', null, null, null, '2872741', '983499196', 'mayrasulca06@gmail.com', '1804436242', '4', '1');
INSERT INTO `estudiante` VALUES ('57', 'Sunta Mariño ', 'Mónica Paola', '1804072369', 'Antonio Clavijo Urb. Eceheverría', null, null, null, '', '992728881', 'paito.1993@hotmail.com', '1804072369', '4', '1');
INSERT INTO `estudiante` VALUES ('58', 'Tamayo Alvarez ', 'Luis Edison', '1717306227', 'Parroquia Tumbaco El Arenal Interoceanica Y Jose Correa E8-73', null, null, null, '22044367', '987505651', 'luis_8725@hotmail.com', '1717306227', '4', '1');
INSERT INTO `estudiante` VALUES ('59', 'Tene Guamán ', 'Lorena Elizabeth', '1804932786', 'Pelileo- Parroquia Humbalo', null, null, null, '', '994263082', 'elylorenita1994@gmail.com', '1804932786', '4', '1');
INSERT INTO `estudiante` VALUES ('60', 'Tirado Guato ', 'Silvia Paola', '1804698668', 'Pelileo Caserio Chambiato', null, null, null, '', '998670399', 'silviapaotg17@hotmail.com', '1804698668', '4', '1');
INSERT INTO `estudiante` VALUES ('61', 'Toasa Yachimba ', 'Julio Miguel', '1804865747', 'Pillaro', null, null, null, '32874880', '997012120', 'migueltoasayachimba@gmail.com', '1804865747', '4', '1');
INSERT INTO `estudiante` VALUES ('62', 'Torres Padilla ', 'Johana Nataly', '1804432761', 'Pinllo', null, null, null, '2466230', '983239194', 'naty_johas@hotmail.com', '1804432761', '4', '1');
INSERT INTO `estudiante` VALUES ('63', 'Torrez Amaguaña ', 'Merci Carolina', '1804168134', 'Picaihua - Barrio Jerusalem', null, null, null, '32762281', '995460217', 'carito2020111@hotmail.com', '1804168134', '4', '1');
INSERT INTO `estudiante` VALUES ('64', 'Villacis Freire ', 'Jazmina Ivnne', '1804468856', 'Cevallos Barrio La Floresta', null, null, null, '32580495', '995868032', 'jazmin.villacis93@gmail.com', '1804468856', '4', '1');
INSERT INTO `estudiante` VALUES ('65', 'Villacis Salazar ', 'Alba Victoria', '1804223905', 'Esnest Alvarado Y Augusto Arias', null, null, null, '2848806', '979294592', 'vickyta20@live.com', '1804223905', '4', '1');
INSERT INTO `estudiante` VALUES ('66', 'Yanchapaxi Oña ', 'Klever Germanico', '503600942', 'Rio Payamino Y Los Atis', null, null, null, '', '95929656', 'klevernick93@gmail.com', '503600942', '4', '1');
INSERT INTO `estudiante` VALUES ('67', 'Zambrano Pujos ', 'Yasmina', '1804453445', 'Huachi La Magdalena', null, null, null, '32586961', '984376979', 'yaz-chivita10@hotmail.com', '1804453445', '4', '1');
INSERT INTO `estudiante` VALUES ('68', 'Villagómez Arroba ', 'Patricia Alexandra', '1804483244', 'Santa Rosa', null, null, null, '32406324', '995423315', 'patygatita1993@gmail.com', '1804483244', '4', '1');
INSERT INTO `estudiante` VALUES ('69', 'Sánchez Jaya ', 'Talia Beatriz', '1803798204', 'Cevallos Barrio La Joya', null, null, null, '2406986', '995711838', 'talice21112012@gmail.com', '1803798204', '4', '1');
INSERT INTO `estudiante` VALUES ('70', 'Chisag Punina ', 'Gladys Mariela', '1804704987', 'Barrio El Mirador', null, null, null, '', '995782597', 'marily-27051991@hotmail.com', '1804704987', '4', '1');
INSERT INTO `estudiante` VALUES ('71', 'Lara Cuenca ', 'Ana Patricia', '1709544645', 'Tambillo Calle García  Moreno y Autopista Casa 150', null, null, null, '22318192', '995968330', 'anypac76@hotmail.com', '1709544645', '4', '1');

-- ----------------------------
-- Table structure for `examencomplexivo`
-- ----------------------------
DROP TABLE IF EXISTS `examencomplexivo`;
CREATE TABLE `examencomplexivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `periodounidadtitulacion` int(11) DEFAULT NULL,
  `periodocapacitacion` int(11) DEFAULT NULL,
  `fechaexamencomplexivo` date DEFAULT NULL,
  `fechainvestidura` date DEFAULT NULL,
  `estudiante` int(11) NOT NULL,
  `secretaria` int(11) NOT NULL,
  `tutor` int(11) NOT NULL,
  `procesoproyectos` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_examencomplexivo_sdt_infpersonalestudiante1_idx` (`estudiante`),
  KEY `fk_examencomplexivo_sdt_infsecretaria1_idx` (`secretaria`),
  KEY `fk_examencomplexivo_sdt_infpersonaltutor1_idx` (`tutor`),
  KEY `fk_examencomplexivo_sdt_procesoproyectos1_idx` (`procesoproyectos`),
  CONSTRAINT `fk_examencomplexivo_sdt_infpersonalestudiante1` FOREIGN KEY (`estudiante`) REFERENCES `estudiante` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_examencomplexivo_sdt_infpersonaltutor1` FOREIGN KEY (`tutor`) REFERENCES `tutor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_examencomplexivo_sdt_infsecretaria1` FOREIGN KEY (`secretaria`) REFERENCES `secretaria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_examencomplexivo_sdt_procesoproyectos1` FOREIGN KEY (`procesoproyectos`) REFERENCES `procesoproyectos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of examencomplexivo
-- ----------------------------

-- ----------------------------
-- Table structure for `items`
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `catalogo` int(11) NOT NULL,
  `activo` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_items_catalogo1_idx` (`catalogo`),
  CONSTRAINT `fk_items_catalogo1` FOREIGN KEY (`catalogo`) REFERENCES `catalogo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES ('1', 'Educacion Parvularia', '1', '1');
INSERT INTO `items` VALUES ('2', 'Educación Inicial', '1', '1');
INSERT INTO `items` VALUES ('3', 'Abril - Septiembre 2015', '2', '1');
INSERT INTO `items` VALUES ('4', 'Octubre 2015 - Marzo 2016', '2', '1');
INSERT INTO `items` VALUES ('5', 'Presencial', '3', '1');
INSERT INTO `items` VALUES ('6', 'SemiPresencial', '3', '1');
INSERT INTO `items` VALUES ('7', 'Noveno', '4', '1');
INSERT INTO `items` VALUES ('8', 'Décimo', '4', '1');
INSERT INTO `items` VALUES ('9', 'Actualización de Conocimientos', '4', '1');
INSERT INTO `items` VALUES ('10', 'Estudiantes con créditos extra curriculares pendientes', '4', '1');
INSERT INTO `items` VALUES ('13', 'Educación Básica\r\nEducación Básica', '1', '1');
INSERT INTO `items` VALUES ('14', 'Turismo y Hoteleria', '1', '1');
INSERT INTO `items` VALUES ('15', 'Cultura Física', '1', '1');
INSERT INTO `items` VALUES ('16', 'Pedagogía de la Actividad Física y deporte', '1', '1');
INSERT INTO `items` VALUES ('17', 'Docencia en Informática', '1', '1');
INSERT INTO `items` VALUES ('18', 'Psicologia Educativa', '1', '1');
INSERT INTO `items` VALUES ('19', 'Psicología Insdustrial', '1', '1');
INSERT INTO `items` VALUES ('20', 'Psicopedagogía', '1', '1');
INSERT INTO `items` VALUES ('21', 'Idiomas', '1', '1');
INSERT INTO `items` VALUES ('22', 'Pedagogía de los Idiomas Nacionales Extranjeros', '1', '1');
INSERT INTO `items` VALUES ('23', 'Abril - Septiembre 2016', '2', '1');
INSERT INTO `items` VALUES ('24', 'Obtubre 2016 -Marzo 2017', '2', '1');

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `icono` varchar(30) DEFAULT NULL,
  `menu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sdt_menu_sdt_menu1_idx` (`menu`),
  CONSTRAINT `fk_sdt_menu_sdt_menu1` FOREIGN KEY (`menu`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '', 'Estudiantes', 'fa fa-users', null);
INSERT INTO `menu` VALUES ('2', '', 'Formato Resoluciones', 'fa fa-archive', null);
INSERT INTO `menu` VALUES ('3', '', 'Procesos', 'fa fa-tasks', null);
INSERT INTO `menu` VALUES ('4', '', 'Ver Proceso', 'fa fa-retweet', null);
INSERT INTO `menu` VALUES ('5', '', 'Formularios', 'fa fa-address-book', null);
INSERT INTO `menu` VALUES ('6', '', 'Examen Complexivo', 'fa fa-book', null);
INSERT INTO `menu` VALUES ('7', 'estudiantenew.htm', 'Formulario', 'fa fa-address-book', '6');
INSERT INTO `menu` VALUES ('8', '', 'Proceso', 'fa fa-tasks', '6');
INSERT INTO `menu` VALUES ('9', '', 'Trabajo Titulación', 'fa fa-university', null);
INSERT INTO `menu` VALUES ('10', '', 'Formulario Tema', 'fa fa-address-book-o', '9');
INSERT INTO `menu` VALUES ('11', '', 'Visualizar Proceso', 'fa fa-tasks', '9');
INSERT INTO `menu` VALUES ('12', '', 'Periodos', 'fa fa-graduation-cap', '9');

-- ----------------------------
-- Table structure for `menurol`
-- ----------------------------
DROP TABLE IF EXISTS `menurol`;
CREATE TABLE `menurol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` int(11) NOT NULL,
  `rol` int(11) NOT NULL,
  PRIMARY KEY (`id`,`menu`,`rol`),
  KEY `fk_sdt_menu_has_std_rol_std_rol1_idx` (`rol`),
  KEY `fk_sdt_menu_has_std_rol_sdt_menu1_idx` (`menu`),
  CONSTRAINT `fk_sdt_menu_has_std_rol_sdt_menu1` FOREIGN KEY (`menu`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sdt_menu_has_std_rol_std_rol1` FOREIGN KEY (`rol`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menurol
-- ----------------------------
INSERT INTO `menurol` VALUES ('1', '1', '3');
INSERT INTO `menurol` VALUES ('2', '2', '3');
INSERT INTO `menurol` VALUES ('3', '3', '3');
INSERT INTO `menurol` VALUES ('4', '4', '4');
INSERT INTO `menurol` VALUES ('5', '5', '4');
INSERT INTO `menurol` VALUES ('6', '6', '2');
INSERT INTO `menurol` VALUES ('7', '7', '2');
INSERT INTO `menurol` VALUES ('8', '8', '2');
INSERT INTO `menurol` VALUES ('9', '9', '2');
INSERT INTO `menurol` VALUES ('10', '10', '2');
INSERT INTO `menurol` VALUES ('11', '11', '2');
INSERT INTO `menurol` VALUES ('12', '12', '2');

-- ----------------------------
-- Table structure for `procesoproyectos`
-- ----------------------------
DROP TABLE IF EXISTS `procesoproyectos`;
CREATE TABLE `procesoproyectos` (
  `id` int(11) NOT NULL,
  `tipofase` varchar(30) DEFAULT NULL,
  `urloficioaceptacion` text,
  `urlfichaaval` text,
  `urlinformeaprobacion` text,
  `urloficiodesignacion` text,
  `urlresolucionratificaciontutor` text,
  `urlplanificaciontutorias` text,
  `urlregistroactividades` text,
  `urlinformefinal` text,
  `urlresolucionevaluadores` text,
  `urlinfevaluador1` text,
  `urlinfevaluador2` text,
  `urlaptitudlegal` text,
  `urlresolucionfechahora` text,
  `urlprorroga` text,
  `urlactagrado` text,
  `estudiante` int(11) NOT NULL,
  `proyectoinves` int(11) NOT NULL,
  `tutor` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sdt_procesoproyectos_sdt_infpersonalestudiante1_idx` (`estudiante`),
  KEY `fk_sdt_procesoproyectos_sdt_proyectoinvestigacion1_idx` (`proyectoinves`),
  KEY `fk_sdt_procesoproyectos_sdt_infpersonaltutor1_idx` (`tutor`),
  CONSTRAINT `fk_sdt_procesoproyectos_sdt_infpersonalestudiante1` FOREIGN KEY (`estudiante`) REFERENCES `estudiante` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sdt_procesoproyectos_sdt_infpersonaltutor1` FOREIGN KEY (`tutor`) REFERENCES `tutor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sdt_procesoproyectos_sdt_proyectoinvestigacion1` FOREIGN KEY (`proyectoinves`) REFERENCES `proyectoinves` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of procesoproyectos
-- ----------------------------

-- ----------------------------
-- Table structure for `procesosexamen`
-- ----------------------------
DROP TABLE IF EXISTS `procesosexamen`;
CREATE TABLE `procesosexamen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipoface` text,
  `resolucionacep` text,
  `resolucioncrono` text,
  `resolucionfecha` text,
  `certificadoaprueba` text,
  `actitudlegal` text,
  `oficiofechainves` text,
  `oficiosegunda` text,
  `resolucionconvo` text,
  `examencomplexivo` int(11) NOT NULL,
  `secretaria` int(11) NOT NULL,
  `tutor` int(11) NOT NULL,
  `estudiante` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sdt_procesosexamen_examencomplexivo1_idx` (`examencomplexivo`),
  KEY `fk_sdt_procesosexamen_sdt_infsecretaria1_idx` (`secretaria`),
  KEY `fk_sdt_procesosexamen_sdt_infpersonaltutor1_idx` (`tutor`),
  KEY `fk_sdt_procesosexamen_sdt_infpersonalestudiante1_idx` (`estudiante`),
  CONSTRAINT `fk_sdt_procesosexamen_examencomplexivo1` FOREIGN KEY (`examencomplexivo`) REFERENCES `examencomplexivo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sdt_procesosexamen_sdt_infpersonalestudiante1` FOREIGN KEY (`estudiante`) REFERENCES `estudiante` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sdt_procesosexamen_sdt_infpersonaltutor1` FOREIGN KEY (`tutor`) REFERENCES `tutor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sdt_procesosexamen_sdt_infsecretaria1` FOREIGN KEY (`secretaria`) REFERENCES `secretaria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of procesosexamen
-- ----------------------------

-- ----------------------------
-- Table structure for `proyectoinves`
-- ----------------------------
DROP TABLE IF EXISTS `proyectoinves`;
CREATE TABLE `proyectoinves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folio` int(4) DEFAULT NULL,
  `matricula` int(4) DEFAULT NULL,
  `nivelestudios` int(11) DEFAULT NULL,
  `periodounidadtitulacion` int(11) DEFAULT NULL,
  `temaproyecto` text,
  `lineainvestigacion` text,
  `sugerenciaareaacademica` text,
  `tutorasignado` varchar(60) DEFAULT NULL,
  `periodorealizacionpi` int(11) DEFAULT NULL,
  `nombresevaluador1` varchar(60) DEFAULT NULL,
  `nombresevaluador2` varchar(60) DEFAULT NULL,
  `periodograduacion` varchar(60) DEFAULT NULL,
  `observaciones` text,
  `fechainicio` date DEFAULT NULL,
  `fechafin` date DEFAULT NULL,
  `fechaproroga` date DEFAULT NULL,
  `sugerencia` text,
  `temacambiado` varchar(100) DEFAULT NULL,
  `tutor` int(11) NOT NULL,
  `estudiante` int(11) NOT NULL,
  `secretaria` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sdt_proyectoinvestigacion_sdt_infpersonaltutor_idx` (`tutor`),
  KEY `fk_sdt_proyectoinvestigacion_sdt_infpersonalestudiante1_idx` (`estudiante`),
  KEY `fk_sdt_proyectoinvestigacion_sdt_infsecretaria1_idx` (`secretaria`),
  CONSTRAINT `fk_sdt_proyectoinvestigacion_sdt_infpersonalestudiante1` FOREIGN KEY (`estudiante`) REFERENCES `estudiante` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sdt_proyectoinvestigacion_sdt_infpersonaltutor` FOREIGN KEY (`tutor`) REFERENCES `tutor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sdt_proyectoinvestigacion_sdt_infsecretaria1` FOREIGN KEY (`secretaria`) REFERENCES `secretaria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of proyectoinves
-- ----------------------------

-- ----------------------------
-- Table structure for `rol`
-- ----------------------------
DROP TABLE IF EXISTS `rol`;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rol
-- ----------------------------
INSERT INTO `rol` VALUES ('1', 'Administrador', 'ADM');
INSERT INTO `rol` VALUES ('2', 'Tutor', 'TUTO');
INSERT INTO `rol` VALUES ('3', 'Secretaria', 'SECRE');
INSERT INTO `rol` VALUES ('4', 'Estudiante', 'EST');

-- ----------------------------
-- Table structure for `secretaria`
-- ----------------------------
DROP TABLE IF EXISTS `secretaria`;
CREATE TABLE `secretaria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombrecompleto` varchar(45) DEFAULT NULL,
  `cedula` int(10) DEFAULT NULL,
  `clave` varchar(45) DEFAULT NULL,
  `rol` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cedula_UNIQUE` (`cedula`),
  KEY `fk_sdt_infsecretaria_std_rol1_idx` (`rol`),
  CONSTRAINT `fk_sdt_infsecretaria_std_rol1` FOREIGN KEY (`rol`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of secretaria
-- ----------------------------

-- ----------------------------
-- Table structure for `tutor`
-- ----------------------------
DROP TABLE IF EXISTS `tutor`;
CREATE TABLE `tutor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombrecompleto` varchar(60) DEFAULT NULL,
  `cedula` int(11) NOT NULL,
  `clave` varchar(20) NOT NULL,
  `rol` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cedula_UNIQUE` (`cedula`),
  KEY `fk_sdt_infpersonaltutor_std_rol1_idx` (`rol`),
  CONSTRAINT `fk_sdt_infpersonaltutor_std_rol1` FOREIGN KEY (`rol`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tutor
-- ----------------------------
INSERT INTO `tutor` VALUES ('1', 'Morayma Bustos', '1104391775', 'admin', '2');
INSERT INTO `tutor` VALUES ('2', 'Ricardo Guaman', '1104509144', 'admin', '2');
INSERT INTO `tutor` VALUES ('3', 'Maria Fernanda Viteri', '1104520125', 'admin', '2');
INSERT INTO `tutor` VALUES ('4', 'Javie Salazar', '1145065122', 'admin', '2');
INSERT INTO `tutor` VALUES ('5', 'Irma Ortiz', '1045698745', 'admin', '2');
INSERT INTO `tutor` VALUES ('6', 'Corina Nuñez', '61456542', 'admin', '2');
INSERT INTO `tutor` VALUES ('7', 'Carolina San Lucas', '1458974552', 'admin', '2');
