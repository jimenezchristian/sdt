-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-01-2017 a las 05:33:08
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `bd_sdt`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo`
--

CREATE TABLE IF NOT EXISTS `catalogo` (
`id` int(11) NOT NULL,
  `nombres` varchar(100) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `codigo` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `catalogo`
--

INSERT INTO `catalogo` (`id`, `nombres`, `descripcion`, `codigo`) VALUES
(1, 'Carreras', 'Carrera posibles de la Universidad', 'CARRERA'),
(2, 'Periodos', 'Peridos de La universidad', 'PERIODO'),
(3, 'Modalidad', 'Modalidades que existen en la facultad', 'MODAL'),
(4, 'Nivel de Estudio', 'Ciclo en que cursa el estudiante', 'NIVESTU'),
(5, 'Tipo de Titulacion', 'Modalidad de Titularse', 'TIPO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiante`
--

CREATE TABLE IF NOT EXISTS `estudiante` (
`id` int(11) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `apellido` varchar(30) DEFAULT NULL,
  `cedula` int(10) DEFAULT NULL,
  `direccion` text,
  `carrera` int(11) DEFAULT NULL,
  `periodounidadtitulacion` int(11) NOT NULL,
  `modalidad` int(11) DEFAULT NULL,
  `nivelestudios` int(11) DEFAULT NULL,
  `convencional` varchar(30) DEFAULT NULL,
  `movil` varchar(30) DEFAULT NULL,
  `correo` varchar(30) DEFAULT NULL,
  `clave` varchar(20) DEFAULT NULL,
  `tipo` int(3) DEFAULT NULL,
  `rol` int(11) NOT NULL DEFAULT '4',
  `tutor` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;


--
-- Estructura de tabla para la tabla `examencomplexivo`
--

CREATE TABLE IF NOT EXISTS `examencomplexivo` (
`id` int(11) NOT NULL,
  `periodounidadtitulacion` int(11) DEFAULT NULL,
  `periodocapacitacion` int(11) DEFAULT NULL,
  `fechaexamencomplexivo` date DEFAULT NULL,
  `fechainvestidura` date DEFAULT NULL,
  `estudiante` int(11) NOT NULL,
  `secretaria` int(11) NOT NULL,
  `tutor` int(11) NOT NULL,
  `procesoproyectos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `items`
--

CREATE TABLE IF NOT EXISTS `items` (
`id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `catalogo` int(11) NOT NULL,
  `activo` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `items`
--

INSERT INTO `items` (`id`, `nombre`, `catalogo`, `activo`) VALUES
(1, 'Educacion Parvularia', 1, 1),
(2, 'Educación Inicial', 1, 1),
(3, 'Abril - Septiembre 2015', 2, 1),
(4, 'Octubre 2015 - Marzo 2016', 2, 1),
(5, 'Presencial', 3, 1),
(6, 'SemiPresencial', 3, 1),
(7, 'Noveno', 4, 1),
(8, 'Décimo', 4, 1),
(9, 'Actualización de Conocimientos', 4, 1),
(10, 'Estudiantes con créditos extra curriculares pendientes', 4, 1),
(13, 'Educación Básica\r\nEducación Básica', 1, 1),
(14, 'Turismo y Hoteleria', 1, 1),
(15, 'Cultura Física', 1, 1),
(16, 'Pedagogía de la Actividad Física y deporte', 1, 1),
(17, 'Docencia en Informática', 1, 1),
(18, 'Psicologia Educativa', 1, 1),
(19, 'Psicología Insdustrial', 1, 1),
(20, 'Psicopedagogía', 1, 1),
(21, 'Idiomas', 1, 1),
(22, 'Pedagogía de los Idiomas Nacionales Extranjeros', 1, 1),
(23, 'Abril - Septiembre 2016', 2, 1),
(24, 'Obtubre 2016 -Marzo 2017', 2, 1),
(25, 'Trabajo de Titulación', 5, 1),
(26, 'Exámen Complexivo', 5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
`id` int(11) NOT NULL,
  `url` varchar(200) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `icono` varchar(30) DEFAULT NULL,
  `menu` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `url`, `nombre`, `icono`, `menu`) VALUES
(1, 'listaEstudiantesTesis.htm', 'Estudiantes', 'fa fa-users', NULL),
(2, '', 'Formato Resoluciones', 'fa fa-archive', NULL),
(3, 'procesostesis.htm', 'Procesos', 'fa fa-tasks', NULL),
(4, 'vistaprocesosest.htm', 'Ver Proceso', 'fa fa-retweet', NULL),
(5, '', 'Formularios', 'fa fa-address-book', NULL),
(6, '', 'Trabajo de Titulación', 'fa fa-university', NULL),
(7, 'estudiantenew.htm', 'Nuevo Estudiante', 'fa fa-users', 6),
(8, 'estudiantelist.htm', 'Formulario Tema', 'fa fa-address-book', 6),
(9, 'listaEstudiantesTutor.htm', 'Visualizar Proceso', 'fa fa-tasks', 6),
(10, '', 'Periodos', 'fa fa-graduation-cap', 6),
(11, '', 'Examen Complexivo', 'fa fa-book', NULL),
(12, 'estudiantenew.htm', 'Nuevo Estudiante', 'fa fa-users', 9),
(13, 'tutorformex.htm', 'Formulario Exámen', 'fa fa-address-book-o', 9),
(14, '', 'Visualizar Proceso', 'fa fa-tasks', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menurol`
--

CREATE TABLE IF NOT EXISTS `menurol` (
`id` int(11) NOT NULL,
  `menu` int(11) NOT NULL,
  `rol` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menurol`
--

INSERT INTO `menurol` (`id`, `menu`, `rol`) VALUES
(6, 6, 2),
(7, 7, 2),
(8, 8, 2),
(9, 9, 2),
(10, 10, 2),
(11, 11, 2),
(12, 12, 2),
(13, 13, 2),
(14, 14, 2),
(1, 1, 3),
(2, 2, 3),
(3, 3, 3),
(4, 4, 4),
(5, 5, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesoproyectos`
--

CREATE TABLE IF NOT EXISTS `procesoproyectos` (
`id` int(11) NOT NULL,
  `tipofase` varchar(30) DEFAULT NULL,
  `id_proceso` int(11) NOT NULL,
  `archivo` mediumblob,
  `nombreArchivo` varchar(200) DEFAULT NULL,
  `estado` varchar(100) NOT NULL,
  `estudiante` int(11) NOT NULL,
  `proyectoinves` int(11) DEFAULT NULL,
  `tutor` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;



--
-- Estructura de tabla para la tabla `procesos`
--

CREATE TABLE IF NOT EXISTS `procesos` (
`id` int(11) NOT NULL,
  `nombreProceso` varchar(100) NOT NULL,
  `archivo` mediumblob NOT NULL,
  `nombreArchivo` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


--
-- Estructura de tabla para la tabla `procesosexamen`
--

CREATE TABLE IF NOT EXISTS `procesosexamen` (
`id` int(11) NOT NULL,
  `tipoface` text,
  `resolucionacep` text,
  `resolucioncrono` text,
  `resolucionfecha` text,
  `certificadoaprueba` text,
  `actitudlegal` text,
  `oficiofechainves` text,
  `oficiosegunda` text,
  `resolucionconvo` text,
  `examencomplexivo` int(11) NOT NULL,
  `secretaria` int(11) NOT NULL,
  `tutor` int(11) NOT NULL,
  `estudiante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectoinves`
--

CREATE TABLE IF NOT EXISTS `proyectoinves` (
`id` int(11) NOT NULL,
  `folio` int(4) DEFAULT NULL,
  `matricula` int(4) DEFAULT NULL,
  `nivelestudios` int(11) DEFAULT NULL,
  `temaproyecto` text,
  `lineainvestigacion` text,
  `sugerenciaareaacademica` text,
  `tutorasignado` varchar(60) DEFAULT NULL,
  `periodorealizacionpi` int(11) DEFAULT NULL,
  `nombresevaluador1` varchar(60) DEFAULT NULL,
  `nombresevaluador2` varchar(60) DEFAULT NULL,
  `periodograduacion` varchar(60) DEFAULT NULL,
  `observaciones` text,
  `fechainicio` date DEFAULT NULL,
  `fechafin` date DEFAULT NULL,
  `fechaproroga` date DEFAULT NULL,
  `sugerencia` text,
  `temacambiado` varchar(100) DEFAULT NULL,
  `tutor` int(11) NOT NULL,
  `estudiante` int(11) NOT NULL,
  `secretaria` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE IF NOT EXISTS `rol` (
`id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `codigo` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`, `codigo`) VALUES
(1, 'Administrador', 'ADM'),
(2, 'Tutor', 'TUTO'),
(3, 'Secretaria', 'SECRE'),
(4, 'Estudiante', 'EST');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secretaria`
--

CREATE TABLE IF NOT EXISTS `secretaria` (
`id` int(11) NOT NULL,
  `nombrecompleto` varchar(45) DEFAULT NULL,
  `cedula` int(10) DEFAULT NULL,
  `clave` varchar(45) DEFAULT NULL,
  `rol` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `secretaria`
--

INSERT INTO `secretaria` (`id`, `nombrecompleto`, `cedula`, `clave`, `rol`) VALUES
(1, 'Silvia Elizabeth Santan', 1803890217, '1803890217', 3),
(2, 'Janeth Bejarano', 1804647673, '1804647673', 3),
(3, 'Norma Punina', 1802047611, '1802047611', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutor`
--

CREATE TABLE IF NOT EXISTS `tutor` (
`id` int(11) NOT NULL,
  `nombrecompleto` varchar(60) DEFAULT NULL,
  `cedula` int(11) NOT NULL,
  `clave` varchar(20) NOT NULL,
  `rol` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tutor`
--

INSERT INTO `tutor` (`id`, `nombrecompleto`, `cedula`, `clave`, `rol`) VALUES
(1, 'Morayma Bustos', 1104391775, 'admin', 2),
(2, 'Ricardo Guaman', 1104509144, 'admin', 2),
(3, 'Maria Fernanda Viteri', 1104520125, 'admin', 2),
(4, 'Javie Salazar', 1145065122, 'admin', 2),
(5, 'Irma Ortiz', 1045698745, 'admin', 2),
(6, 'Corina Nuñez', 61456542, 'admin', 2),
(7, 'Carolina San Lucas', 1458974552, 'admin', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `catalogo`
--
ALTER TABLE `catalogo`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estudiante`
--
ALTER TABLE `estudiante`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `cedula_UNIQUE` (`cedula`), ADD KEY `fk_sdt_infpersonalestudiante_std_rol1_idx` (`rol`), ADD KEY `fk_estudiante_tutor1_idx` (`tutor`);

--
-- Indices de la tabla `examencomplexivo`
--
ALTER TABLE `examencomplexivo`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_examencomplexivo_sdt_infpersonalestudiante1_idx` (`estudiante`), ADD KEY `fk_examencomplexivo_sdt_infsecretaria1_idx` (`secretaria`), ADD KEY `fk_examencomplexivo_sdt_infpersonaltutor1_idx` (`tutor`), ADD KEY `fk_examencomplexivo_sdt_procesoproyectos1_idx` (`procesoproyectos`);

--
-- Indices de la tabla `items`
--
ALTER TABLE `items`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_items_catalogo1_idx` (`catalogo`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_sdt_menu_sdt_menu1_idx` (`menu`);

--
-- Indices de la tabla `menurol`
--
ALTER TABLE `menurol`
 ADD PRIMARY KEY (`id`,`menu`,`rol`), ADD KEY `fk_sdt_menu_has_std_rol_std_rol1_idx` (`rol`), ADD KEY `fk_sdt_menu_has_std_rol_sdt_menu1_idx` (`menu`);

--
-- Indices de la tabla `procesoproyectos`
--
ALTER TABLE `procesoproyectos`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_sdt_procesoproyectos_sdt_infpersonalestudiante1_idx` (`estudiante`), ADD KEY `fk_sdt_procesoproyectos_sdt_proyectoinvestigacion1_idx` (`proyectoinves`), ADD KEY `fk_sdt_procesoproyectos_sdt_infpersonaltutor1_idx` (`tutor`), ADD KEY `foranea_proceso` (`id_proceso`);

--
-- Indices de la tabla `procesos`
--
ALTER TABLE `procesos`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `procesosexamen`
--
ALTER TABLE `procesosexamen`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_sdt_procesosexamen_examencomplexivo1_idx` (`examencomplexivo`), ADD KEY `fk_sdt_procesosexamen_sdt_infsecretaria1_idx` (`secretaria`), ADD KEY `fk_sdt_procesosexamen_sdt_infpersonaltutor1_idx` (`tutor`), ADD KEY `fk_sdt_procesosexamen_sdt_infpersonalestudiante1_idx` (`estudiante`);

--
-- Indices de la tabla `proyectoinves`
--
ALTER TABLE `proyectoinves`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_sdt_proyectoinvestigacion_sdt_infpersonaltutor_idx` (`tutor`), ADD KEY `fk_sdt_proyectoinvestigacion_sdt_infpersonalestudiante1_idx` (`estudiante`), ADD KEY `fk_sdt_proyectoinvestigacion_sdt_infsecretaria1_idx` (`secretaria`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secretaria`
--
ALTER TABLE `secretaria`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `cedula_UNIQUE` (`cedula`), ADD KEY `fk_sdt_infsecretaria_std_rol1_idx` (`rol`);

--
-- Indices de la tabla `tutor`
--
ALTER TABLE `tutor`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `cedula_UNIQUE` (`cedula`), ADD KEY `fk_sdt_infpersonaltutor_std_rol1_idx` (`rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `catalogo`
--
ALTER TABLE `catalogo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `estudiante`
--
ALTER TABLE `estudiante`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT de la tabla `examencomplexivo`
--
ALTER TABLE `examencomplexivo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `items`
--
ALTER TABLE `items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `menurol`
--
ALTER TABLE `menurol`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `procesoproyectos`
--
ALTER TABLE `procesoproyectos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `procesos`
--
ALTER TABLE `procesos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `procesosexamen`
--
ALTER TABLE `procesosexamen`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `proyectoinves`
--
ALTER TABLE `proyectoinves`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `secretaria`
--
ALTER TABLE `secretaria`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tutor`
--
ALTER TABLE `tutor`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `estudiante`
--
ALTER TABLE `estudiante`
ADD CONSTRAINT `fk_estudiante_tutor1` FOREIGN KEY (`tutor`) REFERENCES `tutor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sdt_infpersonalestudiante_std_rol1` FOREIGN KEY (`rol`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `examencomplexivo`
--
ALTER TABLE `examencomplexivo`
ADD CONSTRAINT `fk_examencomplexivo_sdt_infpersonalestudiante1` FOREIGN KEY (`estudiante`) REFERENCES `estudiante` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_examencomplexivo_sdt_infpersonaltutor1` FOREIGN KEY (`tutor`) REFERENCES `tutor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_examencomplexivo_sdt_infsecretaria1` FOREIGN KEY (`secretaria`) REFERENCES `secretaria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `items`
--
ALTER TABLE `items`
ADD CONSTRAINT `fk_items_catalogo1` FOREIGN KEY (`catalogo`) REFERENCES `catalogo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `menu`
--
ALTER TABLE `menu`
ADD CONSTRAINT `fk_sdt_menu_sdt_menu1` FOREIGN KEY (`menu`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `menurol`
--
ALTER TABLE `menurol`
ADD CONSTRAINT `fk_sdt_menu_has_std_rol_sdt_menu1` FOREIGN KEY (`menu`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sdt_menu_has_std_rol_std_rol1` FOREIGN KEY (`rol`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `procesoproyectos`
--
ALTER TABLE `procesoproyectos`
ADD CONSTRAINT `fk_sdt_procesoproyectos_sdt_infpersonalestudiante1` FOREIGN KEY (`estudiante`) REFERENCES `estudiante` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sdt_procesoproyectos_sdt_infpersonaltutor1` FOREIGN KEY (`tutor`) REFERENCES `tutor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `foranea_proceso` FOREIGN KEY (`id_proceso`) REFERENCES `procesos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `procesosexamen`
--
ALTER TABLE `procesosexamen`
ADD CONSTRAINT `fk_sdt_procesosexamen_examencomplexivo1` FOREIGN KEY (`examencomplexivo`) REFERENCES `examencomplexivo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sdt_procesosexamen_sdt_infpersonalestudiante1` FOREIGN KEY (`estudiante`) REFERENCES `estudiante` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sdt_procesosexamen_sdt_infpersonaltutor1` FOREIGN KEY (`tutor`) REFERENCES `tutor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sdt_procesosexamen_sdt_infsecretaria1` FOREIGN KEY (`secretaria`) REFERENCES `secretaria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proyectoinves`
--
ALTER TABLE `proyectoinves`
ADD CONSTRAINT `fk_sdt_proyectoinvestigacion_sdt_infpersonalestudiante1` FOREIGN KEY (`estudiante`) REFERENCES `estudiante` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sdt_proyectoinvestigacion_sdt_infpersonaltutor` FOREIGN KEY (`tutor`) REFERENCES `tutor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `secretaria`
--
ALTER TABLE `secretaria`
ADD CONSTRAINT `fk_sdt_infsecretaria_std_rol1` FOREIGN KEY (`rol`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tutor`
--
ALTER TABLE `tutor`
ADD CONSTRAINT `fk_sdt_infpersonaltutor_std_rol1` FOREIGN KEY (`rol`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
